# Estimation

[//]: <------------------------------------------------ QUALIFIED ---------------------------------------------------->

## Scope concept

**Scope** - опреденый список(обьем) работ.
**Project Scope** - работа которую необходимо выполнить для поставки продукта, сервиса или определенной фичи.
**Product Scope** - фичи или функционал который характеризуют(описывают) продукт, сервис или результ.

## Estimates, Targets and Commitments

**Estimate** - это наше предположение сколько займет реализация той или иной цели.
**Target** - это определение желаемой бизнес-цели.
**Commitment** (обязательство) это обещание поставить оговоренный функционал с конкретным качеством к определенной дате.

McConnell defines a good estimate as an estimate that will provide enough clear view of the project-reality to allow project leadership to make good decisions about how to control the project to hit the targets.


## Overestimate vs Underestimate

Arguments against over estimation:
- **Parkinson's law** - work will expand to fill the available time. 
- **Goldratt's Student's Syndrome** - developers will procrastinate until late in the project and then rush to complete the work.

Arguments against under estimation:
- Банально можно не успеть запланированый скоуп работ

## Decomposition and Recomposition

Decomposition is the practice of separating an estimate into multiple pieces, estimating each piece individually, and then recombining the individual estimates into an aggregate estimate. This estimation approach is also known as "bottom up," "micro estimation," "module build up," "by engineering procedure," and by many other names.


## Analogy based estimation

You can create accurate estimates for a new project by comparing the new project to a similar past project.
В рамках реализации данной методологии создается сравнительная таблица, со строками как задачами и тремя колонками: текущий, прошлый, дельта(на сколько больше может занять реализация нового проекта).


## Story based estimations

В первую очередь нужно определить что мы подразумеваем под определинем story.
**Story** - это способ описания требований к разрабатываемой системе. При этом если мы используем систему agile ми имеем ситуацию когда постоянно происходит переоценка исполняемых работ.
Но когда мы говорим об оценке story нужно говорить также о двух вещах:

- какие единицы измерения мы используем;
- чем мы руководствуемся когда даем оценку.

### Единицы измерений для стори

- часах, днях, неделях, идеальных днях 
- T-shirt sizing (M, S, L, XL)
- story point

**Story point** - это абстрактная единица измерения, характеризующее обьем усилий которые нужно приложить для исполнения задачи.
При этом эта оценка у каждой команды своя. Также пару слов о самом наборе чисел которые используются для оценки:

- последовательный ряд чисел: 1, 2, 3, 4, 5, ...
- числа Фибоначчи: 1, 2, 3, 5, 8, 13, 21, ...
- степени двойки: 1, 2, 4, 8, 16, 32, 64, ...

### Чем мы руководствуемся когда даем оценку

- **Знания**: сколько знает команда об этой истории?
- **Сложность**: насколько сложной будет реализация?
- **Объем**: насколько велика эта история? Сколько времени это займет?
- **Неопределенность**: какие переменные и неизвестные факторы могут повлиять на историю?

### Преимущества

- Относительная оценка – это простая, надежная методика, которая хорошо сочетается с практикой Agile. Она очень адаптивна и во всех последующих итерациях точность оценки будет только повышаться.
- Planning Poker - техника планирования является совместной деятельностью, базирующейся на получении общих договоренностей среди членов команды и имеет положительное влияние на команду разработчиков. Она основывается на мудром совете «спроси команду».
 
### Недостатки

- Относительные оценки основаны на исторических данных и точность зависит от сходства новых историй с более ранними. Если новые истории, коренным образом отличаются от предыдущих историй, не исключено, что точность оценки может уменьшиться.
- Точность скорости (Velocity) зависит от знаний и опыта команды разработчиков. Любые изменения в составе команды повлияют на скорость и, следовательно, на оценку.


[//]: <------------------------------------------------ COMPETENT ---------------------------------------------------->


## Cone of Uncertainty

![Cone of uncetainty](media/cone-of-uncertainty.png)

Cone of Uncertainty (Конус Неопределенности) это график показующий вариативность нашей оценки на разных этапах разработки приложения и как она стает более точной ближе к концу.
 
## Source of Estimation Errors

- Omitted Activities
    - Developers activity
        - менторинг новых членов команды;
        - участие в неучтенных митингах;
        - уточнение требований/коммуникация;
        - написание документации;
        - написание юнит-тестов;
        - дополнительные требования по depoly/deliery (залить архив с проектом через FTP куда-то или через сервис в CRM(Content Read Management) систему);
        - взамодействие с внешними системами (если таск включает работу с translations работа с сервисом по их хранению).
        - support & maintenance того что сделано, старых версий приложенния;
        - участие в ревью-процессе;
        - неучтенный баг-фиксинг;
        - неучтенное улучшение performance;
        - демонстрации и взаимодействие с customers и end-users;
        
    - Non-Software-Development activities
        - project installation (иногда мы оцениваем только девелоперскую работу, когда все энвы настроены, проект поднят локально);
        - Costs related to staff turnover and the need for new skills
        - Vacations, Sick-days, Holidays, Time-offs;
        - Setting up new workstations or additional software;
        - Troubleshooting hardware and software problems;

    
- Unfounded Optimism

- Underestimating costs by assuming who is doing what

- Оценка навскидку(экспромтом) (Off-The-Cuff Estimates)


## Diseconomies of Scale 

Предположим, что размер проекта измеряется в строках кода (LOC). Люди, естественно, предполагают, что система, которая в 10 раз больше другой системы, потребует в 10 раз больше усилий. Естественно можно предположить что система с 100 000 LOC была бы менее чем в 10 раз дороже, чем система с 10 000 LOC.

Но в программном обеспечении большие проекты требуют координации между большими группами людей, что требует большего общения. По мере увеличения размера проекта количество каналов связи между разными людьми увеличивается как график квадратической функции.

Следствием этого является то, что по мере увеличения размера проекта усилия по проектам также экспоненциально увеличиваются. Это известно как **diseconomy of scale**, противоположность **economy of scale**.

![Cone of uncetainty](media/diseconomy-of-scale.jpg)

**Подводя итоги**: большинство проектов в организации часто имеют одинаковый размер, но если мы собираемся оценивать гораздо более крупные проекты, мы должны учитывать неэффективность масштаба при разработке программного обеспечения. В обоих случаях, приобретенный опыт, лучшие практики и улучшения производительности включены. Дело в сложности координации, необходимой в более крупных проектах.


## Count, Compute, Judge techniques

В данном топике наводился пример:

> Ви и еще три человека сидите за одним столом в огромном зале на конференции по "Эстимированию", внезапно заходит конферансье и спрашивает сколько точно людей сидит в этом зале? Это нужно для того чтобы заказать десерт.
>
> Билл оценщик который сидит справа говорит: "У меня хобби заниматся оценкой толпы(estimating crowds). Исходя из своего опыта я могу предположить что в зале сидит около 335 человек".
>
> Карл который сидит за столом от вас говорит: "Эта комната имеет 11 столов вдлину и 7 столов вглубь, один мой друг - это планировщик банкетов и он мне говорил что за этот чтол помещается 5 человек, так что после не хитрых вычислений мы получаем 11 х 7 х 5 = 385".
>
> Люси - оценщик слева говорит:"Я заметила что максимальное число гостей для этой комнаты - 485, при этом эта комната заполнена примерно на 70-80 процентов, так что можна сделать вывод что людей в этом зале примерно 340 to 388. Средний показатель - 364, но для простоты возьмем 365."
>
> Билл говорит: "Итак мы имеем 335, 365, 385, по видимому правильный ответ где в этом диапазоне. Я соглашусь с 365, ви как?". Я тоже, - сказал Карл. Все смотрят на Вас и ожидают ответа, вы в свою очередь просите минуту на размышления.
>
> Когда вы возвращаетесь вы говорите: "Помните что мы пробивали билеты когда входили в зал, я заметил что сканер ID работает как счетчик. Так вот я подошел к охннику и он ответил что согласно ID он просканировал 407 билетов, при этом комнату никто не покидал, так что я думаю 407 это наша оценка, как Вам такое?".

Karl had the historical data of knowing that the banquet was planned to have 5 people per table. He **counted** the number of tables and then **computed** the answer from that.
Similarly, Lucy based her estimate on the documented fact of the room's occupancy limit. She used her **judgment** to estimate the room was 70 to 80 percent full.

- If you can count the answer directly, you should do that first. That approach produced the most accurate answer in the story;
- If you can't count the answer directly, you should count something else and then compute the answer by using some sort of calibration data;
- Use judgment alone only as a last resort.

## Delphi method

Широкополосные Дельфи - метод оценки является на основе консенсуса методика оценки усилий. Они назвали это «широкополосным» , потому что, по сравнению с существующим методом Дельфи, новый метод включает более активное взаимодействие и больше общения среди участников процесса
**Комманда приходит к единому мнению.**

1) Coordinator presents each expert with a specification and an estimation form.
2) Coordinator calls a group meeting in which the experts discuss estimation issues with the coordinator and each other. If the group agrees on a single estimate without much discussion, the coordinator assigns someone to play devil's advocate.
3) Experts fill out forms anonymously.
4) Coordinator prepares and distributes a summary of the estimates
5) Coordinator calls a group meeting, specifically focusing on having the experts discuss points where their estimates vary widely
6) Experts fill out forms, again anonymously, and steps 4 to 6 are iterated for as many rounds as appropriate.

## Challenges with estimation size

The lines of code (LOC) measure is the most common size measure used for estimation.

В главе обсуждается проблематика оценки используя LOC як единицу измерения размера той или иной задачи.

**Плюсы:**
- эта единица всем понятна;
- позволяет делать кросс-проектое сравнения для аналогичных задач;
- есть определенные tools которые заточены под использование LOC как единицы оценки размера задачи.

**Минусы:**
- местами больше кода не значит сложнее;
- этот показатель индивидуален для каждого программиста отдельно;
- также этот показатель может быть индивидуален для каждого проекта в отдельности.


## Challenges with Estimating Effort

The largest influence on a project's effort is the size of the software being built.
The second largest influence is your organization's productivity.

Также можно сказать что при попытке дать оценку нужно обратить внимание что входит в данную задачу. Чисто дев работа или еще например написание тестов, менеджмент, документация.

В данной главе говорилось о том что существуют исторические данные статистики куда входили такие параметры как size, productivity. Также при рассчете усилий на ту или иную таску люди могли как закладывать так и не закладывать время на доп активности.
По этому effort может так варьироватся.


## Challenges with Estimating Schedule

Факторы которые могут влиять на график поставок:

- customer deadlines;
- trade show deadlines (торговые выставки продукции);
- seasonal sales-cycle deadlines;


## Documenting and presenting estimation results

### Estimate Assumptions

Можно написать на чем именно основывается ваша оценка. Тут и вступает в дело assumptions. Где вы можете указать примерную точность ваших прогнозов и что на них влияет.

- Какие фичи включены, какие нет и как они между собой будут связаны (на что мы подписываемся);
- Какие ресурсы нам будут доступны;
- Зависимости от third-libraries;
- Major unknowns, что именно вносит неточность в наш прогноз;
- Настолько точна оценка и для каких целей она предназначена.

![Estimation assumptions](media/estimate-aassumptions.jpg)

### Выражение неопределенности

**Plus-or-Minus Qualifiers**

"6 months ±2 months" 
"$600,000 +$200,000/-$100,000"

**Risk Quantification**

| Impact      | Description of Risk                                                   |
|-------------|-----------------------------------------------------------------------|
| +1.5 months | This version needs more than 20% new features compared to Version 2.0 |
| +1 month    | New development tools don't work as well as planned                   |
| +1 month    | Can't reuse 80% of the database code from the previous version        |
| -0.5 month  | All developers assigned 100% by April 1                               |
| -0.5 month  | New development tools work better than planned                        |

**Confidence Factors**

![Confidence Factors](media/confidence-factor.jpg)

**Case-Based Estimates**

![Case-Based Estimates](media/case-based-estimate.jpg)


## Story based scope definition: scoping project, release planning

### Scoping project

**Project scope** is the part of project planning that involves determining and documenting a list of specific project goals, deliverables, tasks, cost and deadlines.

- High-level определите характеристики системы;
- Составте список фич и требований к системе;
- Разбейте фичи на user-story

### Release planning

- Рассчитать на какие ресурсы мы можем рассчитывать;
- Define Release scope: какие фичи могут быть взяты в работу в рамках релиза?
- Дать оценку фичам находящимся в беклоге;
- Выставить приорететы для фич взятых в работу;


## PERT

[Wiki link](https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique)
PERT(Program evaluation and review technique) - метод оценки и анализа проектов, который используется в управлении проектами.

PERT предназначен для очень масштабных, единовременных, сложных, нерутинных проектов. Метод подразумевает наличие неопределённости, давая возможность разработать рабочий график проекта без точного знания деталей и необходимого времени для всех его составляющих.

Оценка длительности выполнения задачи:

<img src="media/pert-formula.png" alt="Pert formula" width="230"/>

е - ожидаемая длительность
о - оптимистическая оценка
m - наиболее вероятная
p - пессимистическая оценка

**Сетевые диаграммы PERT**

это диаграммы взаимосвязей работ и событий

Диаграмма PERT с работами на стрелках представляет собой множество **точек-вершин (события)** вместе с соединяющими их ориентированными **дугами (работы)**.

![Pert Graph](media/pert-graph.jpg)

Иллюстрация диаграммы, составленной по технологии PERT, который демонстрирует как с помощью диаграммы разбить сад:

![Pert Graph 2](media/pert-graph-2.jpg)

- каждой дуге приписывается численная характеристика - объёмы выделяемых на данную работу ресурсов и, соответственно, её ожидаемая продолжительность (длина дуги);
- **вершина** интерпретируется как событие завершения работ, представленных дугами, которые входят в неё, и одновременно начала работ, отображаемых дугами, исходящими оттуда;
- ни к одной из работ нельзя приступить прежде, чем будут выполнены все работы, предшествующие ей согласно технологии реализации проекта;
- начало этого процесса — вершина без входящих;
- окончание — вершина без исходящих дуг.

