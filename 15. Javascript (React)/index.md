# React

*Какую проблему решает react?*

Самые «тяжелые» операции в web — работа с DOM. Реакт оптимизирует эту работу и берет на себя обязаности по работе с ним.
Как? **Virtual DOM** + обновление страницы за минимум «телодвижений».
Так же вы сами можете влиять на этот процесс посредством различных *hooks* которые предоставляет либа.

## JSX

**JSX** (JavaScript XML) - это синтаксическое расширение для JavaScript (функции CreateElement).  

```jsx harmony
<MyButton color="blue" shadowSize={2} action={this.action}>
  Click Me
</MyButton>
```

```js
React.createElement(
  MyButton,
  { color: 'blue', shadowSize: 2 },
  'Click Me'
)
```

- Babel компилирует JSX в вызовы React.createElement()
- Теги начинаются с заглавной буквы (чтобы отличить React-компоненты от html тегов)
т.к. неявно вызывается функция React.createElement, в файле должен быть import React from 'react'
- атрибуты: class === className, for === htmlFor
- встраивание пользовательского ввода в JSX является безопасным (не допускает XSS-атак) - потому что все преобразуется в строку, а не выполняется

## Virtual DOM

Это концепт, при котором мы создаем легковесную "виртуальную" копию DOM дерева и все модификации происходят именно с ней.
При этом мы просто указываем декларативно что именно мы хотим видить, при этом абстрагируясь от непосредственной 
модификации атрибутов, DOM-узлов, привязки events.

После внесения всех модификаций образовуется новая "виртуальная" модель, которая сравнивается с предидущей и именно эти изменения вносятся в реальный DOM.

> In simple words, you tell React what state you want the UI to be in, and it makes sure that the DOM matches that state.
The great benefit here is that as a developer, you would not need to know how the attribute manipulation, event handling 
or the manual DOM updates happen behind the scenes.

### Batch Update

React следует методологии *batch update* при котором изменения вносятся **"пачками"**, а не по одному.

**Rewind:**

- Frequent DOM manipulations are expensive and performance heavy.
- Virtual DOM is a virtual representation of the real DOM.
- When state changes occur, the virtual DOM is updated and the previous and current version of virtual DOM is compared. This is called “diffing”.
- The virtual DOM then sends a batch update to the real DOM to update the UI.
- React uses virtual DOM to enhance its performance.
- It uses the observable to detect state and prop changes.
- React uses an efficient diff algorithm to compare the versions of virtual DOM.
- It then makes sure that batched updates are sent to the real DOM for repainting or re-rendering of the UI.

Есть два варианта узнать, что данные изменились:

- Первый из них — «dirty checking» (грязная проверка) заключается в том,
чтобы опрашивать данные через регулярные промежутки времени и рекурсивно проверять все значения в структуре данных.
- Второй вариант — «observable» (наблюдаемый) заключается в наблюдении за изменением состояния.
Если ничего не изменилось, мы ничего не делаем. Если изменилось, мы точно знаем, что нужно обновить.

## Shadow DOM

https://learn.javascript.ru/shadow-dom

**Shadow DOM** – это средство для создания отдельного DOM-дерева внутри элемента, которое не видно снаружи без применения специальных методов.

Для того чтобы определить "точку вставки" (insertion point) обычного содержимого можно использовать тег `<content>`.

``` html
<p id="elem">Доброе утро, страна!</p>
```

```javascript
var root = elem.createShadowRoot();
root.innerHTML = `
    <p>Start caption</p>
    <content select=".header"></content>
    <content></content>
    <content select=".author"></content>
    <p>End caption</p>
`;
```

**Output:**
```text
Start caption

New article title
"Me!"
Lorem ipsum for some test text

End caption
```

В `<content>` атрибутом select можно указать конкретный селектор содержимого, которое нужно переносить.
Например, `<content select="h3"></content>` перенесёт только заголовки.

**Важные детали:**

- Тег `<content>` влияет только на отображение, он не перемещает узлы физически.
Как видно из картинки выше, текстовый узел «Доброе утро, страна!» остался внутри `p#elem`.
Его можно даже получить при помощи `elem.firstElementChild`.
- Внутри `<content>` показывается не элемент целиком `<p id="elem">`, а его содержимое,
то есть в данном случае текст «Доброе утро, страна!».

### ShadowRoot

`elem.shadowRoot` - корень внутреннего DOM-дерева. Если нужно работать с содержимым в Shadow DOM, то нужно перейти к иммено к нему

Также вы можете закрыть доступ к модификации shadowRoot средствами JS сменив режим.

```javascript
let shadow = elementRef.attachShadow({ mode: 'open' });
let shadow = elementRef.attachShadow({ mode: 'closed' });
```

```javascript
const elem = document.getElementById('myShadowElement');
const shadow = elem.attachShadow({ mode: 'closed' });

shadow.innerHTML = `
    <p>Start caption</p>
    <content select=".header"></content>
    <content select=".date">Date is not specified</content>
    <content></content>
    <content select=".author"></content>
    <p>End caption</p>
`;
```

![Result of closed Shadow DOM](media/shadow-dom-closed.png)


## Presentational and Container Components

Они отличаются имменно идеологически по приминению и у функциях которые они исполняют

**Presentational components:**

- По большей мере отвечают за представление, как все выглядит;
- Обычно имею HTML-разметку внутри;
- Не имеют зависимостей как REST, Flux, actions, store;
- Принимают данные по большей мере через `props`;
- Не определяют как бизнесс-данные подгружаются или изменяются;
- Редко имеют собственный `state` (но когда имеют, это скорей состаяние UI);
- В основном это `functional components`, если только им не нужен `state`, lifecycle hooks, performance optimization;
- Examples: Page, Sidebar, UserInfo, List.

**Container components:**

- По большей мере обьясняют, как компоненты работают;
- Обычно не имеют разметки, разве что оберточные `div` и подключают другие компненты;
- Обеспечивают данными и логикой другим компонентам;
- Имеют зависимости, как REST, Flux, store;
- Stateful, склонны служить источниками данных;
- Обычно сгенерированные как High-Order-Components: as connect() React Redux;
- Examples: UserPage, StoryContainer. FollowersSidebar;

## Stateless and Stateful

Отличаются наявностью или отсутсвием state (внутреннего состояния) компоненты.

**When would you use a stateless component?**

- When you just need to present the props
- When you don’t need a state, or any internal variables
- When creating element does not need to be interactive
- When you want reusable code

**When would you use a stateful component?**

- When building element that accepts user input
- ..or element that is interactive on page
- When dependent on state for rendering, such as, fetching data before rendering
- When dependent on any data that cannot be passed down as props

## Classes and Functions

Начиная с версии 0.14, компоненты могут быть обьявлены как классы и ф-ии. Компоненты как ф-и выглядят проще,
но они лишены нескольких фич (state, lifecycle hooks).

## Pure Components

PureComponent изменяет lifecycle-метод shouldComponentUpdate, автоматически проверяя, нужно ли заново отрисовывать компонент.
При этом PureComponent будет вызывать рендер только если обнаружит изменения в state или props компонента,
а значит во многих компонентах можно менять state без необходимости постоянно писать.

В качестве проверки он использует сравнение **shallowEqual**.

**shallowEqual** аналог операции `===`
- примитивы сравнивает по значению;
- ссылочные типы по ссылкам, а по значениям аттрибутов.

## Lifecycle

**Основные методы жизненного цикла:**

![Result of closed Shadow DOM](media/main_lifecycle_hooks.png)

**Полная диаграмма вызова hooks:**

![Result of closed Shadow DOM](media/full_lifecycle_hooks.png)

> Примечание:
  Устаревшие методы(для 16.8.6):
  - UNSAFE_componentWillMount()
  - UNSAFE_componentWillUpdate()
  - UNSAFE_componentWillReceiveProps()

### Обработка ошибок
Следующие методы вызываются, если произошла ошибка в процессе рендеринга, методе жизненного цикла или конструкторе любого дочернего компонента.

- static getDerivedStateFromError()
- componentDidCatch()

### API

- setState()
- forceUpdate()
- [defaultProps](https://ru.reactjs.org/docs/react-component.html#defaultprops)  
    Можно определить как свойство самого класса компонента, которое позволяет установить `props` класса по умолчанию.
    Это используется для неопределённых `undefined` пропсов, но не для пропсов со значением `null`(считается как значение).

- [propTypes](https://ru.reactjs.org/docs/typechecking-with-proptypes.html)  
    Можно опрежелить каким типам должны соответсвовать те или иные `props`
    
- [displayName](https://ru.reactjs.org/docs/react-component.html#displayname)  
    По умолчанию используется имя функции или класса, указанное при определении компонента,
    но например, можно использовать при отладке или создании High-Order Component.

### Список hooks:

- **constructor(props)**  
    Вы можете не использовать конструктор в React-компоненте, если вы не определяете состояние или не привязываете методы.
    
    В начале конструктора необходимо вызывать super(props). Если это не сделать, this.props не будет определён
    
    Цели:  
        - Инициализация внутреннего состояния через присвоение объекта this.state;  
        - Привязка обработчиков событий к экземпляру.
    
- **componentDidMount()**  
    Этот метод подходит для настройки подписок

- **shouldComponentUpdate(nextProps, nextState)**  
    Вызывается перед рендером, когда получает новые пропсы или состояние. Значение по умолчанию равно `true`.
    Этот метод не вызывается при первом рендере или когда используется `forceUpdate()`.
    
    При этом если вы опеределили компоненту как `PureComponent` то этот метод
    по дефолту будет производить `shallowCompare` для state & props.

- **componentDidUpdate(prevProps, prevState, snapshot)**  
    componentDidUpdate() вызывается сразу после обновления. Не вызывается при первом рендере.
    
    Метод позволяет работать с DOM при обновлении компонента. Также он подходит для выполнения таких сетевых запросов,
    которые выполняются на основании результата сравнения текущих пропсов с предыдущими.
    Если пропсы не изменились, новый запрос может и не требоваться.
    
    snapshot - передается, если был реализован `getSnapshotBeforeUpdate()`
    
- **static getDerivedStateFromProps()**  
    Он должен вернуть объект для обновления состояния или null, чтобы ничего не обновлять.
    
    ```javascript
    static getDerivedStateFromProps (nextProps, prevState) {
        if (nextProps.someValue !== prevState.someValue){
            return { someState: nextProps.someValue };
        }
        else return null;
    }
    ```
    
- **getSnapshotBeforeUpdate(prevProps, prevState)**  
    Это применяется редко, но может быть полезно в таких интерфейсах, как цепочка сообщений в чатах, в которых позиция прокрутки обрабатывается особым образом.
    
    Значение снимка (или null) должно быть возвращено.
    
> Примечание:
  Предохранители перехватывают ошибки в компонентах ниже по дереву. Предохранители не могут поймать ошибку внутри себя.
    
- **static getDerivedStateFromError()**  
    Этот метод жизненного цикла вызывается после возникновения ошибки у компонента-потомка.
    Он получает ошибку в качестве параметра и возвращает значение для обновления состояния.

- **componentDidCatch(error, info)**  
    Этот метод жизненного цикла вызывается после возникновения ошибки у компонента-потомка. Он получает два параметра:
        - error — перехваченная ошибка
        - info — объект с ключом componentStack, содержащий информацию о компоненте, в котором произошла ошибка.

```javascript
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Обновите состояние так, чтобы следующий рендер показал запасной интерфейс.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    // Пример "componentStack":
    //   in ComponentThatThrows (created by App)
    //   in ErrorBoundary (created by App)
    //   in div (created by App)
    //   in App
    logComponentStackToMyService(info.componentStack);
  }

  render() {
    if (this.state.hasError) {
      // Здесь можно рендерить запасной интерфейс
      return <h1>Что-то пошло не так.</h1>;
    }

    return this.props.children;
  }
}
```

## State vs. Props

**Props**
- передаются в компонент из-вне
- props нельзя изменить (однонаправленный поток данных, сверху-вниз)

**State**
- хранит внутреннее состояние компонента между рендерами;
- setState() асинхронный, иммутабельный, можно внутрь передавать функцию или объект
    (если не зависим от прошлого состояния)
    
При изменении как одного так и другого происходит ре-рендер.

## Refs

Рефы дают возможность получить доступ к DOM-узлам или React-элементам, созданным в рендер-методе.

### Создание рефов

- React.createRef();
- callback-ref

```javascript
class MyComponent extends Component {
    constructor (props) {
        super(props);
        this.containerRef = React.createRef();
        this.inputRef = null;
    }

    componentDidMount () {
        console.log(this.containerRef.current); // React.createRef()
        console.log(this.inputRef); // callback-ref
    }

    render () {
        return (
            <div ref={this.containerRef}>
                <input
                    type="text"
                    ref={(element) => {
                        this.inputRef = element;
                    }}
                />
            </div>
        );
    }
}
```

Если ref колбэк определён как встроенная функция, колбэк будет вызван дважды во время обновлений: первый раз со 
значением `null`, а затем снова с DOM-элементом. Это связано с тем, что с каждым рендером создаётся новый экземпляр
функции, поэтому React должен очистить старый реф и задать новый. Такого поведения можно избежать, если колбэк в 
ref будет определён с привязанным к классу контекстом.


## Events

- React events используют нейминг в формате camelCase;
- JSX вы можете передать ф-ю для этого обработчика;
- React использует SyntheticEvent. SyntheticEvent сделан в соответствии с W3C спецификации (кроссбраузерно);

> SyntheticEvent, a cross-browser wrapper around the browser’s native event. It has the same interface as the browser’s 
native event, including stopPropagation() and preventDefault(), except the events work identically across all browsers.

## Data Fetching

> React itself doesn’t have any allegiance to any particular way of fetching data.

- axios (promises)
- fetch (browser api)

```javascript
componentDidMount() {
    axios.get(`http://www.reddit.com/r/${this.props.subreddit}.json`)
      .then(res => {
        const posts = res.data.data.children.map(obj => obj.data);
        this.setState({ posts });
      });
  }
```

## What is the significance of keys in React?

Keys are used for identifying unique Virtual DOM Elements with their corresponding data driving the UI.
They help React to optimize the rendering by recycling all the existing elements in the DOM.
These keys must be a unique number or string, using which React just reorders the elements instead of re-rendering them.
This leads to increase in application’s performance.

## [Context](https://ru.reactjs.org/docs/context.html)

**Контекст** позволяет передавать данные через дерево компонентов без необходимости передавать пропсы на промежуточных уровнях.

### Когда нужно использовать

Контекст разработан для передачи данных, которые можно назвать «глобальными» для всего дерева React-компонентов (например,
текущий аутентифицированный пользователь, UI-тема или выбранный язык). По возможности не используйте его,
так как это усложняет повторное использование компонентов.

**TodoList.js**
```jsx harmony
// --------------------------------------------- parent component -------------------------------------
import React, { PureComponent, createContext } from 'react';

const author = {
    name: 'Roman',
    age: 25
};

const TodoAuthorContext = createContext({ name: 'Bruce', age: 30 }); // default params for context

export const TodoAuthorConsumer = TodoAuthorContext.Consumer;

class TodoList extends PureComponent {
    render () {
        return (
            <div>
                <TodoAuthorContext.Provider value={author}>
                    <ul>
                        {this.props.todos.map((todo, index) =>
                            <TodoItem
                                key={`key-${index}`}
                                item={todo}
                                index={index}
                                toggleTodoStatus={this.toggleTodoStatus}
                            />
                        )}
                    </ul>
                </TodoAuthorContext.Provider>
            </div>
        );
    }
}
```

**TodoBody.js**
```jsx harmony
// ------------------------------------- child component -------------------------------------
import { TodoAuthorConsumer } from './TodoList';

class TodoBody extends PureComponent {
    render () {
        return (
            <TodoAuthorConsumer>
                {(author) => (
                    <Fragment>
                        <span>{this.props.text}</span>,
                        <span>Author: {author.name}: {author.age}</span>
                    </Fragment>
                )}
            </TodoAuthorConsumer>
        );
    }
}
```

```js
const TodoAuthorContext = createContext({ name: 'Bruce', age: 30 }); // default params for context
```

Если вы не определите `<TodoAuthorContext.Provider value={author}>` который бы обварачивал `<TodoAuthorConsumer>`,
то значение `author` будет взято из дефолтного. В случае если же `Consumer` был обернут в `Provider`, то это дефолтное
значение не учитывается.  

Особое внимание на синтаксическую конструкцию при использовании `Consumer`

```jsx harmony
<TodoAuthorConsumer>
    {(author) => (
        <Fragment>
            <span>{this.props.text}</span>,
            <span>Author: {author.name}: {author.age}</span>
        </Fragment>
    )}
</TodoAuthorConsumer>
```


## High-Order Component

[link](https://ru.reactjs.org/docs/higher-order-components.html)

**Компонент высшего порядка (Higher-Order Component, HOC)** — это один из продвинутых способов для повторного использования логики.
HOC не являются частью API React, но часто применяются из-за композиционной природы компонентов.

> Компонент высшего порядка — это функция, которая принимает компонент и возвращает новый компонент

```javascript
const EnhancedComponent = higherOrderComponent(WrappedComponent);
```

**Usage example:**

```javascript
const CommentListWithSubscription = withSubscription(
  CommentList,
  (DataSource) => DataSource.getComments()
);

const BlogPostWithSubscription = withSubscription(
  BlogPost,
  (DataSource, props) => DataSource.getBlogPost(props.id)
);
```

**Example of HOC:**

```javascript
// Это функция принимает компонент...
function withSubscription(WrappedComponent, selectData) {
  // ...и возвращает другой компонент...
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.state = {
        data: selectData(DataSource, props)
      };
    }

    componentDidMount() {
      // ...который подписывается на оповещения...
      DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      DataSource.removeChangeListener(this.handleChange);
    }

    handleChange() {
      this.setState({
        data: selectData(DataSource, this.props)
      });
    }

    render() {
      // ... и рендерит оборачиваемый компонент со свежими данными!
      // Обратите внимание, что мы передаём остальные пропсы
      return <WrappedComponent data={this.state.data} {...this.props} />;
    }
  };
}
```


## Composition vs Inheritance

При наследовании мы передаем потомку полный функционал его родителя, плюс привязывает к определенному интерфейсу.
То есть потомок будет иметь функционал по тем же методам и полям что и у родителя.

При этом используя композицию мы можем выборочно добавлять ту или иную логику компоненту, можно и с другими ключами

```javascript
import React, { Component } from 'react';
import DataSource from '...';

export default function dataProvider (WrappedComponent, selectDataHandler) {
    return class extends Component {
        constructor (props) {
            super(props);
            this.state = {
                data: []
            };
        }

        async componentDidMount () {
            const data = await DataSource.getList();
            this.setState({ data });
        }

        render () {
            return (
                <WrappedComponent
                    {...this.props}
                    data={this.state.data}
                    getItem={DataSource.getEntityById}
                />
            );
        }
    }
}
```

В этом примере мы передали `WrappedComponent` как prop `getItem` логику методу на получение одной айтемы.
А не полностью всю логику обьекта DataSource.
А также добавили поле `data` тем самым расширив `WrappedComponent`

## Styling in React

- Add css-classes using `className` property;  
    Also u may use lib [classnames](https://www.npmjs.com/package/classnames#usage-with-reactjs)
    which simplify class list generation by simple rules

- Use [inline styles](https://reactjs.org/docs/dom-elements.html#style)
    
    ```javascript
    render () {
        const wrapperStyle = {
            color: 'red',
            width: '100%',
            backgroundColor: 'purple',
            WebkitTransition: 'all', // note the capital 'W' here
            msTransition: 'all' // 'ms' is the only lowercase vendor prefix
        };
        return (
            <div style={wrapperStyle} className='comments-list'/>
        );
    }
    ```
  

## React Router

Для корректной работы данного компонента нужно использовать компонент обертку (BrowserRouter, Router)

Для привязки роут-компонент используется следующая структура

```js
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

// pre-defined components
import Index from './components/Index';
import Contact from './components/Contact';
import Feedback from './components/Feedback';
```

```html
function App() {
  return (
      <Router>
          <div className="app-wrapper">
              <header className="app-header flex justify-start align-center">
                  <img src={logo} className="app-logo" alt="logo"/>

                  <nav>
                      <ul className='flex row'>
                          <li><Link to="/">Home</Link></li>
                          <li><Link to="/contact/">Contacts</Link></li>
                          <li><Link to="/feedback/">Feedback</Link></li>
                      </ul>
                  </nav>
              </header>

              <div className="content-wrapper">
                  <p>My custom layout</p>

                  <Route path="/" exact component={Index} /> /* Renders: This is index page (home page) */
                  <Route path="/contact/" component={Contact} /> /* Renders: Here info contact information about this site */
                  <Route path="/feedback/" component={Feedback} /> /* Renders: You can leave feedback on this page */
              </div>
          </div>
      </Router>
  );
}
```

### Route

Как видно из примера компонент `<Route/>` имеет несколько параметров

- **path** - сообственно какому путю соответсвует даный компонент;
    При этом можно указать и несколько роутов:
    
    ```jsx harmony
    <Route path="/users/:id" component={User} />
    <Route path={["/users/:id", "/profile/:id"]} component={User} />
    ```
    Routes without a path always match.


- **component** - компонент который будет отображатся по роуту;  
    Но как быть в случаях когда вам нужно помимо указания самого компонента нужно ему также передать props.
    Сделать это лучше используя атрибут `render`
    
    ```jsx harmony
    <Route
      path='/dashboard'
      render={(props) => <Dashboard {...props} isAuthenticated={true}
    />
    ```
    
    Использововать инлайн-функцию для генерации компонента под роут не лучшее решение. Почему?  
    
    > When you use component (instead of render or children, below) the router uses `React.createElement` to create a
    new React element from the given component.
    That means if you provide an inline function to the component prop, you would create a new component every render

- **exact** - роут будет отображен при *полном соответсвии* с указаным роутом;  

    ```html
    <Route path="/users" component={Users} />
    <Route path="/users/create" component={CreateUser} />
    ```
    
    Например имею такую структуру роутов при вводе в адрессную строку `/users/create` роутер найдет в свою очередь два соответсвия.
    
    React Router ищет не полную точность, а partial matching(частичное соответсвие) этим и обьясняется такое поведение.
    
    ```html
    <Route path="/" component={Index} /> /* Renders: This is index page (home page) */
    <Route path="/contact/" component={Contact} /> /* Renders: Here info contact information about this site */
    <Route path="/feedback/" component={Feedback} /> /* Renders: You can leave feedback on this page */
    ```
    
    В такой структуре например, где `<Route path="/" component={Index} />` первый роут указан без атрибута `exact`,
    при вооде в адрессную строку `http://localhost:3000/contact` мы увидим два текста:
    
    ```
    This is index page (home page)
    Here info contact information about this site
    ```
 
- **strict** - будет ли учитан trailing slash *(/)* при проверке соответсвий в роутинге с location.pathname
    
    ```html
    <Route strict path="/one/" component={About} />
    ```
    
    | path  | location.pathname | matches? |
    |-------|-------------------|----------|
    | /one/ | /one              | no       |
    | /one/ | /one/             | yes      |
    | /one/ | /one/two          | yes      |
    
    > Warning: strict can be used to enforce that a location.pathname has no trailing slash,
    but in order to do this both strict and exact must be true.
    
    | path | location.pathname | matches? |
    |------|-------------------|----------|
    | /one | /one              | yes      |
    | /one | /one/             | no       |
    | /one | /one/two          | no       |


### Switch

Отображает только первый результат который соответсвует адрессу.

Представте что мы захотели добавить обработку 404, в случае если ни один роут не подходит.
После добавления `<Route component={NotFound} />` при вводе некорректного роута мы увидим layout компонента `NotFound`

```jsx harmony
<Router>
    <div className="app-wrapper">
        <Route path="/" component={Index} exact />
        <Route path="/contact" component={Contact} />
        <Route path="/feedback" component={Feedback} />
        <Route component={NotFound} />
    </div>
</Router>
```

Но вот незадача, имея данную разметку и перейдя например по роуту `/contact` мы увидим на экране разметку двух компонентов: `Contact` & `NotFound`.
Почему? Потому что для последнего роута мы не указали `path` и как результат он соответсвует всем роутам.
Помочь в этом может использование `<Switch/>`. Который отрисует первое соответсвие(match).

```jsx harmony
<Router>
    <div className="app-wrapper">
        <Switch>
            <Route path="/" component={Index} exact />
            <Route path="/contact" component={Contact} />
            <Route path="/feedback" component={Feedback} />
            <Route component={NotFound} />
        </Switch>
    </div>
</Router>
```

### Prompt

Используется для того чтобы отловить момент ухода со страницы.  
- **message** - obvious
- **when** - [bool] определяет следует ли превентить уход

### Link vs NavLink

Оба этих компонента предоставляют возможность навигации по апликейшену.

#### Link

```jsx harmony
<Link to="/about">About</Link>

<Link
  to={{
    pathname: "/courses",
    search: "?sort=name",
    hash: "#the-hash",
    state: { fromDashboard: true }
  }}
/>
```

Также есть возможность получить внутреннюю ссылку на сам элемент `<a>` используя `innerRef`

```jsx harmony
const anchorRef = React.createRef();

<Link to="/" innerRef={anchorRef} />
```

#### NavLink

Специальная версия `<Link/>` которая позволяет дополнительно стилизировать линки через атрибуты

```jsx harmony
<nav>
  <ul className='flex row'>
      <li><NavLink to="/" exact activeClassName="selected-home">Home</NavLink></li>
      <li><NavLink to="/contact" activeClassName="selected-contact">Contacts</NavLink></li>
      <li><NavLink to="/feedback" activeClassName="selected-feedback">Feedback</NavLink></li>
      <li><NavLink to="/user/69" activeClassName="selected-user">User69</NavLink></li>
  </ul>
</nav>
```

**activeClassName** - определяет класс который будет дан элементу когда ссылка станет активной (по дефолту `active`)
**activeStyle** - определяет стили которые будет применены к элементу когда ссылка станет активной

Также вы можете использовать уже знакомые свойства для уточнения поиска совпадений:
- **strict**
- **exact**
- **isActive** - ф-ия в которой можно описать дополнительную логику для определения стоит ли маркровать сссылку как активную

### Query params

Вы можете иметь доступ к query params посредством свойства props.match.params.  
Как и показано ниже в примере.  

```html
<Route route="/user/:userId" component={User} />
```

```javascript
class User extends PureComponent {
    constructor (props) {
        super(props);
        this.user = this.getUserById(props.match.params.userId);
    }
    
    render () { /*...*/ }
}
```


## Performance

- Production сборка для деплоя на енв;
- [Анализ нагрузки используя табу "performance" в хроме](https://ru.reactjs.org/docs/optimizing-performance.html#profiling-components-with-the-chrome-performance-tab);
- Виртуализация больших обьемов информации (react-virtualized);
- smart метод жизненного цикла **shouldComponentUpdate()** (prefer usage of PureComponent);
- Иммутабельные данные

Использование иммутабельных данных позволяет вам спокойно использовать PureComponent, что позволит избежать ситуации
когда ре-рендер не произошел из-за скрытой мутации данных.

Иммутабельные структуры данных предоставляют вам дешёвый способ отслеживания изменений в объектах и всё, что вам нужно
для реализации shouldComponentUpdate. В большинстве случаев это даст вам хороший прирост в производительности.


## React reselect

Когда мы хотим передать что-то из redux-store в компоненту, мы пишем mapStateToProps. 

```javascript
import { connect } from 'react-redux'
import { toggleTodo } from '../actions'
import TodoList from '../components/TodoList'

const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return todos
    case 'SHOW_COMPLETED':
      return todos.filter(t => t.completed)
    case 'SHOW_ACTIVE':
      return todos.filter(t => !t.completed)
  }
}

const mapStateToProps = state => {
  return {
    todos: getVisibleTodos(state.todos, state.visibilityFilter)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
}

const VisibleTodoList = connect(mapStateToProps, mapDispatchToProps)(TodoList)

export default VisibleTodoList
```

**mapStateToProps** вызывается каждый раз когда что-то поменялось в state.
Но было бы круто если бы этот метод срабатывал только тогда когда были изменены только те части, с которыми мы работаем.
Это соответсвенно и позволяет сделать reselect, указав зависимости на которые нужно смотреть.


```javascript
// selectors/index.js
import { createSelector } from 'reselect';

const getVisibilityFilter = state => state.visibilityFilter;
const getTodos = state => state.todos;

export const getVisibleTodos = createSelector(
  [getTodos, getVisibilityFilter],
  (visibilityFilter, todos) => {
    switch (visibilityFilter) {
      case 'SHOW_ALL':
        return todos
      case 'SHOW_COMPLETED':
        return todos.filter(t => t.completed)
      case 'SHOW_ACTIVE':
        return todos.filter(t => !t.completed)
    }
  }
)
```

```javascript
import { getVisibleTodos } from '../selectors';

// ...

const mapStateToProps = state => {
  return {
    todos: getVisibleTodos(state)
  }
}
```

- Селекторы эффективны. Селектор не производит вычислений, пока один из его аргументов не изменился.
- Селекторы являются составными. Они могут использоваться в качестве входных для других селекторов.

```javascript
import { createSelector } from "reselect";

const shopItemsSelector = state => state.shop.items;
const taxPercentSelector = state => state.shop.taxPercent;

const subtotalSelector = createSelector(shopItemsSelector, items =>
  items.reduce((acc, item) => acc + item.value, 0)
);

const taxSelector = createSelector(
  subtotalSelector,
  taxPercentSelector,
  (subtotal, taxPercent) => subtotal * (taxPercent / 100)
);

export const totalSelector = createSelector(
  subtotalSelector,
  taxSelector,
  (subtotal, tax) => ({ total: subtotal + tax })
);
```


## React saga

Saga - это библиотека, цель которой - облегчить управление побочными эффектами приложения (то есть асинхронными вещами, такими как выборка данных,
и нечистыми вещами, такими как доступ к кешу браузера), эффективнее выполнять, легко тестировать и лучше обрабатывать сбои.

**Подключение**
Это по факту middleware, такая же как и react-thunk, которая передается когда вы создаете redux-store.


```javascript
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

// ...
import { helloSaga } from './sagas'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  reducer,
  applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(helloSaga)
```

Саги состоят из:
- **watcher**: следит когда какой-то redux-action будет задиспачен и позволяет выполнить свой side-effect после него
- **worker**: описывает те действия которые нужно исполнить когда какой-то action был задиспачен.

```javascript
// sagas.js
import {takeEvery, put, call} from 'redux-saga';
import {LOAD_DATA_START} from '../actions.constant';
import {loadDataSuccess} from '../actions';

function fetchData () {
    // ... async call to get data
}

function* workerLoadData () {
    const data = yield call(fetchData);

    yield put(loadDataSuccess(data));
}

export function* watcherLoadData () {
    yield takeEvery(LOAD_DATA_START, workerLoadData);
}
```

*takeEvery* - обработать все подряд
*takeLatest* - обработать последнюю чтобы был только один обработчик за все время.


**take** - приостанавливает выполенение пока какой-то action не будет задиспачен

```javascript
import { take, put } from 'redux-saga/effects'

function* watchFirstThreeTodosCreation() {
  for (let i = 0; i < 3; i++) {
    const action = yield take('TODO_CREATED')
  }
  yield put({type: 'SHOW_CONGRATULATION'})
}
// 'SHOW_CONGRATULATION' will be dispatched after three times TODO_CREATED
```

**call** - метод call вернет только объект описывающий эту операцию и redux-saga сможет позаботиться о вызове и возвращении результатов в функцию-генератор.

**fork** - не блокирует слежение за другими событиями
Если пока обрабатывается однин action, был сгенерирован другой, то он может быть не обработан из-за `call()`. Так как он полностью блокирует поток пока он не будет выполнен.

**select** - позволяет получить redux-state по селектору


```javascript
// from 'selectors.js';
export const getCart = state => state.cart;

/// получить для саги данные по определенному полю
const cart = yield select(getCart)
```


## React Fiber

главная особенность это инкрементный рендеринг: способность разделять работу рендера на единицы и распределять их между множественными фреймами.


Другие ключевые фичи включают 
- возможность приостановки, отмены или переиспользования входящих обновлений DOM дерева
- возможность приоритизации разных типов обновлений
- также — согласование примитивов.


**Reconciliation** - это алгоритм React, используемый для того, чтобы отличить одно дерево елементов от другого для определения частей, которые нужно будет заменить.


- В текущей реализации React проходит дерево рекурсивно и вызвает функции рендеринга на всем обновленном дереве в ходе одного тика (16 мс). 

- Разные типы апдейтов имеют разные приоритеты – обновления анимации должны заканчиваться быстрее чем, скажем, обновление данных хранилища.


Fiber — позволить React воспользоваться планированием. Конкретно, нам нужно иметь возможность:

- остановить работу и вернуться к ней позже.
- приоритизировать разные типы работы.
- переиспользовать работу проделанную ранее.
- отменить работу, если она больше не нужна.


Если вы полагаетесь только на стек вызовов, работа продолжится пока стек не будет пуст.

Не было бы прекрасно если бы мы могли настроить поведение стека вызовов чтоб оптимизировать отображение частей пользовательского интерфейса? Было бы здорово если бы мы могли прервать стек вызова, чтобы манипулировать контейнерами вручную?


Это и есть призвание React Fiber. Fiber — это новая реализация стека, подстроенная под React компоненты. Вы можете думать об одном волокне как о виртульном стек-контейнере.


Если говорить конкретно, то **«волокно»** это JavaScript обьект, который содержит информацию о компоненте, его вводе и выводе.

Оно содержит инфу о:
- тип
- ключ
- child & sibling
- return value
