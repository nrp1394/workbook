# Redux

A predictable state container for JavaScript apps.

Его целью было создание библиотеки управления состоянием с минимальным API, но вполне предсказуемым поведением,
так чтобы можно было реализовать протоколирование, горячую перезагрузку, путешествия во времени, универсальные приложения,
запись и воспроизведение, без каких-либо вложений от разработчика.


## Три принципа Redux

- Единственный источник правды (Общее хранилище данный **Store**)
    Все состояние нашего приложения сведено к дереву обьектов внутри одного хранилища.  
    Универсальность, так как это только **одно место** с состоянием,
    также очевидный плюс в процессе отладки который позволит вам mock даные если потребуется.


- Состояние только для чтения
    Единственный способ изменить состояние - <u>dispatch action</u>.  
    **Action** - это обьект, который описывает как это произойдет изменение store.  
    Это гарантирует централизованое изменение состояния, в порядке очереди без "гонки состояний".  
    Также actions могут быть залогированы, сохранены и затем воспроизведены для отладки или тестирования.


- Мутации написаны как чистые ф-ии - <u>reducers</u>.  
    **Редюсеры** — это просто чистые функции, которые берут предыдущее состояние и действие и возвращают новое состояние.
    Не забывайте возвращать новый объект состояния вместо того, чтобы изменять предыдущее.  
    
    
## Actions

**Действия** — это структуры, которые передают данные из вашего приложения в хранилище.
Они являются единственными источниками информации для хранилища.
Вы отправляете их в хранилище, используя метод <u>store.dispatch()</u>.

```js
{
  type: ADD_TODO,
  text: 'Build my first Redux app'
}
```

Действия должны иметь поле `type`. Типы должны, как правило, определяться, как строковые константы.

```js
import { ADD_TODO, REMOVE_TODO } from '../actionTypes'
```

## Генераторы действий (Action Creators)

Генераторы действий (Action Creators) — не что иное, как функции, которые создают действия.  
Довольно просто путать термины `“action”` и `“action creator”`, поэтому постарайтесь использовать правильный термин.

В Redux генераторы действий (action creators) просто возвращают action:

```js
function addTodo(text) {
  return {
    type: ADD_TODO,
    text
  }
}
```

## Редюсеры (Reducers)

Получая аргументы одного типа, редюсер должен вычислять новую версию состояния и возвращать ее. Никаких сюрпризов.
Никаких сайд-эффектов. Никаких обращений к стороннему API. Никаких изменений (mutations).
Только вычисление новой версии состояния.  

В наведеном примере в сторе мы будем иметь только два поля:
```js
store = {
    visibilityFilter: '',
    todos: []
}
```

Также в примере мы вынесли отдельно обработку каждого поля в отдельную ф-ию,
которую в свою очередь можно вынести в отдельный файл и экспортировать.

```js
import { combineReducers } from 'redux';
import { ADD_TODO, TOGGLE_TODO, SET_VISIBILITY_FILTER, VisibilityFilters } from './constants';

const initialState = {
    visibilityFilter: VisibilityFilters.SHOW_ALL,
    todos: []
};

// ------------------------------------ EXPORT BLOCK -----------------------------------------

// 1)
// export default function todoReducers (state = initialState, action) {
//     return {
//         visibilityFilter: visibilityFilter(state.visibilityFilter, action),
//         todos: todos(state.todos, action)
//     }
// }

// 2)
const todoReducers = combineReducers({
    visibilityFilter,
    todos
});
export default todoReducers;

// -------------------------------------------------------------------------------------------

function todos (todos = initialState.todos, action) {
    switch (action.type) {
        case ADD_TODO:
            return [
                ...todos,
                { text: action.text, completed: false }
            ];

        case TOGGLE_TODO: {
            const newTodos = [ ...todos ];
            newTodos[ action.index ].completed = !newTodos[ action.index ].completed;
            return {
                todos
            }
        }

        default:
            return todos;
    }
}

function visibilityFilter (visibilityFilter = initialState.visibilityFilter, action) {
    switch (action.type) {
        case SET_VISIBILITY_FILTER:
            return action.filter;

        default:
            return visibilityFilter;
    }
}
```

Более детально рассмотрим блок с экспортом редьюсеров:

```js
import { combineReducers } from 'redux';

// ------------------------------------ EXPORT BLOCK -----------------------------------------

// 1)
// export default function todoReducers (state = initialState, action) {
//     return {
//         visibilityFilter: visibilityFilter(state.visibilityFilter, action),
//         todos: todos(state.todos, action)
//     }
// }

// 2)
const todoReducers = combineReducers({
    visibilityFilter,
    todos
});
export default todoReducers;

// -------------------------------------------------------------------------------------------
```

Мы можем сделать это как описано в двух блоках.

`combineReducers()` — генерирует функцию, которая вызывает ваши редюсеры, передавая им в качестве одного из аргументов срез глобального состояния,
который выбирается в соответствии с именем его ключа в глобальном состоянии, и затем снова собирает результаты всех вызовов в один объект.  

Например следующие куски кода равноценны:

```js
const reducer = combineReducers({
  a: doSomethingWithA,
  b: processB,
  c: c
})
```

```js
function reducer(state = {}, action) {
  return {
    a: doSomethingWithA(state.a, action),
    b: processB(state.b, action),
    c: c(state.c, action)
  }
}
```

При этом каждый из этих дочерних редюсеров управляет только какой-то одной частью глобального состояния.


## Хранилище (Store)

**Хранилище (Store)** — это объект, который соединяет эти части вместе. Хранилище берет на себя следующие задачи:

- содержит состояние приложения (application state);
- предоставляет доступ к состоянию с помощью getState();
- предоставляет возможность обновления состояния с помощью dispatch(action);
- регистрирует слушателей (listeners) c помощью subscribe(listener).

Source code:

```js
import { createStore } from 'redux'
import todoApp from './reducers';

let store = createStore(todoApp);
```

### createStore()

<u>createStore(reducer, [preloadedState], [enhancer])</u>  

**reducer** (Function): Функция редюсера которая возвращает следующее дерево состояния, принимая текущее состояние и действие к обработке.

**[preloadedState]** (any): Начальное состояние. Вы можете дополнительно указать это для гидрирования состояния с
сервера в универсальных приложениях или для восстановления предыдущей сериализированной сессии пользователя.
Если вы создали редюсер с помощью combineReducers - это должно быть простым объектом с той же формой, как и ключи переданные ему.

**[enhancer]** (Function): Расширитель хранилища. Вы можете дополнительно указать это, чтобы расширить хранилище такими сторонними возможностями,
как мидлвары (middleware), путешествия во времени, персистентность, и т.д. Единственный расширитель хранилища, который поставляется с Redux, это applyMiddleware()


### applyMiddleware(...fn)

Middleware - это предлагаемый способ расширения Redux с помощью настраиваемых функций.
Он позволяет вам обернуть метод хранилища dispatch для пользы и дела. 

Например:
- <u>*redux-thunk*</u> позволяет генераторам действий инвертировать управление вызывая функции.
    Они будут получать *dispatch* как аргумент и могут вызывать его асинхронно.
    Такие функции называются преобразователями (thunks);
    
- <u>*redux-promise*</u>
    он позволяет вам вызывать асинхронное действие c Promise и вызывать обычные действия, когда промис вернет resolve;
    
- или же вы можете указать middleware которая будет логировать все ваши actions.

## Использование с React

### Инициализировать store(хранилище)

**store.js**

```js
import { createStore, applyMiddleware } from 'redux';
import todoReducers from './reducers';
import logger from 'redux-logger';

export default createStore(
    todoReducers,
    applyMiddleware(logger)
);
```

Также в этом примере мы добавляем миддлвару для логирования actions

### Подключить store(хранилище) к приложению

**src/index.js**

```js
import { Provider } from 'react-redux';
import store from './redux/store';

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
```

### Привязать store к компоненту

Представим что у нас уже есть описана компонента Todo, которую мы импортим.

```js
import Todo from './Todo';
import { connect } from 'react-redux';
import { addTodo, toggleTodo } from '../../redux/actions';

const mapStateToProps = (state) => ({
    todos: state.todos
});

const mapDispatchToProps = (dispatch) => ({
    addTodo: (text) => dispatch(addTodo(text)),
    toggleTodo: (index) => dispatch(toggleTodo(index))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Todo);
```

## Redux Thunk

**Redux thunk** is middleware that allows us to write action creators that return a function instead of an action.
The thunk can then be used to delay the dispatch of an action if a certain condition is met.
This allows us to handle the asyncronous dispatching of actions.
The inner function receives the store methods dispatch and getState as parameters.

```js
import { createStore, applyMiddleware } from 'redux';
import todoReducers from './reducers';
import logger from 'redux-logger';
import thunk from 'redux-thunk';

export default createStore(
    todoReducers,
    applyMiddleware(thunk, logger)
);
```

### Написание собственной миддлвары

#### Пример логгера

```js
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
// import logger from 'redux-logger';
import thunk from 'redux-thunk';

const logger = store => next => action => {
    // action - {type: "SET_TODOS", payload: Array(200)}
    // next - fn dispatch
    
    console.log('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    return result
};

export default createStore(
    reducers,
    applyMiddleware(thunk, logger)
); 
```

#### Пример crash reporter

```js
const crashReporter = store => next => action => {
  try {
    return next(action)
  } catch (err) {
    console.error('Caught an exception!', err)
    Raven.captureException(err, {
      extra: {
        action,
        state: store.getState()
      }
    })
    throw err
  }
}
```