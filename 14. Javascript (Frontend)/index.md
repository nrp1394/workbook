# Javascript (Frontend)

## Bubbling and capturing

Bubbling
![Bubbling](media/event-order-bubbling.svg)

Capturing
![Capturing](media/event-order-capturing.gif)

- stopPropagation() - запретить распространение событий дальше
- stopImmediatePropagation - препятствует продвижению события дальше и полностью останавливает обработку события другими обработчиками для этого же элемента

```javascript
element.addEventListener('click', handler, true) // use capture
```

- event.target – самый глубокий элемент, на котором произошло событие.
- event.currentTarget (=this) – элемент, на котором в данный момент сработал обработчик (тот, на котором «висит» конкретный обработчик)
- event.eventPhase – на какой фазе он сработал (погружение=1, фаза цели=2, всплытие=3).


## SPA
Это web-приложение, размещенное на одной странице, которая для обеспечения работы загружает все javascript-файлы (модули, виджиты, контролы и т.д.), а также файлы CSS вместе с загрузкой самой страницы.

**Плюсы**
Если приложение достаточно сложное и содержит богатый функционал, как например, система электронного документооборота, то количество файлов со скриптами может достигать нескольких сотен, а то и тысяч. А “…загрузка всех скриптов…” никоим образом не означает, что при загрузке сайта будут загружены сразу все сотни и тысячи файлов со скриптами. Для решения проблемы загрузки большого количества скриптов в SPA призван API под названием AMD. AMD реализует возможность загрузки скриптов по требованию. То есть, если для “главной станицы” одностраничного портала потребовалось 3 скрипта, они будут загружены стразу перед стартом программы. А если пользователь кликнул на другую страницу одностраничного портала, например, “О программе”, то принцип AMD загрузит модуль (скрипт + разметка) только перед тем как перейти на эту страницу.

Первым плюсом стоит отметить тот факт, что приложения на SPA отлично работают на устройствах как стационарных, так и мобильных. “Большие” компьютеры, планшеты, смартфоны, и, в конце-концов, простые телефоны (некоторые) могут беспрепятственно работать с сайтами построенных по принципу SPA. Итак, первый “плюс” – работа на большом количестве устройств, а значит, создав одно приложение, вы получаете гораздо большую аудиторию пользователей нежели при использовании стандартного подхода.

Далее второй “плюс” – богатый пользовательский интерфейс, так называемый User Experience. Так как web-страница одна, построить богатый, насыщенный пользовательский интерфейс гораздо проще. Проще хранить информацию о сеансе, управлять состояниями представлений (views) и управлять анимацией (в некоторых случаях).

Третий “плюс” – SPA существенно (в разы) сокращает так называемые “хождения по кругу”, то есть загрузку одного и того же контента снова и снова. Если ваш портал (сайт) использует шаблон, то вместе с основным содержанием какой-либо страницы посетитель сайта обязательно загружает разметку шаблона. Да, кэширование данных на данном этапе развития WWW достигло высочайших результатов, но если нечего кэшировать, то и время, и ресурсы на это не тратятся.

**Минусы**
1. Сильно усложняет front-end часть приложения. Кроме html и логики UI тут еще и роутеры, MVC и прочие сладости.
2. Так как у приложения одна точка входа, существует риск того, что одна ошибка может привести к нерабочему состоянию всего приложения.
3. Дублирование роутеров (по сравнению с классическим подходом)
4. SEO


## HTTP / HTTPS / WS

**HTTP** — протокол передачи данных, изначально предназначенный для передачи гипертекстовых документов (то есть документов, которые могут содержать ссылки, позволяющие организовать переход к другим документам).

почему HTTP не использует шифрование по умолчанию:

- Для этого требуется больше вычислительных мощностей
- Передается больше данных
- Нельзя использовать кеширование


**HTTPS** (HTTP over TLS) - тот же HTTP но с другим портом по умолчанию и дополнительным шаром шифрования/аутентификации между HTTP и TCP.

Поскольку HTTPS это фактически HTTP, который передается через SSL или TLS, то почти все его основные элементы шифруются: 
- URL-запросы, включая путь и название ресурса (страницы)
- параметры запроса
- заголовки и куки, которые часто содержат идентификационные данные о пользователе. 

НЕ шифруются: название или адрес хоста (сайта) и порт, поскольку они используются транспортным протоколом TCP / IP для установления соединения.

По сути, сертификаты связывают доменные имена с определенным публичным ключом. Это предотвращает возможность того, что злоумышленник предоставит свой публичный ключ, выдавая себя за сервер, к которому обращается клиент.
При этом сертификат также должен быть подписан соответсвующим центром, который предоставляет этот список браузерам.


**WebSocket** — протокол связи поверх TCP-соединения, предназначенный для обмена сообщениями между браузером и веб-сервером в режиме реального времени.

Протокол WebSocket — это независимый протокол, основанный на протоколе TCP. Он делает возможным более тесное взаимодействие между браузером и веб-сайтом, способствуя распространению интерактивного содержимого и созданию приложений реального времени.


## REST concept

**REST** (от англ. Representational State Transfer — «передача состояния представления») - архитектурный стиль взаимодействия компонентов распределённого приложения в сети. 

Для веб-служб, построенных с учётом REST (то есть не нарушающих накладываемых им ограничений), применяют термин «RESTful».

Требования к архитектуре:

- Модель клиент-сервер
- Отсутствие состояния
    Сервер ничего не знает о состоянии клиента, он просто получает всю информацию в запросе

- Кэширование
- Единообразие интерфейса
    сервер может отсылать данные из базы данных в виде HTML, XML или JSON, ни один из которых не является типом хранения внутри сервера, клиент сам это определяет

- Слои
    Клиент обычно не способен точно определить, взаимодействует он напрямую с сервером или же с промежуточным узлом, в связи с иерархической структурой сетей (подразумевая, что такая структура образует слои). Применение промежуточных серверов способно повысить масштабируемость за счёт балансировки нагрузки и распределённого кэширования. Промежуточные узлы также могут подчиняться политике безопасности с целью обеспечения конфиденциальности информации.

**Additional headers**
- content-type
- mime-type
- cache-control
- cookie
- authorization


## AJAX
(Asynchronous Javascript and XML)

подход к построению интерактив`ных пользовательских интерфейсов веб-приложений, заключающийся в «фоновом» обмене данными браузера с веб-сервером. В результате при обновлении данных веб-страница не перезагружается полностью

**XMLHttpRequest** - обьект который умеет посылать запросы к серверу


## XML vs JSON

XML - язык разметки.
JSON - формат для обмена данными, во множестве языках реализован, как массив данных.

- и там, и там есть варики чтобы распарсить данные
- проще расширять JSON
- мнение что XML проще использовать на серверной стороне, а JSON на клиентской


## JQuery

**selectors**
По факту это CSS селекторы

**events**
```javascript
$("p").click();
```

**work with lists**
- те же методы что и для работы с массивами
- .each()



**jQuery Effect Methods**
- .animate({styles})
- .fadeIn() (показать) / .fadeOut() (скрыть)
- .slideUp() / .slideDown()


**DOM manipulation**
- .append() вставить элемент в конец children
- .prepend() вставить элемент в начало children
- .before() / .after()
- .remove()
- .replaceAll()
- .wrap()


**AJAX**
Это самый основной метод, а все последующие методы лишь обертки для метода jQuery.ajax. У данного метода лишь один входной параметр — объект включающий в себя все настройки (выделены параметры которые стоит запомнить):
- async — асинхронность запроса, по умолчанию true
- cache — вкл/выкл кэширование данных браузером, по умолчанию true
- contentType — по умолчанию «application/x-www-form-urlencoded»
- data — передаваемые данные — строка иль объект
- dataFilter — фильтр для входных данных
- dataType — тип данных возвращаемых в callback функцию (xml, html, script, json, text, _default)
- jsonp — переустановить имя callback функции для работы с JSONP (по умолчанию генерируется на лету)
- processData — по умолчанию отправляемые данный заворачиваются в объект, и отправляются как «application/x-www-form-urlencoded», если надо иначе — отключаем
- timeout — время таймаут в миллисекундах
- type — GET либо POST
- url — url запрашиваемой страницы

Локальные AJAX Event'ы:
- beforeSend — срабатывает перед отправкой запроса
- error — если произошла ошибка
- success — если ошибок не возникло
- complete — срабатывает по окончанию запроса

Для организации HTTP авторизации (О_о):
- username — логин
- password — пароль

```javascript
$.ajax({
    method: 'GET',
    url: '/ajax/example.html',             // указываем URL и
    dataType : "json"                    // тип загружаемых данных
})
    .done() // then
    .fail() // catch
    .always(); // finally
```

**Promise**
Очень похоже на то что было у angular1

```javascript
function promiseFn () {
    const deferred = $.Deferred();
    setTimeout(() => {
        deferred.resolve({alpha: true});
    }, 1000);
    return deferred.promise();
}

promiseFn()
    .then(console.log);
```


## URI Handling Function Properties

*decodeURI()* - Decodes a URI
*decodeURIComponent()* - Decodes a URI component
*encodeURI()* - Encodes a URI
*encodeURIComponent()* - Encodes a URI component

```javascript
var uri = "https://w3schools.com/my test.asp?name=ståle&car=saab";
console.log(encodeURI(uri)); // https://w3schools.com/my%20test.asp?name=st%C3%A5le&car=saab
console.log(encodeURIComponent(uri)); // https%3A%2F%2Fw3schools.com%2Fmy%20test.asp%3Fname%3Dst%C3%A5le%26car%3Dsaab
```

Чтобы понять разницу нужно ввести понятия
- Зарезервированные символы:
; , / ? : @ & = + $

- Неизменяемые символы
латинский алфавит, десятичные цифры и - _ . ! ~ * ' ( )

*encodeURI* - заменяет все символы кроме зарезервированых и неизменяемых
*encodeURIComponent* - также заменяет все символы, кроме неизменяемых

То есть у *encodeURIComponent* более широкий спектр заменяемых символов.


## Using underscoreJS/lodash:

**native js vs provided by library methods**

- библиотечные методы уже много предлагают utilities с коробки (с более приятным интерфейсом) (find, get, fill, flatMap, first, last, uniq, debounce, clone)
- плюс они если пишутся - то кроссбраузерно
- также некоторые методы просто заменяют одну операцию, но делает код более читабельным, где вы просто читаете что делается внутри по методам 
- в принципе можно настроить tree shaking через babel & webpack 


- миксованый подход к использованию ф-ий
- еще одна внешняя зависимость
- +1 скрипт на загрузку во время рендеринга страницы


## Same origin policy
**CORS**
Cross Origin Resource Sharing
Поведение дефолту блочить ответ со стороны сервера если domain отличается от серверного
alpha.com -> beta.com запрос будет сфейлен
alpha.com/test -> alpha.com - ok

Но CORS - это спецификация W3C позволяющая обойти это ограничение путем настроек сервера и клиента
На стороне клиента:
- XMLHttpRequest’s
    `xhr.withCredentials = true;`
    
- сервер со своей стороны должен разрешить получать доступ по credential путем установки 
    `Access-Control-Allow-Credentials: true`
    
- внести соответствующий домен для header `Access-Control-Allow-Origin` на стороне сервера
- middleware cors() для express
    

**JSONP**
JSONP способ для передачи данных в формате JSON, не заботясь о проблемах между доменами.
JSONP означает JSON с Padding.

Это технология для выполнения запроса к другому домену минуя ограничения Cross Origin Domain
Но если XHR запросы на другого домен не работаю, но можно получить JS файл с другого домена, на это браузеры не ругаются.

При этом ответная сторона (example.com) в тело этого скрипта вернет не просто JSON, а вызов javascript функции указанной в jsonp параметре (мы разумеется эту функцию должны заблаговременно объявить).
Но есть ограничение: эта история работает только с `GET` запросами. 
 
**Proxy**
для обхода запрета можно отправить запрос на прокси, который в свою очередь будет иметь доступ к необходимому ресурсу, он обратится к нему и вернет все клиенту.

*Custom headers*
Используя их можно передавать дополнительную информацию об авторизации


## Dynamic UI routing
В современных фреймворках используется HTML5 History API
http://mywebsite/#comments/1
У SPA фреймворках можно было раньше увидеть "#" как индикатор использования нового роута



## CommonJS vs AMD

### AMD

[AMD and RequireJS](https://code.tutsplus.com/tutorials/next-generation-javascript-with-amd-and-requirejs--net-21596)

AMD (Asynchronous Module Definition)

Это только спецификация как писать JS modules, а чтобы загружать файлы нам нужен лоадер - requirejs например.

- стандартный путь как писать куски функционала
- легко адаптировать код других людей под шаблон
- так как структура определена мы можем использовать единый подход как загружать и обрабатывать JS-модули
- возможность писать несколько модулей в одном файле при этом каждый из них инкапсулирован
    
**Пример**

file structure
```
test
|-- _requirejs
|   |-- require.js // lib from the site
|   |-- main.js
|   |-- utils.js
|-- index.html
```

```html
<html>
<head>
    <script data-main="requirejs/main" src="requirejs/require.js"></script>
</head>
<body/>
</html>
```

*data-main* - definition of the entry point

```javascript
// main.js
// ['utils'] cause utils.js on the level (path from main.js not from index.html(requirejs/utils) ) 
require(['utils'], function(utils) { 
    console.log('main module', utils);
});
```

```javascript
// utils.js
define(function() {
    return {
        alpha: () => 'alpha',
        beta: () => 'beta'
    };
});
```

**Немного лирики:**

*define* is used to define named or unnamed modules based on the proposal using the following signature:

```javascript
define(
    module_id /*optional*/, 
    [dependencies] /*optional*/, 
    definition function /*function for instantiating the module or object*/
);
```

*require* on the other hand is typically used to load code in a top-level JavaScript file or within a module should you wish to dynamically fetch dependencies

```javascript
require(['foo', 'bar'], function ( foo, bar ) {
        // rest of your code here
        foo.doSomething();
});
```

*Dynamically-loaded Dependencies*

```javascript
define(function ( require ) {
    var isReady = false, foobar;
 
    // note the inline require within our module definition
    require(['foo', 'bar'], function (foo, bar) {
        isReady = true;
        foobar = foo() + bar();
    });
 
    // we can still return a module
    return {
        isReady: isReady,
        foobar: foobar
    };
});
```


### CommonJS
Это также подход к написанию JS модулей, который для работы требует лоадеров на стороне клиента.

```javascript
var lib = require('package/lib');
 
function foo() {
    lib.log('hello world!');
}
 
exports.foo = foo;
```

### AMD vs. CommonJS
AMD применяет подход browser-first в разработке, выбирая асинхронное поведение и упрощенную обратную совместимость, но не имеет никакой концепции файлового ввода-вывода. Он поддерживает объекты, функции, конструкторы, строки, JSON и многие другие типы модулей, работающие изначально в браузере.

CommonJS, с другой стороны, использует серверный подход, предполагающий синхронное поведение. Под этим мы подразумеваем, что, поскольку CJS поддерживает unwrapped модули(при AMD мы оборачуем модули), он может чувствовать себя немного ближе к спецификациям ES.next/Harmony, освобождая вас от оболочки define(), которую использует AMD.


## WebWorker
Предоставляют собой средство запуска скриптов в отдельном потоке.
Workers запускаются в другом глобальном контексте, отличающемся от текущего, - window. Поэтому использование переменной window для получения текущего глобального контекста (вместо self) внутри Worker вернет ошибку.

```javascript
// doWork.js
self.addEventListener('message', function(event) {
    var data = event.data;

    switch (data.cmd) {
        case 'average':
            // do some calculations
            self.postMessage('some result'); // вернуть что-то в основной поток
            break;
        case 'exit':
            self.close(); // остановить webWorker
            break;
        default:
            self.postMessage('Unknown command');
    }
}, false);
```

```html
<button onclick="startComputation()">Start computation</button>
```

```javascript
function startComputation() {
    worker.postMessage({'cmd': 'average', 'data': [1, 2, 3, 4]});
}

var worker = new Worker('doWork.js');

// worker.terminate(); // остановить WebWorker

worker.addEventListener('message', function(e) {
    console.log(e.data);
}, false);

// обработать ошибки
worker.addEventListener('error', onError, false);
```

Worker потоки имеют доступ к глобальной функции, `importScripts()`, которая позволяет импортировать скрипты с того же домена в их область видимости.

```javascript
importScripts('foo.js');
```

**WebWorker'aм доступны**
- Объект navigator
- Объект location (только для чтения)
- XMLHttpRequest
- setTimeout()/clearTimeout() и setInterval()/clearInterval()
- Кэш приложения
- Импорт внешних скриптов с использованием importScripts()
- Создание других веб-воркеров

**WebWorker'ы ограничены**
- DOM (это не потокобезопасно)
- Объект window
- Объект document
- Объект parent

Передача данных между главной страницей и worker-ом происходит путем копирования, а не передачи по ссылке. Объекты сериализуются при передаче и затем десериализуются на другом конце. Страница и работник не используют совместно одни и те же экземпляры, для каждого создается свой. Большинство браузеров реализуют это структурированным клонированием (structured cloning).


## Service worker
[Google Service Worker](https://developers.google.com/web/fundamentals/primers/service-workers)

<img src="media/service-worker.jpg" width="800"/>

Service worker фактически выполняет роль прокси-сервера, находящегося между веб-приложением и браузером, а также сетью (если доступна). Он позволяет (кроме прочего) описывать корректное поведение веб-приложения в режиме офлайн, перехватывать запросы сети и принимать соответствующие меры, основываясь на доступности сети, и обновлять данные, находящиеся на сервере при доступе к нему. Также они имеют доступ к push-уведомлениям и API для фоновой синхронизации.

<img src="media/sw-lifecycle.png" width="700"/>

Пример с кешированием файлов при инициализации SW
```javascript
var CACHE_NAME = 'my-site-cache-v1';
var urlsToCache = [
  '/',
  '/styles/main.css',
  '/script/main.js'
];

self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        console.log('Opened cache');
        return cache.addAll(urlsToCache);
      })
  );
});
```

Пример с перехватом запросов
```javascript
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.match(event.request)
      .then(function(response) {
        // Cache hit - return response
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
    )
  );
});
```

```javascript
self.addEventListener('fetch', function(event) {
	var url = event.request.url;
	if (url.indexOf("/update-resource") > 0) {
		var r = new Request(url.replace("\/update-resource", ""));
		return fetchAndCache(r);
	}
});
```


## HTML5 Geolocation
The HTML Geolocation API is used to locate a user's position.

*getCurrentPosition(success, error, options)*
```javascript
var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};
```
- *maximumAge*: целое число (миллисекунды) | infinity - максимальное время кеширования позиции.
- *timeout*: целое число (миллисекунды) - количество времени до вызова callback ошибки. Если 0, вызов не происходит.
- *enableHighAccuracy*: `false` | `true`

```javascript
window.navigator.geolocation.getCurrentPosition(showPosition);

function showPosition (position) {
    console.log(position);
    const response = {
        coords: { // GeolocationCoordinates
            latitude: 50.459545,
            longitude: 30.4011535,
            altitude: null,
            accuracy: 38,
            altitudeAccuracy: null,
            heading: null,
            speed: null
        },
        timestamp: 1584017608002
    };
}
```

*watchPosition()* - returns the current position of the user and continues to return updated position as the user moves (like the GPS in a car).

*clearWatch()* - stops the watchPosition() method.


## History API

- *window.history.length*: Количество записей в текущей сессии истории
- *window.history.state*: Возвращает текущий объект истории
- *window.history.go(n)*: Метод, позволяющий гулять по истории. В качестве аргумента передается смещение, относительно текущей позиции. Если передан 0, то будет обновлена текущая страница. Если индекс выходит за пределы истории, то ничего не произойдет.
- *window.history.back()*: Метод, идентичный вызову go(-1)
- *window.history.forward()*: Метод, идентичный вызову go(1)
- *window.history.pushState(data, title [, url])*: Добавляет элемент истории.
- *window.history.replaceState(data, title [, url])*: Обновляет текущий элемент истории


вызывается, когда пользователь переходит по истории
```javascript
window.addEventListener('popstate', function(e) {}, false);
```

```javascript
// pushState() example
var state = { 'page_id': 1, 'user_id': 5 };
var title = 'Hello World';
var url = 'hello-world.html';

history.pushState(state, title, url);
```


## LocalStorage / SessionStorage / SQLite

**LocalStorage / SessionStorage**
Объекты хранилища localStorage и sessionStorage предоставляют одинаковые методы и свойства:

- *setItem(key, value)* – сохранить пару ключ/значение.
- *getItem(key)* – получить данные по ключу key.
- *removeItem(key)* – удалить данные с ключом key.
- *clear()* – удалить всё.
- *key(index)* – получить ключ на заданной позиции.
- *length* – количество элементов в хранилище.

LocalStorage - время жизни пока юзер сам не почистит его(даже после закрытия браузера)
SessionStorage - время жизни пока юзер не закроет табу браузера

5 Мб для одного источника




**IndexedDB**
Для традиционных клиент-серверных приложений эта мощность обычно чрезмерна. IndexedDB предназначена для оффлайн приложений, можно совмещать с ServiceWorkers и другими технологиями.

```javascript
let openRequest = indexedDB.open("store", 1);

// срабатывает, если на клиенте нет базы данных
// ...выполнить инициализацию...
// создаём хранилище объектов для books, если ешё не существует
openRequest.onupgradeneeded = function() {
  let db = openRequest.result;
  if (!db.objectStoreNames.contains('books')) { // if there's no "books" store
    db.createObjectStore('books', {keyPath: 'id'}); // create it
  }
};


openRequest.onerror = function() {
  console.error("Error", openRequest.error);
};

openRequest.onsuccess = function() {
  let db = openRequest.result;
  // продолжить работу с базой данных, используя объект db
};
```

Курсоры === Итераторы

Транзакция – это группа операций, которые должны быть или все выполнены, или все не выполнены (всё или ничего).

Например, когда пользователь что-то покупает, нам нужно:

Вычесть деньги с его счёта.
Отправить ему покупку.
Будет очень плохо, если мы успеем завершить первую операцию, а затем что-то пойдёт не так, например отключат электричество, и мы не сможем завершить вторую операцию. Обе операции должны быть успешно завершены (покупка сделана, отлично!) или необходимо отменить обе операции (в этом случае пользователь сохранит свои деньги и может попытаться купить ещё раз).

Транзакции гарантируют это.


## FileReader API
window.requestFileSystem(type, size, successCallback, opt_errorCallback)
*type* - window.TEMPORARY(браузер может сам удалить) or window.PERSISTENT(только юзер системы).
*size* - сколько памяти может вам быть выделено

но чтобы использовать PERSISTENT нужно использовать метод на выделение вам квоты

```javascript
window.webkitStorageInfo.requestQuota(PERSISTENT, 1024*1024, function(grantedBytes) {
  window.requestFileSystem(PERSISTENT, grantedBytes, onInitFs, errorHandler);
}, function(e) {
  console.log('Error', e);
});
```

```javascript
function onInitFs(fs) {

    fs.root.getFile('log.txt', {create: true, exclusive: true}, function(fileEntry) {

        // fileEntry.isFile === true
        // fileEntry.name == 'log.txt'
        // fileEntry.fullPath == '/log.txt'

    }, errorHandler);

}
window.requestFileSystem(window.TEMPORARY, 1024*1024, onInitFs, errorHandler);
```

как видно из примера ф-ия onInitFs принимает аргумент `fs` который умеет работать с файловой системой.  

```javascript
function onInitFs(fs) {
  fs.root.getFile('log.txt', {}, function(fileEntry) {

    // Get a File object representing the file,
    // then use FileReader to read its contents.
    fileEntry.file(function(file) {
       var reader = new FileReader();

       reader.onloadend = function(e) {
         var txtArea = document.createElement('textarea');
         txtArea.value = this.result;
         document.body.appendChild(txtArea);
       };

       reader.readAsText(file);
    }, errorHandler);

  }, errorHandler);

}

window.requestFileSystem(window.TEMPORARY, 1024*1024, onInitFs, errorHandler);
```

Как видно из примера у Reader'a есть метод `readAsText` и соответсвующий event `onloadend`.

Добавление данных у файл

```javascript
function onInitFs(fs) {

  fs.root.getFile('log.txt', {create: false}, function(fileEntry) {

    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function(fileWriter) {

      fileWriter.seek(fileWriter.length); // Start write position at EOF.

      // Create a new Blob and write it to log.txt.
      var blob = new Blob(['Hello World'], {type: 'text/plain'});

      fileWriter.write(blob);

    }, errorHandler);

  }, errorHandler);

}

window.requestFileSystem(window.TEMPORARY, 1024*1024, onInitFs, errorHandler);
```

`fileEntry` результат коллбека для getFile обладает методами для работы с файлом, такие как `createWriter`, `remove`


## Graceful Degradation & Progressive Enhancement

Graceful Degradation грубо говоря это сделать так чтобы все классно работало в хроме, и потом постепенно понижая требования, апплаить все изменения. 
Например потом проверить в мозилле, потом в IE на разных версиях. Аля имплементировать самый success case и потом постепенно обрабатывать все возможные варианты.

Progressive Enhancement же в свою очередь: начинать с малово и постепенно наращивать.
В статье говорилось об этапах
1) HTML
2) CSS
3) CSS3
4) JS

Я бы назвал плюсом такого подхода что у вас есть полностью рабочий вариант в каждый момент времени.



## Mobile first
Mobile First это метод разработки оптимизированного вебсайта для различных мобильных устройств с учетом скорости подключения к сети. И важность отображения основного содержания конечному пользователю.

- Показать самое важное содержание в первую очередь
- Вебсайт должен быть легковесным и оптимизированным, т.к. скорость подключения мобильной сети может быть слабой в зависимости от местонахождения пользователя
- Вебсайт не должен загружать больше ресурсов, чем требуется пользователю для получения нужной информации, т.к. мобильный Интернет все еще остается дорогим. Дополнительная информация должна грузиться только по требованию пользователя

суть вопроса: какой у Вас исходный шаблон, и в какую сторону его проще будет адаптировать.

удобнее - начинать с мобильной версии, т.к. она априори "меньше" и расширить элемент гораздо проще, чем "слепить более мелкую его версию".


## Offline first
- максимально сократить засимость от сервева. Чтобы он просто отдавал данные и работал как REST API.
- абстрагировать в код базе логику на получение данных от сервера, а не держать прям том же коде.
- прокси-обьекты для работы с данными

*offline.js* - либа которая помагает отследить потерю интернет связи.


## Data synchronization patterns 

**DATA SYNCHRONIZATION MECHANISM PATTERNS**
when should an application synchronize data between a device and a remote system (such as a cloud server)?

- Asynchronous Data Synchronization
    Скорость и отклик приложения для юзера, выполнение дейтвий на фоне, приложение не заблокировано, но можно применять когда порядок загрузки данных не важен.
    Отправка статистики, юзер нотификации, push-notification
    
- Synchronous Data Synchronization
    Важно когда нам играет роль актуальность данных, которых естественно нужно дождатся. Кейсы когда данные зависят друг от друга, последовательность важна. Такие как банковские операции например. Запасы на складах.
    Писать проще, состояний гонок - нет. Юзер будет заблокан. 
    
    
**DATA STORAGE AND AVAILABILITY PATTERNS**
how much data should be stored? Or how much data should be available without further transfer of data?

- Partial Storage
    Запрашивать данные *по требованию*, хранить только то что нужно на данный момент. Уделить особое внимание освобождению этих ресурсов.
    Много мелких запросов, если у вас проблемы с сетью - то ваш аппликейшн безполезен.
    Google Maps
    
- Complete Storage
    Загрузить все данные до того как они будут использоватся. Плюс даже если инет отвалится - то апп будет все равно рабочим.
    Но когда актуальность данных нам не горит.
    
    
**DATA TRANSFER PATTERNS**
- Full Transfer
    Все данные пересылаются за раз. Можно юзать когда собственно данных не слишком много или когда файл нельзя разбить на части. Изи солюшен.

- Timestamp Transfer (Передача метки времени)
    Запрашиваются только те данных у которых просрочился срок хранения. Гранулярность данных, плюс на них должны также стоять временные метки.
    
- Mathematical Transfer
    Когда проблема с сетью. Обновить только некоторые части с момента последней синхронизации. Синхронизировать данные или нет решается по некоему алгоритму: сравнение checksum, например.


## Progressive Web Apps (PWA)
Techs:
- WebWorker
- Web App Manifest
- App shell
- HTTPS
- Push notifications

**Service worker**
Сердце PWA — Service Worker. Это проксирующий слой между фронтэндом и бэкэндом, находящийся в браузере. Все запросы браузера идут через него. Данное разделение на два независимых слоя позволило сделать переход обычного веб сайта в PWA максимально простым.

Из хранилищ у Service Worker'a есть доступ к Cache Storage для web ресурсов, и IndexDB для данных.

Можно, например, принять запрос от браузера, проверить состояние сети, взять данные из хранилища, произвести с ними операции и вернуть некий результат обратно в браузер — который будет думать, что ответ ему пришел от сервера. Или не будет — как разработчик захочет, так и сделает. Два браузерных слоя (клиентский фронтэнд и Service Worker) позволяют писать полноценные приложения.

В тоже время, для большинства сайтов будет достаточно кэширующей функциональности Service Worker'a, чтобы превратиться в PWA.

**HTTPS**
PWA требует, чтобы все ресурсы сайта передавались по HTTPS протоколу. 

**Application shell**
показ определенного каркаса сайта, как превьюшки в youtube пока все видео подгружаются.

**Web App manifest**
JSON файл, декларативно определяющий для браузера название приложения, иконку, как будет выглядеть PWA (fullscreen, standalone и др.) и некоторые другие параметры. 


## D3.js / highcharts.js

Data-Driven Documents


