## New Tags
*(just general description)*

- **article** - self contained areas on the page, it could be forum post or blog article. Represents a whole and complete composition or entry.

```html
<body>
  <article>
    <header>
      <h1>Benefit of Fruits</h1>
      <p>Author: Nate Clyne</p>
    </header>

    <p>We should eat fruits ...</p>
    <p>...</p>
    <p>...</p>
  </article>
</body>
```

- **aside** - small content areas outside main flow of webpage. Defines a block on the side of the content some additional (not main) information. It usually called like a "sidebar" or "side panel".

```html
<body>
  <article>
    <header>
      <h1>Benefit of Fruits</h1>
      <p>Author: Nate Clyne</p>
    </header>
    
    <aside>
        <h3>List of useful links</h3>
        <ul>
          <li>...</li>
          ...
        </ul>
      </aside>

    <p>We should eat fruits ...</p>
    <p>...</p>
    <p>...</p>
  </article>
</body>
```

- **header** - top block of section or page. Contains content such us navigation links, h1...h6, search ...

- **footer** - bottom block of section or page. A footer typically contains information about its section such as who wrote it, links to related documents, copyright data, etc. It in turn may contain entire sections, with appendices, indexes, license agreements, and other similar content.

- **hgroup** - this element is a semantic method that organizes headers and subheaders

- **nav** - _**section**_ with navigation links, either to other pages or to parts within the same page

- **menu** - is a _**list**_ of commands within the navigation or any other layout section. Designed to display a list of menu items like \<ol\/> or \<ul\/> tags. The list is formed using \<li\> tags.
	- type - (list | context | toolbar) - also semantic attribute just to show what this menu used for

```html
// Example of usage **nav** and **menu**
<body>
  <header>
    <h1>Main label of page</h1>
    
    <nav>
      <ul>
        <li>
          <span>File</span>
          <menu>
            <li>Open</li>
            <li>Save</li>
            <li>Save As</li>
            <li>Import</li>
            <li>Export</li>
            <li>Close</li>
          </menu>
        </li>
        
        <li>
          <span>Edit</span>
          <menu>
            <li>Copy</li>
            <li>Cut</li>
            <li>Paste</li>
          </menu>
        </li>
        
        <li>...</li>
      </ul>
      
      <input type="search">
    </nav>
  </header>
</body>
```

- **section** - distinct content of page

- **figure** - content that contains a figure, such as an image, chart, or picture

- **figcaption** - defines a caption for a <figure> element

- **main** - defines the main content of the body of a document or web app. As such, the main content area holds content that is directly related to or expands upon the central topic of the page. Content should be unique and doesn't contain \<header\>, \<footer\>, \<aside\> or other type block. Its not block element.

```html
<body>
  <article>
    <h1>Benefit of Fruits</h1>

    <main>We should eat fruits ...</main>
    
    <p>...</p>
    <p>...</p>
  </article>
</body>
```

- **mark** - defines text which should be highlighted (bring attention, make accent).

- **wbr** - defines places in which browser can move text for next line

- **time** - semantic representation of some time in webpage
	- datetime - time in some special format (http://htmlbook.ru/html/time/datetime)
	- pubdate - indicates that this date is publication date

- **details** - produces an expandable box to display additional information. The expanding/collapsing behaviour does not depend on scripting, so it should works even if JavaScript is disabled or not supported. If there is no summary tag inside - main caption is *"Details"* and content inside will be expanded/collapsed
	- open - is opened details tag (enough only presence)

- **summary** - An optional element which used for specifying label caption for details tag

- **output** - defines area which displays some information

- **datalist** (doesn't supported by safari, android, ios) - works like autocomplete for search. Available List of available searched values sets using *option* tag 

- **meter** (doesn't supported by IE) - used to display some range affiliation. (Temperature -> hot, normal, cold)

- **progress** - used for displaying state of readiness.
	- min - lower bound of some range
	- max - higher bound of some range
	- value - current position in specified range (if this attr doesn't specified progress render to indicate some progressing)

```html
<!--state of readiness -->
<div class="progress">
    <p>Current progress ...</p>
    <progress max="312" value="22"></progress>
</div>


<!-- processing example -->
<div class="loading">
    <p>Loading ...</p>
    <progress max="10"></progress>
</div>
``` 

- **audio** - gives default html5 player to play some audio file

- **video** - gives default html5 player to play some video file
	-> src
	-> autoplay - start playing the video as soon as it loads;
	-> controls - include its built-in video controls, such as play and pause;
	-> loop - continuously play the video after it completes;
	-> poster - image to show in the place allocated to the video until the user starts to play the video;

- **source**
	-> src;
	-> type - defines MIME-type of source file;
	-> media - defines the device for which the file will be played;

    ```html
        <video controls height="400" width="600" poster="../Javascript%20(Core)/media/poster.jpg">
            <source src="../Javascript%20(Core)/media/SampleVideo_1280x720_2mb.mkv" type="video/x-matroska"/>
            <source src="../Javascript%20(Core)/media/SampleVideo_1280x720_2mb.flv" type="video/x-flv"/>
            <source src="../Javascript%20(Core)/media/SampleVideo_1280x720_2mb.mp4" type="video/mp4"/>
            <object><p>Video is not supported by this browser.</p></object> <!-- This message will be shown if browser doesn't support video/audio tag -->
        </video>
    ```
    
    #### Video JS methods
    - play() - plays the video from its current position;
    - pause() - pauses the video at its current position;
    - volume - allows the user to control the volume of the video;
    - currentTime - represents the current position of the video. Increase or descrease this value to move forward or backward in the video;

- **canvas** - defines some area which allow you to draw smth.


## Few words about SEO

SEO - Search Engine Optimization

The \<article> and \<section> elements are the main ones used by the SEO algorithm. These elements are known to contain the main body of the page.

Within each \<article\> element, the engine then seeks out elements such as <hgroup> or \<h1\> to get the main topic of the <article> element for relevant indexing.


## Questions

0. Difference mark strong tags
	There are some html tag which make semantic emphasis for some text. There are \<mark\>, \<strong>, \<em\> but what diff between them?
	
	- The \<strong\> element represents strong importance for its contents. Changing the importance of a piece of text with the strong element does not change the meaning of the sentence.
	- The \<em\> element represents stress emphasis of its contents. The placement of stress emphasis changes the meaning of the sentence.
	- The \<mark\> element represents a run of text in one document marked or highlighted for reference purposes, due to its relevance in another context.
	
	**\<mark\>** doesn't really have relevance to content, only context (e.g. marking content that matches a search term, misspelled words, selected content in a web app, etc.).
	
	**\<strong\>** denotes important text, but does not affect meaning.
	
	**\<em\>** denotes important text and affects the meaning of the content by saying that it should be read/spoken with emphasis.




