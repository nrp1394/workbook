# Webpack

Webpack - иструмент для сборки.

## Внешний доступ к модулям

Представим что у нас есть класс `Instance`

**instance.class.js**
```js
class Instance {
    constructor (id) {
        this.id = id;
    }
}

export default Instance;
```

И мы хотим сделать его доступным извне окружения webpack.
Для этого мы в свою очередь:

1) Экспортируем его из главного файла сборки

**index.js**
```js
import Instance from './src/instance.class.js';

export { Instance };
```

2) Указываем имя по которому наш сборочный модуль будет доступным

**webpack.config.js**
```js
module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        library: 'myExportLibrary'
    },
};
```

## Разница между webpack & gulp

## package.json config

![Replace](media/package-json-config.png)

If you set `"private": true` in your `package.json`, then npm will refuse to publish it.
This is a way to prevent accidental publication of private repositories

## Loading CSS

In order to import a CSS file from within a JavaScript module, you need to install and add the style-loader and css-loader to your module configuration:

```
yarn add style-loader css-loader --dev
```

```js
module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};
```

## Loading Images

```
yarn add file-loader --dev
```

```js
module.exports = {
    ...,
    module: {
        rules: [
            ...,
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    }
}
```

Now, when you import MyImage from './my-image.png', that image will be processed and added to your output directory and the MyImage variable will contain the final url of that image after processing