# Refactoring

## Composing Methods

- Extract method
     тут понятно
     
- Inline method
    обратная штука extract method, используется когда темо метода и так говорит само за себя
    
    ```javascript
    function moreThanFiveLateDeliveries () {
        return _numberOfLateDeliveries > 5;
    }
    function getRating () {
        return (moreThanFiveLateDeliveries()) ? 2 : 1;
    }
    ///////////////////////////////////////////////////
    function getRating() {
        return (_numberOfLateDeliveries > 5) ? 2 : 1;
    }
    ```
    
- Inline temp
    разовое использование переменной которое может быть заменено на непосредственно ее тело

    ```javascript
    double basePrice = anOrder.basePrice();
    return (basePrice > 1000);
    ////////////////////////////////////////
    return (anOrder.basePrice() > 1000)
    ```
  
- Introduce Explaining Variable
    вынесение кусков проверки в переменные
    
    ```javascript
    if ((platform.toUpperCase().indexOf("MAC") > -1) && (browser.toUpperCase().indexOf("IE") > -1) && wasInitialized() && resize > 0) {
        // do something
    }
    //////////////////////////////////////
    const isMacOs = platform.toUpperCase().indexOf("MAC") > -1;
    const isIEBrowser = browser.toUpperCase().indexOf("IE") > -1;
    const wasResized = resize > 0;
    
    if (isMacOs && isIEBrowser && wasInitialized() && wasResized) {
        // do something
    }
    ```
  
- Split Temporary Variable
    Замена переменной которая используется временно для каких-либо кейсов отдельной со своим именем

    ```javascript
    let temp = 2 * (_height + _width);
    console.log(temp);
    temp = _height * _width;
    console.log(temp)
    
    ///////////////////////////////////////
    
    const perimeter = 2 * (_height + _width);
    console.log(perimeter);
    const area = _height * _width;
    console.log(area);
    ```
  
## Move methods
В этом блоке мы говорим о вынесении полей и методов в сущность где им семантически лучше лежать

- Move method
- Move field


## Organizing Data (basic)
Этот блок говорит о сокрытии определенных полей и методов которые не должны быть видны снаружи

- Encapsulate field
- Encapsulate method


## Simplifying Conditional Expressions (basic)

- Decompose Conditional Expression
    Вынесение проверки if в отдельный метод
    
    ```javascript
    if (date.before (SUMMER_START) || date.after(SUMMER_END))
        charge = quantity * _winterRate + _winterServiceCharge;
    else
        charge = quantity * _summerRate;
    
    ///////////////////////////////////////
    
    if (notSummer(date))
        charge = winterCharge(quantity);
    else
        charge = summerCharge (quantity);
    ``` 
  
- Remove Control Flag
    Старатся избегать использования каких-либо флажков отвечающих за бранчевание
    
- Replace Conditional with Polymorphism
    Замена общего поведения единым методом но с разными параметрами
    