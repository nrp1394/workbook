## Streams

Streams are objects that allow reading of data from the source and writing of data to the destination as a continuous process.
