## Canvas

Canvas is a HTML element is used to draw graphics on a web page.
It is a bitmap with an “immediate mode” graphics application programming interface (API) for drawing on it.
The \<canvas\> element is only a container for graphics. In order to draw the graphics, you are supposed to use a script.

- to draw smth using canvas used JS;



### Canvas methods
- **_beginPath_** - resets/begins a new drawing path
- **_moveTo_** - moves the context to the point set in the beginPath method
- **_lineTo_** - sets the destination end point for the line
- **_stroke_** - strokes the line, which makes the line visible
- **_fill_** - filled some figure with styled which specified in `fillStyle` prop
- **_arc_** - standard arc based on a starting and ending angle and a defined radius
- **_quadradicCurveTo_** - more complex arc that allows you to control the steepness of the curve
- **_bezierCurveTo_** - another complex arc that you can skew



### Canvas props
- strokeStyle
- lineWidth
- fillStyle

```html
<canvas id="drawingSurface" width="800" height="600" style="border: 1px solid black;">
    Your browser does not support HTML5.
</canvas>
```
```javascript
const drawingSurface = document.getElementById('drawingSurface');
const context = drawingSurface.getContext('2d');
context.beginPath();
context.moveTo(100, 100);
context.lineTo(200, 200);
context.lineTo(300, 100);
context.stroke();
```



### Methods description
- **arc**
	- _X, Y_ - the first two parameters are the X and Y coordinates for the center of the circle.
	- _radius_ - the third parameter is the radius. This is the length of the distance from the center point of the circle to the curve.
	- _startAngle, endAngle_ - the fourth and fifth parameters specify the starting and ending angles of the arc to be drawn. This is measured in radians, not in degrees.
	- _counterclockwise_ - the final parameter specifies the drawing direction of the arc.

- **rect**
	- x,y - x-coordinate and y-coordinate define the starting position of the rectangle. This is the top-left corner of the rectangle.
	- width - defines the width of the rectangle.
	- height - defines the height of the rectangle.



### How to change styling

```javascript
context.fillStyle = 'lightgrey';
context.lineWidth = 5;
context.strokeStyle = 'green';

// creates some shape with 'lightgrey' background and border: 5px solid green;
```


### Gradient usage

- **_context.createLinearGradient(x0, y0, x1, y1)_**
	![Linear Gradient](media/linear-gradient.png)

- **_context.createRadialGradient(x0, y0, r0, x1, y1, r1)_**
	![Radial Gradient](media/radial-gradient.jpg)


```javascript
context.rect(0, 0, 100, 100);
const gradient = context.createLinearGradient(0,0, 100,100);
gradient.addColorStop(0, 'yellow');
gradient.addColorStop(0.5, 'blue');
gradient.addColorStop(1, 'green');

context.fillStyle = gradient;
context.fill();
context.stroke();
```



### Usage images

- **_context.drawImage(imageObj, x, y, width, height)_**

```javascript
// example of image with text
const backgroundImage = new Image();
backgroundImage.src = "repeat-image.png";
backgroundImage.onload = () => {
    context.drawImage(backgroundImage, 0, 0, 300, 300);
    context.stroke();
    context.strokeText("Caption for image", 100, 300);
};
```


- **_context.createPattern(image, repetition)_**
	- repetition
		- "repeat" (both directions)
		- "repeat-x" (horizontal only)
		- "repeat-y" (vertical only)
		- "no-repeat" (neither direction)

```javascript
context.rect(10, 10, 700, 700);

const image = new Image();
image.src = 'repeat-image.png';
image.onload = () => {
    const pattern = context.createPattern(image, 'repeat');
    context.fillStyle = pattern;
    context.fill();
    context.stroke();
};
```



### Diff between canvas & svg

https://habr.com/company/simbirsoft/blog/332750/
https://webformyself.com/canvas-ili-svg-kak-vybrat-pravilnyj-instrument-dlya-raboty/