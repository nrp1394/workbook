## SVG

Scalar Vector Graphic

XML-подібна мова розмітки, для двухмірної векторної графіки.

**Векорна графіка** - спосіб представлення обєктів, який базується на математичному описі елементарних графічних обєктів, які називаються примітивами(точки, лінії, криві).

Наприклад, для малювання **_круга_**, нам необхідно знати:
- радіус
- координати центру
- ширину лінії обводки
- фон

### Плюси
- обєм даних для опису мінімальний і не залежить від реальної величини обєкту
- як результат при масштабувані, збільшені якість не погіршиться

### Tags
- circle
	- r - радіус круга
	- cx - зміщення по осі х (але відносно центра фігури, а не її країв, як у margin)
	- cy - зміщення по осі у
	
	```html
	<circle r="50" cx="100" cy="100"></circle>
	```

- ellipse(rx, ry, cx, cy)
	- rx - радіус еліпса по осі х
	- ry - радіус еліпса по осі у
	- cx - зміщення по осі х
	- cy - зміщення по осі у
	
	```html
	<ellipse rx="100" ry="50" cx="100" cy="100" fill="lightgreen"></ellipse>
	```

- rect(width, height)
	- width
	- height
	- x - зміщення по відповіній осі
	- y - зміщення по відповіній осі
	- rx - заокруглення по осі х
	- ry - заокруглення по осі у
	
	```html
	<rect width="100" height="100" x="15" y="15" rx="10" ry="20"></rect>
	```

- polygon(points="x,y x,y ...")
	- points - набір значень х,у для представлення вершин
	
	```html
	<polygon points="50,50 100,100 150,50"></polygon>
	```

- line(x1,y1,x2,y2)
	- x1,y1 - координати початку;
	- x2,y2 - координати кінця
	- stroke - border-color - якщо не визначено - ви нічого не побачите
	```html
	<line x1="10" y1="10" x2="50" y2="10" stroke="green"></line>
	```

- polyline(points)
	- points - набір значень х,у для представлення вершин
	```html
	<polyline points="0,100 50,25 50,75"></polyline>
	```
- linear-gradient(id)

```html
<svg>
    <linearGradient id="linear-gradient">
        <stop offset="0%" stop-color="gold"/>
        <stop offset="100%" stop-color="teal"/>
    </linearGradient>
    <circle r="60" cx="150" cy="50%" fill="url(#linear-gradient)"></circle>
</svg>
```

- radial-gradient(id, cx, cy)
- desc - опис, не рендериться в документі, покращує доступність svg doc


### Attributes
- fill _(default color - black)_
- fill-opacity
- cx & cy - відступ по осям від центру
- x & y - відступ по осям від краю
- rx & ry - округлення по осям (border-radius) або обозначення радіуса для певної осі
- points - координати точок
- stroke - колір границі
- stroke-width - ширина границі
- stroke-opacity - прозорість ліній
- stroke-linecap - кінець обводки (butt, round, square)
- stroke-linejoin - як обробляги вигини для ліній (miter(дефолт), round, bevel)
- stroke-dasharray - як обробяти пунктири (15 10, 15(15 15) - ширина ліній і пробілів, массив)
- stroke-dashoffset - сдвиг для рендерінга пунктирів
- xlink:href - запозичити стилі із іншого градієнту


Як намалювати дугу
 - намалювати круг і використовуючи властивість dash-array показати лише певну частину круга
 - використати тег path
 
http://rsdn.org/forum/media/6435894.1
http://qaru.site/questions/45461/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
Більше інфи про тег path
https://developer.mozilla.org/ru/docs/Web/SVG/Tutorial/Paths


#### Pattern
\<pattern\> тег який використовується для задання шаблону, який буде використовуватись для зафарбовування

```html
<svg viewBox="0 0 230 100" xmlns="http://www.w3.org/2000/svg">
  <defs>
    <pattern id="star" viewBox="0,0,10,10" width="10%" height="10%">
      <polygon points="0,0 2,5 0,10 5,8 10,10 8,5 10,0 5,2"/>
    </pattern>
  </defs>
  <circle cx="50"  cy="50" r="50" fill="url(#star)"/>
  <circle cx="180" cy="50" r="40" fill="none" stroke-width="20" stroke="url(#star)"/>
</svg>
```


### Diff between polyline and polygon
- polygon зєднує останню і першу координату, таким чином замикаючи фігуру
- polyline використовується для обозначення відкритих фігур