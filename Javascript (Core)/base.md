## Types

Primitive types:
- number (Infinity, NaN)
- string
- boolean
- null - посилання на обєкт, якого не існує
- undefined - тип даних, який присвоюється для неініціалізованих зміних
- symbol

Object - ссилочний тип даних

### Оператори для визначення типів
- typeof
- instanceOf

```javascript
typeof 1;			// 'number'
typeof 'str';			// 'string'
typeof true;			// 'boolean'
typeof {};			// 'object'
typeof null;			// 'object'
typeof undefined;		// 'undefined'
typeof function () {}		// 'function'
```

== - порівняння **рівності** *(приводить значення до одного типу)*  
== - порівняння **ідентичності**  

```javascript
1 / 0;					// Infinity
0 / 0;					// NaN
'string' * 2;			// NaN
null == undefined;		// true
null === undefined;		// false

5  == 5;			// true
5 === 5;			// true
new Number(5) == 5;		// true
new Number(5) === 5;		// false!

const obj1 = { alpha: 1 };
const obj2 = { alpha: 1 };
ob1 == obj2;			// false
ob1 === obj2;			// false

(function(){
	console.log(this  == 5);		// true
	console.log(this === 5);		// false
	console.log(this);			// Number {5}, result of new Number(5)
}).call(5);

Infinity === Infinity;		// true
NaN === NaN;			// false
isNaN(NaN);			// true

isFinite(Infinity);		// false
isFinite(1);			// true
```

### Number
Все числа в JavaScript, как целые так и дробные, имеют тип Number и хранятся в 64-битном формате **IEEE-754**, также известном как **«double precision»**.
Саме через це існує момент із:
```javascript
const a = 0.1 + 0.2;		// 0.300000000001
console.log(9999999999999999);	// 10000000000000000
```

Хоча в математиці 0.5 при округленні трактується вверх, при використанні `toFixed` результат округлюється вниз. Відбувається це тому що це число зберігається в наступному форматі `0.35.toFixed(20); // "0.34999999999999964473"` і при цьому дане число тактується як `0.34...`
```javascript
0.35.toFixed(1);		// "0.3"
0.34999999999999964473;	// "0.34999999999999964473"
```

*Числа можна задавати*
- в шістнадцяковому форматі:
	```javascript
	const a = 0xff;		// 255
	parseInt('ff', 16);	// 255
	```

- в двійковому форматі:
	```javascript
	parseInt('101', 2);	// 5
	```

- запис із плаваючою точкою
	```javascript
	const a = 1e3;	// 1000
	const a = 1e-3;	// 0.001
	```
	
#### Перетворення в числа
- parseInt('1'), parseFloat('1.1')
- +'56', Number('a')

Різниця між цими двома підходами, що якщо строка, яка парситься містить символи, то перетворення через **"+"** видасть **NaN**, а parseInt, parseFloat перетворить в цифру всі цифри до першого входження символу.
  
При цьому, якщо parseInt приймає у якості параметру строку, яка представляє дробове число - то результатом буде лише ціла частина, дробова буде відкинута, округлення не відбудеться 

```javascript
+'45';			// 45
+'45a';			// NaN
Number('77');		// 77
Number('a77');		// NaN

parseInt('45');		// 45
parseInt('45a');	// 45
parseInt('a45a');	// NaN

parseInt('2.7');	// 2
```

#### Округлення
- Math.floor - вниз
- Math.ceil - вверх
- Math.round - до найближчого цілого

```javascript
Math.floor(3.1);	// 3
Math.ceil(3.1);		// 4
Math.round(6.1);	// 6
Math.round(6.5);	// 7
```

##### Округлення використовуючи бітові операції
```javascript
~~12.6;		// 12
12.6 ^ 0;	// 12
```
При цьому не забуваємо що бітові операції мають найнижчий пріорітет

```javascript
var x = a * b / c ^ 0; // читается как "a * b / c и округлить"
```

#### Округлення до заданої точності
toFixed:
- результат не обрізається, f **округлюється** до заданої точності, а не до цілого значення, на відміну від `Math.round`
- результат який повертається - **строка**

```javascript
12.34.toFixed(1);	// "12.3"
12.37.toFixed(1);	// "12.4"
12.34.toFixed(5);	// "12.34000"

12.34567.toFixed(8);	// "12.34567000"
12.34567.toFixed(8);	// "12.34567000"
+12.34567.toFixed(8);	// 12.34567
```

## Reference vs value
По дефолту всі параметри передаються у функції **по значенню**, а не по ссилці.

Тобто значення одного параметру копіюється в інший (операція =). При цьому для примітивних значень це відбувається по одному, для ссилочних переприсваюється посилання.

Значення не було модифіковано всередині функції!
```javascript
const num1 = 5;
const num2 = addSomeValue(num1);
console.log(num1);	// 5
console.log(num2);	// 10

function addSomeValue (someNum) {
	someNum += 5;
	return someNum;
}
```

Для обєкту - наоброт
```javascript
const obj1 = { alpha: 1 };
const obj2 = addProp(obj1);
console.log(obj1);		// { alpha: 1, beta: 1 }
console.log(obj2);		// { alpha: 1, beta: 1 }
console.log(obj1 === obj2);	// true

function addProp (myObj) {
	myObj.beta = 1;
	return myObj;
}
```

Ще одна цікава особливість
```javascript
const obj1 = { alpha: 1 };
const obj2 = reInitObject(obj1);
console.log(obj1);		// { alpha: 1 }
console.log(obj2);		// { beta: 1 }
console.log(obj1 === obj2);	// false

function reInitObject (myObj) {
	// in this case myObj = obj1; And then re-init
	myObj = { beta: 2 };
	return myObj;
}
```

### Variable Scoping
// ------ Zakas: variables resolution

### Exception Handling
// ---

### Context of calling
// this
// call, bind, apply

**Execution context** - це абстракте поняття, певне оточення в рамках якого відбувається виконання коду. В рамках браузера - це глобальний обєкт window, якщо ми говоримо про обєкти - то це this.

Методи **call** та **apply** дозволяють викликати певний метод при цьому змінивши контекст **this**, на той який був переданий як аргумент.
При цьому різниця між ними у форматі передачі параметрів у функцію.

```javascript
func.call(context, arg1, arg2);
func.apply(context, [ arg1, arg2 ]);
```  
Метод **bind** поверне нову функцію, при виклику якої буде використовуватись контекст, який ми передали як параметр.  
При цьому можна predefinded parametrs

### Functions
#### Hoisting
#### Closures

