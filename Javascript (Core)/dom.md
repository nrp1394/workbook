## DOM

*Dom* - Document Object Model - представлення структури веб-сторінки у вигляді дерева.

### Methods of creating DOM nodes (Create)

- **document.createElement(elementName)** - create a new element node;
- **document.createTextNode(text)** - create a new text node;
- **node.textContent(innerText)** - get or set the text content of an element node;
- **node.innerHTML** - get or set the HTML content of an element.
- **document.cloneNode(_[deep]_)** - method returns a duplicate of the node on which this method was called. But don't forget to change _'id'_ property if it exists before pasting into the DOM.

### Methods of querying DOM nodes (Read)

- **getElementById()** - gets some specific node by unique id attribute. Returns only one node (if exists);
- **getElementsByClassName()** - gets all DOM-nodes which have some class. Returns **_HTMLCollection_**, and is live;
- **getElementByTagName()** - gets all the elements with some element name or specific tag. Returns **_HTMLCollection_**, and is live;
- **getElementByName()** - method returns a collection of all elements in the document with the specified name. Returns **_NodeList_** and is live;

- **querySelector()** -  gets the first child element found that matches the provided CSS selector criteria;
- **querySelectorAll()** - gets all the child elements that match the provided CSS selector criteria. Returns **_NodeList_** and is **not** live.

These two last methods return live collections. It means if appears some element which satisfies current css query it also would be present at this collection.


### Methods of modifying DOM (Update)

- **parentNode.appendChild(_newNode_)**- add a node as the last child of a parent element;
- **parentNode.insertBefore(_newNode_, _nextSibling_);** - insert a node into the parent element before a specified sibling node;
- **parentNode.replaceChild(newNode, oldNode);** - replace an existing node with a new node;

```html
<ul id="listWrapper">
    <li>Buy groceries</li>
    <li>Feed the cat</li>
    <li>Do laundry</li>
</ul>
```

```javascript
const listWrapper =  document.getElementById('listWrapper');

const newElement = document.createElement('li');
newElement.innerText = '*** Do homework ***';
listWrapper.appendChild(newElement); // paste at last position

const anotherElement = document.createElement('li');
anotherElement.innerText = '*** Pay bills ***';
listWrapper.insertBefore(anotherElement, listWrapper.children[0]); // paste at first position

const actualNode = document.createElement('li');
actualNode.innerText = '*** Feed the dog ***';
listWrapper.replaceChild(actualNode, listWrapper.children[2]);
```


### Methods for delete DOM elements (Delete)

- **node.removeChild()** - remove child node. Returns reference to the removed child node;
- **node.remove()** - remove node. Doesn't return anything.


### Useful DOM props and methods

- **firstChild** - gets first child node (any type, text node too);
- **firstElementChild** - gets first child node of element type;
- **lastChild** - gets last child node (any type, text node too);
- **lastElementChild** - gets last child node of element type;

- **childNodes** - returns a **_live NodeList_** of child nodes of the given element (any node types);
- **children** - returns a live **_HTMLCollection_** which contains all of the child **elements** of the node upon which it was called (only element nodes); 
- **hasChildNodes()** - returns true if the parent element has any child nodes at all. A good practice is to check this property before accessing other properties, such as firstChild or lastChild.

- **nextSibling** - returns a node immediately following the specified one in their parent's childNodes, or returns _null_ if the specified node is the last child in the parent element. 
- **nextElementSibling**
- **previousSibling** - returns the node immediately preceding the specified one in its parent's childNodes list, or null if the specified node is the first in that list;
- **previousElementSibling**

- **parentElement** - returns the DOM node's parent _Element_, or null if the node either has no parent, or its parent isn't a DOM Element
- **parentNode** - returns the parent of the specified node in the DOM tree.


### Modifying Attributes

- **_hasAttribute()_** - returns a true or false boolean
- **_getAttribute()_** - returns the value of a specified attribute or null;
- **_setAttribute()_** - adds or updates value of a specified attribute;
- **_removeAttribute()_** - removes an attribute from an element.

```javascript
element.hasAttribute('href');
element.getAttribute('href');
element.setAttribute('href', 'https://google.com');
element.removeAttribute('href');
```

### Modifying Classes

- **_className_** - gets or sets class value;
- **_classList.add()_** - adds one or more class values;
- **_classList.toggle()_** - toggles a class on or off;
- **_classList.contains()_** - checks if class value exists;
- **_classList.replace()_** - replace an existing class value with a new class value;
- **_classList.remove()_** - remove a class value.

```javascript
console.log(element.className);
element.classList.add('active');
element.classList.toggle('active');
element.classList.contains('active');
element.classList.replace('old', 'new');
element.classList.remove('active');

```

## Questions

0. What does it mean _'live collection'_. Which methods return NodeList and which HtmlCollection;

	- _document.getElementsByClassName()_ is an **HTMLCollection**, and is live.
	- _document.getElementsByTagName()_ is an **HTMLCollection**, and is live.
	- _document.getElementsByName()_ is a **NodeList** and is live.
	- _document.querySelectorAll()_ is a **NodeList** and is **not** live.

	JQuery methods return **not** live collections.

0. What difference between NodeList and HtmlCollection?

	NodeList and HTMLCollection are both collections.  
	They differ in the methods they provide and in the type of nodes they can contain.  
	While a **_NodeList_** can contain any node type(text, comment), a **_HTMLCollection_** is supposed to only contain Element nodes.  
	
	**_HTMLCollection_** also has method which called _namedItem()_ which allow you to get element by name attribute. Also these elements are available like a collection's props where keys are name attribute's value. Also HTMLCollection always **live**, they are update on DOM change.
	
	**_NodeList_** is more abstract list which provides methods for traversing its collection. _entries()_, _keys()_, _values()_ provides ArrayIterator for traversing. And also present _forEach()_ method. NodeList objects are “live” or static, depending on the method used to get them.

0. What result of getElementById(), $('#...'), querySelector('#...') would be if document contains more than one elements with the same ID.
 
0. **(-)** Cross-Site Scripting (XSS) using innerHTML?

0. **(-)** Does the _cloneNode()_ method faster? In which situation?

0. **(-)** How to display element like an object instead of DOM-node?

0. **(-)** What method works faster either innerHTML or createElement/appendElement?