# Storages

## LocalStorage & SessionStorage

Це певні локальні сховища даних на клієнті. Тривалістю зберігання своїх даних.

- localStorage - немає обжень аж до фізичного очищення
- sessionStorage -  до закриття поточної сесії (вкладки)

window.localStorage => returns object Storage, which contains all data.


#### Methods

- **_setItem_** - adds a key/value pair into storage. If no item with the specified key exists, the item is added; if that key does exist, its value is updated.
- **_getItem_** - retrieves data from storage based on a specified key value or index.
- **_clear_** - clears all storage that has been saved. Use this method to clear out the storage as needed.
- **_key_** - получает числовое значение n и возвращает имя n-ого ключа из хранилища.  Порядок ключей определяется браузером.
- **_removeItem_** - removes the specified key/value pair from storage.

```javascript
localStorage.alpha = 1;
localStorage.beta = 2;

console.log(localStorage);
// Storage { alpha: 1, beta: 2 };

localStorage.key(0); // -> alpha
localStorage.key(1); // -> beta
```

![Local Storage](media/local-storage-order.png)



## AppCache API

#### Using AppCache API

The manifest attribute on the html element tells the browser that this webpage needs to be available offline.

```html
<html manifest="webApp.appcache">
...
</html>
```

The manifest file contains three sections:
- CACHE  
	lists all the resources that must be cached offline. CSS files, JPG files, video and audio files, and any other resource required for the page to function correctly

- NETWORK
	declares any resources that must be available from the Internet. Anything that the page requires from the Internet, such as embedded third-party elements.

- FALLBACK
	instructions to the browser in the event that an item isn’t available in the cache and the browser is in offline mode.


**window.applicationCache** - AppChache global object 


#### Application Cache status property:

- **_Uncached_** - the web application isn’t associated with an application manifest.
- **_Idle_** - the caching activity is idle, and the most up-to-date copy of the cache is being used.
- **_Checking_** - the application manifest is being checked for updates.
- **_Downloading_** - the resources in the application manifest are being downloaded.
- **_UpdateReady_** - the resources listed in the manifest have been successfully downloaded.
- **_Obsolete_** - the manifest can no longer be downloaded, so the application cache is being deleted.

#### Application Cache methods:

- **_swapCache_** - indicates that the cache be replaced with a newer version.
- **_update_** - tells the browser to update the cache if an update is available.

#### Application Cache events:

- **_onchecking_** - the browser is checking for an update to the application manifest, or the application is being cached for the first time.
- **_onnoupdate_** - the application manifest has no update available.
- **_ondownloading_** - the browser is downloading what it has been told to do per the manifest file.
- **_onprogress_** - files are being downloaded to the offline cache. This event fires periodically to report progress.
- **_oncached_** -the download of the cache has completed.
- **_onupdateready_** - the resources listed in the manifest have been newly redownloaded, and the swapCache method might be called.
- **_onobsolete_** - a manifest file is no longer available.
- **_onerror_** - an error has occurred. This could result from many things. Appropriate logging is necessary to get the information and resolve.


## Geolocation API

sakdljslk

- **_geoLocator.getCurrentPosition_** (positionCallback, [positionErrorCallback], [positionOptions]) - current position of the user or the device in which the application is running;
	- positionCallback
	- positionErrorCallback
	- positionOptions
	
- **_geoLocator.watchPosition_** (successCallBack,errorCallback,positionOptions) - built-in mechanism that continuously polls for the current position. Returns watcherOptions.

	
**PositionOption**
- enableHighAccuracy - this causes the method to be more resource intensive if set to true. The default is false. If true, the getCurrentPosition method tries to get as close as it can to the actual location.
- timeout - specifies a timeout period for how long the getCurrentPosition method can take to complete. This number is measured in milliseconds and defaults to zero. A value of zero represents infinite.
- maximumAge - if this is set, the API is being told to use a cached result if available, rather than make a new call to get the current position. The default is zero, so a new call is always be made. If maximumAge is set to a value and the cache isn’t older than the allowable age, the cached copy is used. This value is measured in milliseconds.