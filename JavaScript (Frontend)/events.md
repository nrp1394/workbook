## Events

**Event** - это сигнал от браузера о том, что что-то произошло.

**События мыши:**

- click – происходит, когда кликнули на элемент левой кнопкой мыши
- contextmenu – происходит, когда кликнули на элемент правой кнопкой мыши
- mouseover – возникает, когда на элемент наводится мышь
- mousedown и mouseup – когда кнопку мыши нажали или отжали
- mousemove – при движении мыши

**События на элементах управления:**

- submit – посетитель отправил форму <form>
- focus – посетитель фокусируется на элементе, например нажимает на `<input>`

**Клавиатурные события:**

- keydown – когда посетитель нажимает клавишу
- keyup – когда посетитель отпускает клавишу

**События документа:**

- DOMContentLoaded – когда HTML загружен и обработан, DOM документа полностью построен и доступен.

**События CSS:**

- transitionend – когда CSS-анимация завершена.


### Назначение обработчиков событий

- inline html attribute

    ```html
    <input value="Нажми меня" onclick="alert('Клик!')" type="button">
    ```
    
- DOM-property

    But remember in this case you rewrite all handlers which were bound!!!

    ```javascript
    element.onclick = function () {
      // do some actions
    }
    ```
    
    Don't use `setAttribute` method for binding events, it doesn't work.
    Cause attribute function will be converted to string
    
    ```javascript
    document.body.setAttribute('onclick', function() { /* code here */ });
    ```
    
    ```html
    <body onclick="function() { console.log('alpha'); }"></body>
    ```
    
- addEventListener & removeEventListener (attachEvent & detachEvent)

    addEventListener(eventType, handler, useCapture);
    
    ```javascript
    element.addEventListener('click', handler, true);
    ```
    
    For IE8- you can use `attachEvent` & `detachEvent`
    ```javascript
    element.attachEvent("on" + event, handler);
    ```
    
    Difference is in concatenation 'on' before event name ('onclick').
    Also attachEvent method doesn't have `this` inside handler.
    By default `attachEvent` uses bubbling
    
### Funny case: mix in usage

- 1) When js attribute handler rewrites existed one

    ```html
    <input value="Нажми меня" onclick="console.log('HTML attribute handler!')" type="button">
    ```
    ```javascript
    const input = document.getElementsByTagName('input')[0];
    
    input.addEventListener('click', function () {
        console.log('inside add event listener #1');
    });
    input.addEventListener('click', function () {
        console.log('inside add event listener #2');
    });
    
    input.onclick = function () {
        console.log('inside js attribute');
    };
    ```
    
    Result:
    
    ```javascript
    // inside js attribute
    // inside add event listener #1
    // inside add event listener #2
    ```
    
- 2) Without js attribute handler, only HTML-attribute

    ```html
    <input value="Нажми меня" onclick="console.log('HTML attribute handler!')" type="button">
    ```
    ```javascript
    const input = document.getElementsByTagName('input')[0];
    
    input.addEventListener('click', function () {
        console.log('inside add event listener #1');
    });
    input.addEventListener('click', function () {
        console.log('inside add event listener #2');
    });
    ```
    
    Result:
    
    ```javascript
    // HTML attribute handler!
    // inside add event listener #1
    // inside add event listener #2
    ```
    
### Bubbling and capturing

**Bubbling** - when event bubbles from deepest element to highest

![Bubbling](media/events/event-order-bubbling.gif)

**Capture** - when event captures from highest to deppest element

![Bubbling](media/events/event-order-capturing.gif)

#### Порядок обработки в стандарте W3C

Решение от W3C объединяет обе модели: перехват и всплытие в одну универсальную.

При совершении действия, сначала событие перехватывается, пока не достигнет конечного элемента, затем всплывает.

Таким образом, разработчик сам решает, когда должен срабатывать обработчик события – при перехвате или при всплытии.

![Bubbling](media/events/event-order-w3c.gif)

#### "Return false" from handler

- event.preventDefault()
- return false (from event handler)
- event.returnValue = false;

All of these points do the same. They are prevent standard browser behavior on some event, but don't stop bubbling.

### Event Loop

https://habr.com/ru/company/ruvds/blog/340508/

![Bubbling](media/events/JS-async-components.png)

**Web API** - потоки, к которым у нас нет прямого доступа, мы можем выполнять лишь, обращения к ним.
Они встроены в браузер, где собственно и выполняются асинхронные действия.

![Bubbling](media/events/event-loop.png)

**Event Loop** - наблюдает за стеком вызовов и очередью колбэков (callback queue). Если стэк вызовов пуст, то цикл берет первое событие из очереди и помещает его в стек, что приводит к запуску этого события на выполнение.

Подобная итерация называется **тиком (tick)** цикла событий. Каждое событие — это просто коллбэк.

![Bubbling](media/events/events-loop-process.gif)