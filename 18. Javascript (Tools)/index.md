## npm

**Пакетный менеджер** - набор программного обеспечения, позволяющего управлять процессом установки, удаления, настройки и обновления различных компонентов программного обеспечения.
**npm** - стандартный менеджер пакетов с очень большим регистром модулей который идет в комплекте с node js.

**npm разница между локальными и глобальными модулями**
При установке локально - модули будут доступны только в рамках одного проекта.
При установке глобально - можно использовать независимо от места, обычно глобально ставят 
- cli
- nodemon
- системы сборки(gulp, grunt)
- пакетные менеджеры(yarn)
- генератор кода(yo, create react app)


**разница между dependencies & devDependencies**
В devDependencies мы помещаем пакеты которые нужны нам в процессе разработки, которые не попадут у production версию продукта.

`npm init` - создать package.json

*package.json* содержит метаданные о нашем приложении или модуле, а также список зависимостей для установки из npm при запуске npm install.
- name
- version
- description
- author
- script (используется cli)
- main - основная точка входа в приложение
- peerDependencies - зависимости которые идут в паре с установленными модулями


**Публикация пакета**
- нужно иметь в проекте package.json
- залогинится на npm (npm login)
- `npm publish` / `npm unpublish <project>[@<version>]`

**script**
```json
{
  "start": "node index.js"
}
```
И для того чтобы исполнить нужно написать `npm start`.

Но в данном примере мы использовали один из списка зарезервированых скриптов.
npm позволяет нам определить кастомные скрипты. Разница лишь в методе запуска.
для кастомных скриптов мы используем команду `npm run [script_name]`



## Gulp
Система для описания произвольных задач, одной из которых также является и сборка.

Если рассматривать сравнение с `grunt`, то там для плагинов(minification, sass, autoprefixer), например, мы описываем таких аттрибуты как `src` & `dest`:
```js
{
    sass: {
        src: 'styles/*.css',
        dest: 'build/styles'
    },
    autoprefier: {
        src: 'styles/*.css',
        dest: 'build/styles'
    }
}
```

И когда у нас таких несколько задач, то по факту каждый из этих плагинов создает промежуточную директорию для своей работы, так как каждый из них это независимая единица.

Но преимущество gulp это наличие `VinylFS`. Это представление файла у ввиде обьекта с которым можно работать и передавать дальше для последующей обработки.
Плюс при работе с файлами не создается *temp directory*.
А также `.pipe()` - stream parallelism.


## Karma
это тула которая создает веб-сервер, который выполняет исходный код для тестового кода для каждого из подключенных браузеров.

**karma.config.file**

- frameworks
- browsers
- files (какие файлы нужно подгрузить: либы, utils, картинки)
- preprocessors
- port
- singleRun


**Code coverage**
karma-coverage node module

```js
coverageReporter: {
    check: {
        global: {
            statements: coverageThresholds.statements,
            branches: coverageThresholds.branches,
            functions: coverageThresholds.functions,
            lines: coverageThresholds.lines
        }
    },
    dir: REPORT_DIR + '/coverage/',
    reporters: [{type: 'text-summary'}]
}
```
  

## ESLint
Инструмент, который позволяет проводить анализ качества вашего кода, написанного на любом выбранном стандарте JavaScript. Он приводит код к более-менее единому стилю, помогает избежать глупых ошибок, умеет автоматически исправлять многие из найденных проблем и отлично интегрируется со многими инструментами разработки.

**.eslintrc**

- Environments - which environments your script is designed to run in. Each environment brings with it a certain set of predefined global variables.
- Globals - the additional global variables your script accesses during execution.
- Rules - which rules are enabled and at what error level.

```js
{
  "extends": "eslint:recommended",
  "env": {
    "browser": true,
    "node": true,
    "es6": true
  },
  "rules": {
    "no-console": 0,
    "indent": ["warning", 2]
  }
}
```

**formatters** - в каком виде выводить ошибки в консоли

**cli**
Позволяет запустить проверку файлов, папок (-c с указанием конфигурации) 
`eslint file1.js file2.js`
`eslint -c ~/my-eslint.json file.js`



## Typescript
язык программирования, являющийсь по факту строго типизированым JS

**Basic Types**
*Primitives*
```typescript
const isFlag: boolean = false;
const message: string = 'hello world';
const age: number = 13.5;
const someVal: any = 13.5;

let u: undefined = undefined;
let n: null = null;
```

*Arrays*
```typescript
const list: number[] = [1, 2, 3];
const tuple: [string, number] = ['some', 10];
```

*Enum*
```typescript
enum Color {Red = 1, Green, Blue}
let c: Color = Color.Green;
```


**Interfaces**
```typescript
interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person: Person, addition?: string) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

console.log(greeter({ firstName: "Jane", lastName: "User" }));
```


**Generics**
Обобщённые типы

```typescript
function identity<T, M>(name: T, age?: M): void {
    console.log({name, age})
}

identity<number, string>(10, '25');
```

**strictNullChecks**
Без этого флага `null` & `undefined` можно присваивать любым типам, если же он включен - то только если было явно указано в типе или для `any`


**Declaration Files**
Файлы которые описывают синтаксис и структуру функций и свойств, которые могут использоваться в программе, не предоставляя при этом конкретной реализации.
Нужны для корректного импорта файлов.

*globals.d.ts*
```typescript
declare var globalVar: string;
declare function display(): void;
```


## Babel

Babel — это транспилятор, который, в основном, используется для преобразования конструкций, принятых в свежих версиях стандарта ECMAScript, в вид, понятный как современным, так и не самым новым браузерам и другим средам, в которых может выполняться JavaScript.

Babel, кроме того, умеет преобразовывать в JavaScript и JSX-код, используя `@babel/preset-react`.

Сам по себе бабель ничего не делает, чтобы указать что именно нужно преобразовать и использууются **плагины**:
- async-to-generator
- modules-commonjs

Но сам по себе этот список может быть очень долгим, чтобы сделать жизнь проще существуют **preset** набор плагинов:
- @babel/preset-env
- @babel/preset-react 
- @babel/preset-typescript


## Webpack
Система сборки

- entry - ввходящая точка приложения с которой нужно начинать сборку
- output - куда сгенерировать bundle файл
- loaders - с коробки webpack умеет работать только с js & json. Loaders пользволяют "обьяснить" как нужно работать с другими типами файлов.
- plugins - возможность реализовать широкий спектр операций таких как: оптимизация, работа с assets


- performance - настраивает как показывать подсказки для перформанса.
- resolve - настройка aliases
- externals - что должно быть исключено из bundle файла. Но вместо этого он расчитывает что указаные зависимости будут в пользовательском окружении
    ```js
    externals: {
        'ha-config': 'window.HA.PXCUI.config',
        'user': 'window.HA.PXCUI.user'
    }
    ```
  
*Caching*

- split chunks. Возможность переюзать общие модули во время сборки

```js
optimization: {
   runtimeChunk: 'single',
}
```

- вынесение библиотек таких как `react` || `jquery` в отдельный chunk.

```js
optimization: {
  runtimeChunk: 'single',
  splitChunks: {
    cacheGroups: {
      vendor: {
        test: /[\\/]node_modules[\\/]/,
        name: 'vendors',
        chunks: 'all',
      }
    }
  }
}
```

*Shimming*
Сделать доступным некий набор глобальных переменных

```js
  const path = require('path');
  const webpack = require('webpack');

  module.exports = {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
      new webpack.ProvidePlugin({
        _: 'lodash',
      }),
    ],
  };
```


## Continuous Integration
это практика разработки программного обеспечения, которая заключается в слиянии рабочих копий в общую основную ветвь разработки несколько раз в день и выполнении частых автоматизированных сборок проекта для скорейшего выявления потенциальных дефектов и решения интеграционных проблем.


## Chrome developers tools
