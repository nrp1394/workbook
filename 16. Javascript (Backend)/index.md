# JS Backend

## NodeJS concepts

Node.js — это JavaScript фреймворк, основанный на движке V8 от Google(под капотом конвертирует код в C++), созданный для решения проблемы одновременной обработки большого количества запросов.

Node.js использует управляемую событиями, неблокирующую модель ввода-вывода.

Однопоточный, но легко масштабируемый — Node.js использует однопоточную модель с циклом событий. Механизм Event помогает серверу реагировать неблокирующим образом и обеспечивает высокую масштабируемость, в отличие от традиционных серверов, которые создают ограниченные потоки для обработки запросов. Node.js использует однопоточную программу, и одна и та же программа может обслуживать гораздо большее количество запросов, чем традиционные серверы, такие как Apache HTTP Server.


## Globals and Built-in modules

- Console
- Process
    Used to get information on current process. Provides multiple events related to process activities.
    - pid (process id)
    - env
    - argv
    
    - cwd() - current working directory
    - exit() 

__filename - исполняемый файл
__dirname - директория в которой исполняется скрипт


## Simple server

```js
const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

## Package.json
*package.json* содержит метаданные о нашем приложении или модуле, а также список зависимостей для установки из npm при запуске npm install.

- name (required)
- version (required)
- description
- author
- script (используется cli)
- main - основная точка входа в приложение
- peerDependencies - зависимости которые идут в паре с установленными модулями

```json
{
  "name": "my-awesome-package",
  "version": "1.0.0"
}
```

## Reading and Writing Files

**fs.open** & **fs.write**
*fs.open* создает файл с возможностью что-то с ним сделать. С ним в связке идут такие методы как *fs.write* и *fs.read*.
Которые принимают как параметр для своей работы *fd*(file descriptor) идентификатор файла с которым нужно произвести неокторые действия. 

```js
const fs = require('fs');

fs.open('demo.txt', 'w+', function (err, fd) {
    if (err) {
        return console.error(err);
    }
    console.log('File is opened to perform some operation');

    const data = new Buffer.from('Hello Node js');
    fs.write(fd, data, (err) => {
        if (err) {
            return console.error(err);
        }
    });
});
```

**fs.writeFile**
Как видно из примера, попроще...

```js
fs.writeFile('demo2.txt', 'Hello content!', function (...args) {
    // if (err) throw err;
    console.log(args);
});
```


## Express JS
Express - веб-фреймворк для Node который спроектирован для создания веб-приложений и API.

Он предоставляет следующие механизмы:

- Более простой механизм для обработки различных HTTP-запросов(GET, POST, PUT, DELETE). Написание обработчиков для запросов с различными HTTP-методами в разных URL-адресах (маршрутах).

- Интеграцию с механизмами рендеринга «view», для генерации ответов, вставляя данные в шаблоны.

- Настройка параметров сервера. Установка общих параметров веб-приложения, такие как порт для подключения, и расположение шаблонов, которые используются для отображения ответа.

- Миддлвары. «Промежуточное ПО» для дополнительной обработки запроса в любой момент в конвейере обработки запросов.


**Базовый шаблон**
(после `npm i express`)

```js
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
```

Базовый nodejs для сравнения

```js
const http = require('http');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```

Но обычно мы используем генератор
```
npm i -g express-generator
express <project-name>
```

```
├── app.js
├── bin
│   └── www
├── package.json
├── public
│   ├── images
│   ├── javascripts
│   └── stylesheets
│       └── style.css
├── routes
│   ├── index.js
│   └── users.js
└── views
    ├── error.pug
    ├── index.pug
    └── layout.pug
```

По дефолту express использует **jade** в качестве шаблонизатора.

```js
// app.js

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
```

Но если мы хотим задать кастомно свой шаблонизатор - то `express.engine` вам в помощь.

> Шаблонизатор — программное обеспечение, позволяющее использовать html-шаблоны для генерации конечных html-страниц.

```js
app.engine('ntl', function (filePath, options, callback) { // define the template engine
  fs.readFile(filePath, function (err, content) {
    if (err) return callback(new Error(err));
    // this is an extremely simple template engine
    var rendered = content.toString().replace('#title#', ''+ options.title +'')
    .replace('#message#', ''+ options.message +'');
    return callback(null, rendered);
  });
});
app.set('views', './views'); // specify the views directory
app.set('view engine', 'ntl'); // register the template engine
```

**Routing**

```js
// app.js

var usersRouter = require('./routes/users');
app.use('/users', usersRouter);
```

```js
// users.js

var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
```


## Manipulating file paths
Здесь мы обсуждаем полезные плюшки о модуле `path`

**.basename()**
```js
path.basename('/foo/bar/baz/asdf/quux.html');
// 'quux.html'

path.basename('/foo/bar/baz/asdf/quux.html', '.html');
// 'quux'
```

**.dirname()**
```js
path.dirname('/foo/bar/baz/asdf/quux');
// '/foo/bar/baz/asdf'
```

**.extname()**
```js
path.extname('my-path/another-one/index.html')
// .index.html
```

**.join()**
```js
path.join('/foo', 'bar', 'baz', 'quux', '77', '...');
// /foo/bar/baz/quux/77/...
```

**.resolve()**
```js
path.resolve('/foo/bar', './baz');
// Returns: '/foo/bar/baz'
```


## HTTP create server(req, res, queryParams)
Много где здесь будет упоминатся о **trailers** это header but at the end of the message.
Trailers will only be emitted if chunked encoding is used for the response.


**incomingMessage class methods**
Грубо говоря здесь мы имеем дело с обьектом **req** из обработчика
```js
router.get('/', function(req, res, next) {
  res.json({
    queryParams: req.query
  });
});
```

Основные методы:
- trailers
- rawTrailers
- headers
- rawHeaders - те же headers но у формате массива
- method - [GET, POST]
- statusCode
- url


**serverResponse class methods**
Обьект **res**

- addTrailers()
- end() - то что будет передано, будет трактоватся как законченый ответ
- getHeader()
- setHeader()
- removeHeader()
- statusCode
```js
response.statusCode = 404;
````

- statusMessage
```js
response.statusMessage = 'Not found';
```

- write() - передать chunk в качестве данных на отдачу


**handling URL Parameters**
В express.js мы имеем `req.query` который содержит все агрументы в качестве обьекта

```js
// http://localhost:3000/users?alpha=12&beta=true
"query": {
    "alpha": "12",
    "beta": "true"
  },
```

Но если мы говорим о нативном NodeJS, то модуль **url** нам в помощь

`url.parse(urlString[, parseQueryString[, slashesDenoteHost]])`

```js
url.parse("http://localhost:8080/default.htm?year=2017&month=february")

{
    "protocol": "http:",
    "slashes": true,
    "auth": null,
    "host": "localhost:8080",
    "port": "8080",
    "hostname": "localhost",
    "hash": null,
    "search": "?year=2017&month=february",
    "query": "year=2017&month=february",
    "pathname": "/default.htm",
    "path": "/default.htm?year=2017&month=february",
    "href": "http://localhost:8080/default.htm?year=2017&month=february"
}
```

## Работа с командной строкой
Вся эта работа организована вокруг `process.argv`, возвращающая массив где каждый элемент это слово указаное в командной строке


```js
// custom.js

console.log(process.argv);
```

```js
// node custom.js --name "alpha"

[ '/usr/local/bin/node',
  '/Users/rnest/Projects/express-test/custom.js',
  '--name',
  'alpha' ]
```

Так чтобы добавить функционал для чтения параметров с командной строки можем написать следующее:

```js
const nameIndex = process.argv.indexOf('--name');
let nameValue = 'defaultValue';

if (nameIndex > -1) {
    nameValue = process.argv[nameIndex + 1];
}

console.log(nameValue);
```

Суть этой локиги прочитать следующий параметр после `--name`.



## Spawning a Child Process
**execFile()**
Метод позволяющий нам запустить подпрограмму или файл по определенному пути, очень похож на WebWorker.
Но обмениватся сообщениями с ним в отличии от WebWorker нельзя. Для этого есть *fork()*.

```js
const execFile = require('child_process').execFile;
const child = execFile('node', ['--version'], (error, stdout, stderr) => {
    if (error) {
        console.error('stderr', stderr);
        throw error;
    }
    console.log('stdout', stdout);
});
```

Этот метод следует использовать, когда у нам просто нужно исполнить програму и получить от нее ответ об успешности: отработало нормально или с ошибками.
Не подходит для моментов когда нам нужно работать с большыми данными(получать/возвращать).


**exec()**
Метод просто исполняющий копределенную команду с аргументами.
Из которой мы просто можем получить ответ об успешности.

**spawn()**
Порождает новый процес и возращает streaming interface for I/O(Поток).

```js

const spawn = require('child_process').spawn;
const fs = require('fs');
function resize(req, resp) {
    const args = [
        "-", // use stdin
        "-resize", "640x", // resize width to 640
        "-resize", "x360<", // resize height if it's smaller than 360
        "-gravity", "center", // sets the offset to the center
        "-crop", "640x360+0+0", // crop
        "-" // output to stdout
    ];
    const streamIn = fs.createReadStream('./path/to/an/image');
    const proc = spawn('convert', args);
    streamIn.pipe(proc.stdin);
    proc.stdout.pipe(resp);
}
```

С которым мы в свою очередь можем работать как с потоком.
Стоит использовать когда мы работаем с большим обьемом данных, обрабатывая данные chunks.


**fork()**
Метод очень похож на *spawn()*, позволяет создать новый процесс с которым можно работать как с потоком + имеющию возможность обмениватся сообщениями по аналогии с WebWorker-ом.

```js
//parent.js
const cp = require('child_process');
const n = cp.fork(`${__dirname}/sub.js`);
n.on('message', (m) => {
  console.log('PARENT got message:', m);
});
n.send({ hello: 'world' });
//sub.js
process.on('message', (m) => {
  console.log('CHILD got message:', m);
});
process.send({ foo: 'bar' });
```



## Socket.io basic operations
**Include in application**
```
// installation

npm i --save socket.io
```

Я юзал фреймворк *express*. Поэтому логика подключения для меня была немного иной.
По доке нам нужно добавить использование модуля где производится вызов метода *.listen()*.

```js
// app.js

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res) {
   res.sendfile('index.html');
});

//Whenever someone connects this gets executed
io.on('connection', function(socket) {
   console.log('A user connected');

   //Whenever someone disconnects this piece of code executed
   socket.on('disconnect', function () {
      console.log('A user disconnected');
   });
});

http.listen(3000, function() {
   console.log('listening on *:3000');
});
```

Хоть в этом примере мы выдим что express используется, не заблуждайся, этот файл не был сгенерирован express-generator. 
У таких проектов немного другая файловая структура и этот код нужно добавлять по пути bin/www(для проектов сгенерированых express-generator).

```js
var app = require('../app');
var debug = require('debug')('express-test:server');
var http = require('http');

/** Get port from environment and store in Express. */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/** Create HTTP server. */

var server = http.createServer(app);
var io = require('socket.io')(server);

/** Handle socket.io events */

io.on('connection', function (socket) {
  console.log('A user connected');

  // ....
});

server.listen(port);
```


**Create connection**
Со стороны клиента мы пишем следующее(jade так как express-generator):

```jade
extends layout

block content
  h1= title
  p Welcome to #{title}

  script(src='/socket.io/socket.io.js')
  script.
    var socket = io.connect('http://localhost:3000');
    socket.on('news', function (data) {
      console.log(data);
      socket.emit('my other event', {my: 'data'});
    });
    socket.on('message', function (data) {
      console.log(data)
    });
```

Со стороны сервера это(чисто io часть, коннекшн выше по тексту)

```js
io.on('connection', function (socket) {
  console.log('A user connected');

  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });

  //Send a message after a timeout of 4seconds
  setTimeout(function() {
    socket.send('Sent a message 4seconds after connection!');
  }, 4000);

  socket.on('disconnect', function () {
    io.emit('user disconnected');
  });
});
```

Как видно из примера все крутится вокруг методов:
- socket.on(eventName, callback)
- socket.emit(eventName, data)


**Using channels**
Socket.io позволяет возможность создавать разные namespace для сообщений

*Со стороны сервера:*
```js
var chat = io
  .of('/chat')
  .on('connection', function (socket) {
    socket.emit('a message', {
        that: 'only'
      , '/chat': 'will get'
    });
    chat.emit('a message', {
        everyone: 'in'
      , '/chat': 'will get'
    });
  });
```

*Со стороны клиента*
```js
<script>
  var chat = io.connect('http://localhost/chat'),
      news = io.connect('http://localhost/news');

  chat.on('connect', function () {
    chat.emit('hi!');
  });
  
  news.on('news', function () {
    news.emit('woot');
  });
</script>
```

Мой пример:
Server:
```js
io.on('connection', function (socket) {
  console.log('A user connected');

  socket.emit('news', { hello: 'world' });
  socket.on('news-callback', function (data) {
    console.log('from news-callback: ', data);
  });

  socket.on('disconnect', function () {
    io.emit('user disconnected');
  });
});


var chat = io
    .of('/chat')
    .on('connection', function (socket) {
      // ответка только новоприбывшему
      socket.emit('message', {
        that: 'only'
        , '/chat': 'will get'
      });
      // разослать всем в неймспейсе
      chat.emit('message', {
        everyone: 'in'
        , '/chat': 'will get'
      });
    });
```

Client:
```jade
  script.
    var socket = io.connect('http://localhost:3000');
    socket.on('news', function (data) {
      console.log(data)
    });
    socket.emit('news-callback', {good: true});


    var chat = io.connect('http://localhost:3000/chat');
    chat.on('message', function (data) {
      console.log(data)
    });
    chat.on('connect', function () {
      chat.emit('hi!');
    });
```


## Debugging modules and applications
Позволяет отдебажить NodeJS шаг за шагом в консоли
```
node inspect custom.js
```

Возвращает ссылку по которой можно присоеденится к node js process
```
node --inspect custom.js
```

Тоже что и выше, но останавливается на первой линии 
```
node --inspect-brk custom.js
```


## Understanding the boomerang effect or callback hell
**callback hell** - ситуация возникающая при вложености нескольких коллбеков при работе с ассинхронным кодом.

Рецепты по избежанию:
- вынесение коллбеков в отдельные именованые методы
- дальше в отдельные модули(файлы)
- обрабатывать все ошибки





