# CSS/CSS3

## FlexBox 

[model](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

### Properties for the Container

**display**
```css
.container {
  display: flex; /* or inline-flex */
}
```


**flex-direction**
<img src="media/flex/flex-direction.svg" width="400"/>

У flex все элементы по дефолту располагаются в одну линию, **которая не прерывается** даже если контент не помещается в одну строку, но мы имеем возвожность задать направление и реверсивность.

```css
.container {
  flex-direction: row | row-reverse | column | column-reverse;
}
```


**flex-wrap**
<img src="media/flex/flex-wrap.svg" width="400"/>

Если же вам нужно задать возможность переноса на новую линию вы можете использовать соответвтвующее свойство

```css
.container{
  flex-wrap: nowrap | wrap | wrap-reverse;
}
```


**flex-flow**
This is a shorthand for the flex-direction and flex-wrap properties

```css
flex-flow: <‘flex-direction’> || <‘flex-wrap’>
```


**justify-content**
<img src="media/flex/justify-content.svg" width="400"/>

Способ задать расположение по главной оси(так как вы сами можете ее выбрать х,у)

```css
.container {
  justify-content: flex-start | flex-end | center | space-between | space-around | space-evenly | start | end | left | right ... + safe | unsafe;
}
```


**align-items**
<img src="media/flex/align-items.svg" width="400"/>

This defines the default behavior for how flex items are laid out along the **cross axis** on the current line


**align-content**
<img src="media/flex/align-content.svg" width="400"/>

**Важно**: эта вся история не работает если у вас контент располагается в одну линию.



### Properties for the Children

**order**
<img src="media/flex/flex-order.svg" width="400"/>

По дефолту элементы на странице располагаются по порядку, но у нас есть возможность выдернуть из общего флоу и поставить в нужну нам позицию. Имейте ввиду что по дефолту это значение "0", и если вы хотите сделать какой-то элемент первым - поставте "-1".

```css
.item {
  order: <integer>; /* default is 0 */
}
```


**flex-grow**
grow (анг.) - расширятся;

<img src="media/flex/flex/flex-grow.svg" width="400"/>

Устанавливает возможность для children расширятся, при этом эти цифри относительные.

```css
.item {
  flex-grow: <number>; /* default 0 */
}
```
Negative numbers are invalid.


**flex-shrink**
shrink (анг.) - сокращатся, уменшатся;

Если:
- 1 - то контент будет сокращен
    <img src="media/flex/flex-shrink-1.png"/>

- 0 - то контент будет распространятся до конца
    <img src="media/flex/flex-shrink-0.png"/>
    
    
**flex**
This is the shorthand for flex-grow, flex-shrink and flex-basis combined. 
The second and third parameters (flex-shrink and flex-basis) are optional. 
The default is 0 1 auto, but if you set it with a single number value, it's like <number> 1 0.

```css
.item {
  flex: none | [ <'flex-grow'> <'flex-shrink'>? || <'flex-basis'> ]
}
```


**align-self**
<img src="media/flex/align-self.svg" width="400"/>

Позволить перелоределить стили для отдельно взятого элемента.

```css
.item {
  align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```


## CSS Colors

- RGBA color values are an extension of RGB color values with an alpha channel - which specifies the opacity for a color.
- Hex value
- HSL (hue, saturation, lightness)
    - hue(цветной тон): degree on the color wheel from 0 to 360. 0 is red, 120 is green, 240 is blue.
    - saturation(насыщенность): 0% means a shade of gray and 100% is the full color
    - lightness(яркость): 0% is black, 100% is white
    
- currentColor: соответствует значению свойства color для текущего элемента;


## CSS: Fonts, text

- font-family: 'Arial' | sans-serif
- font-weight: bold | normal | 300 | 400
- font-stretch: возможность задать степень сжатости для буквы
    ![font stretch](media/text/font-stretch.png)

- font-style: normal | italic | oblique(косой)
- font-size
- font: shorthand for all font-* properties (italic bold 12px Arial)

### Font Resources

```css
@font-face {
    font-family: Gentium;
	font-style: normal;
    font-weight: bold;
    src: url(http://example.com/fonts/Gentium.woff);
}

p { font-family: Gentium, serif; }
```

- text-transform: управление регистрами (none | [capitalize | uppercase | lowercase ])
- white-space: устанавливает, как отображать пробелы между словами (normal | nowrap | pre | pre-line | pre-wrap | inherit). Отличаются по факту все эти значения по двум показателям (Перенос текста, Учитывание проблелов при переносе)
- word-break: устанавливает как делать перенос строк внутри слов.
    - break-all: перенос строк добавляется автоматически, чтобы слово поместилось в заданную ширину блока
        <img src="media/text/word-break-break-all.png" width="200"/>
        
- text-align
- text-align-last: задает выравнивание последней строки текста, когда свойство text-align установлено как justify.
    <img src="media/text/text-align-justify.png" width="250"/>
    
- word-spacing: устанавливает интервал между словами
    <img src="media/text/word-spacing.png" width="250"/>
    
- letter-spacing: определяет интервал между символами в пределах элемента
    <img src="media/text/letter-spacing.png" width="250"/>


## CSS: Selectors types

- Groups of selectors
    Это список селекторов разделенный запятой
    
    ```css
    h1, h2, h3 { font-family: sans-serif }
    ```

- Type selector
    Селектор грубо говоря по тегу
    
    ```css
    h3 { font-family: sans-serif }
    ```
  
- Universal selector (*)

- Attribute selectors
    *(Attribute presence and value selectors)*
    - [att]
    - [att=val]
    - [att~=val] - значения атрибутов могут перечисляться через пробел и val одно из этих слов
        ```html
        <div myCustom="test alpha"></div>
        ```
      
        ```css
        [myCustom~=alpha] { ... }
        ```
         
    - [att|=val] - значения атрибутов могут перечисляться через дефис и val одно из этих слов
        ```html
          <div myCustom="alpha-text"></div>
          <div myCustom="alpha-color"></div>
        ```
      
        ```css
        [myCustom|=alpha] { ... }
        ```
      
    *(Substring matching attribute selectors)*
    - [att^=val] - значение атрибута начинается с определённого текста
    - [att$=val] - значение атрибута оканчивается определённым текстом
    - [att*=val] - значение атрибута содержит указанный текст
    
- class selector

- ID selector

- Pseudo-classes
    - :link - link which wasn't visited yet
    - :visited
    - :hover
    - :active
    - :focus
    - :enabled
    - :disabled
    - :checked
    - :root - html element
    - :nth-child()
    - :nth-last-child()
    - :nth-of-type()
    - :nth-last-of-type()
    - :first-child
    - :last-child
    - :first-of-type
    - :last-of-type
    
- Pseudo-elements
    - ::first-line
    - ::first-letter
    - ::before
    - ::after

    <img src="media/pseudo-first-*.png" width="200"/>
    
    ```css
    p:first-letter {
        color: green;
    }
    
    p:first-line {
        color: blue;
    }
    ```
  
### Combination of selectors

- Descendant combinator (Nested selector)
    `h1 span`

- Combined selectors
    `.header.article-header`

- Child combinators (непосредственный наследник)
    - `h1 > span`
    
- Sibling combinators
    - math + p: следующий
    - math ~ p: который лежит на этом же уровне
    
    
## CSS: Selectors cascading and inheritance

**Каскад** - на самом простом уровне, этот принцип заключается в том, что порядок CSS правил имеет значение. В случаях, когда можно применить один из двух различных правил с одинаковой специфичностью, выбирается то правило, которое указано последним.
        
```css
h1 { 
    color: red; 
}
h1 { 
    color: blue; 
}
```        
Цвет который в итоге будет выбран - blue


**Принцип специфичности** заключается в том, как браузер решает то, которое с CSS правил применить тогда, когда различные правила относятся к различным селеторив, но все равно могут быть применены к одному и тому же элемента
```css
.main-heading { 
    color: red; 
}
        
h1 { 
    color: blue; 
}
```
```html
<h1 class="main-heading">This is my heading.</h1>
```        

Цвет который в итоге будет выбран - red


**Наследование** также необходимо понимать в этом контексте - некоторые значения свойств CSS, установленные для родительских элементов, наследуются их дочерними элементами, а некоторые - нет.

```css
body {
    color: blue;
}
span {
    color: black;
}
```

```html
<p>As the body has been set to have a color of blue this is inherited through the descendants.</p>
<p>We can change the color by targetting the element with a selector, such as this <span>span</span>.</p>
```

<img src="media/styles-inheritance.png" width="700"/>


## Bootstrap: Main features and concepts

- Easy to Use
    Любой кто знает CSS & HTML может использовать

- Responsive features
    Подстраивается под платформу
    
- Mobile-Friendly
    Поддерживает mobile first approach
    
- Много готовых стилизированих компонентов

- Кроссбраузерность

- JS-plugins

- Grid system

- Customizable Bootstrap
    Вы можете выбрать то что вам нужно у вашу сборку
    
## Bootstrap Grid

- В наличии классы на контейнер
- для строк и колонок (.row & .col)
- возможность указать сколько та или иная колонка будет занимать при разных разширениях
    (.col, .col-sm, .col-md, .col-lg, .col-xl)
    (.col-6 .col-md-4)

- задать выравнивание

## Bootstrap customization

Переходим по [ссылке](https://getbootstrap.com/docs/3.4/customize/), смотрим и играемся

<img src="media/bootstrap/bootstrap-less-component.png" width="800" />
<img src="media/bootstrap/bootstrap-jquery-components.png" width="800" />
<img src="media/bootstrap/bootstrap-less-variables.png" width="800" />



## CSS: Complex rules and defining styles structure 
(defining rules that applies to specific elements based on complex condition)

По факту мы говорим о сложных селекторах которые позволяют нам уточнить что именно нам нужно найти:

- h2 p - вложеный селектор
- h2 > p - непосредственный наследник
- h2 + p: следующий sibling
- h2 ~ p: sibling, который лежит на этом же уровне
- h2[test] - селекторы по аттрибутам
- использование псевдоклассов


## CSS: box model

По факту мы говорим о разнице между значениями для свойства box-sizing [content-box | border-box | padding-box]

- **content-box**: свойства width и height задают ширину и высоту контента и не включают в себя значения отступов, полей и границ.
    
- **border-box**: width и height включают в себя: border, padding, но не margin
    
- **padding-box**: N/A


## CSS: Typography & Fonts

- **Embedded OpenType (EOT)**: EOT – старый формат, разработанный Microsoft. EOT необходим для рендера шрифтов в старых версиях IE. EOT часто предоставляют в несжатом виде, поэтому если вам не нужно поддерживать IE8 и ниже, то лучше отказаться от этого формата.

- **TrueType (TTF)**: TTF – формат шрифта, разработанный Microsoft и Apple в 1980-х. Современные файлы TTF также называют TrueType OpenType шрифты. TTF полезен для расширения поддержки старых браузеров, особенно мобильных при необходимости.

- **Web Open Font Format (WOFF)**: WOFF разработан в 2009 – это формат-обертка для шрифтов TrueType и OpenType. Формат сжимает файлы и поддерживается во всех современных браузерах.

- **Web Open Font Format 2 (WOFF2)**: WOFF2 – обновление оригинального формата WOFF. Разработан Google и считается лучшим форматом из предложенных, так как обеспечивает меньший размер файлов и лучшую производительность в современных браузерах с поддержкой.


## CSS3: Transitions, animations, transforms

**Пример написания animation**

```css
p {
  animation-duration: 3s;
  animation-name: slidein;
}

@keyframes slidein {
  from {
    margin-left: 100%;
    width: 300%; 
  }

  to {
    margin-left: 0%;
    width: 100%;
  }
}
```

**Что быстрее?**

- Nope, the performance should be just about the same.
- In some cases CSS3 animation is slower than CSS3 transition as some painting work is not lifted to the GPU when using CSS3 animation.

**Почему transform лучше чем top,left?**

1) The drawing order of rendering layers is:

- layout layer
- paint layer
- compositor layer

A redraw in a layer will trigger redraw in subsequent layers.

Changing left or margin will trigger a redraw in **layout layer** (which, in turn, will trigger redraws in the other two layers) for the animated element and for subsequent elements in DOM.

Changing transform will trigger a redraw in **compositor layer** only for the animated element (subsequent elements in DOM will not be redrawn).

2) top, left можно применять только к position: absolute | relative | static. Плюс если вы хотите сдвинуть элемент влево на 5px, вам нужно учитывать существуюющее значение left для элемента, тоесть 100px -> 105px.


## CSS: Selectors performance

**Rating:**

1) ID, e.g. #header
2) Class, e.g. .promo
3) Type, e.g. div
4) Adjacent sibling, e.g. h2 + p
5) Child, e.g. li > ul
6) Descendant, e.g. ul a
7) Universal, i.e. *
8) Attribute, e.g. [type="text"]
9) Pseudo-classes/-elements, e.g. a:hover

При этом важно учитывать что браузер чтает селекторы справа-налево, а не слева-направо как человек. 
Поэтому в статье говорится о **key selector** - это последняя часть селектора. В селекторе `#content div` именно `div` будет ключевым.
То есть сначала браузер найдет все `div`, а потом только те что находятся в `#content`. Поэтому такой акцент делается на то чтобы сузить этот первый этап поиска. 


## CSS: For different media (printing, mobile etc.)

По факту мы сейчас говорим об использовании **@media queries**

```css
@media not|only mediatype and (mediafeature and|or|not mediafeature) {
  CSS-Code;
}
```

**mediatype**
где у нас есть возможность задать девайс к которому будут применятся стили:
- all
- screen - computer screens, tablets, smart-phones etc. 
- speech - для синтезаторов речи
- print - intended for paged material and for documents viewed on screen in print preview mode

**mediafeature**
- width
- height
- aspect-ratio
- orientation
- resolution
- ...


## CSS Measures: Values, units, vh, vw, mm, inch

### Fonts measurements

**px**
    Самая универсальная единица измерения, но которая уступает относительным единицам измерения
    
*Deprecated*

**mm** - миллиметр
**cm** - сантиметр
**pt** - пункт
**pc** - пика

1mm (мм) = 3.8px
1cm (см) = 38px
1pt (типографский пункт) = 4/3 px
1pc (типографская пика) = 16px

Так как браузер пересчитывает эти значения в пиксели, то смысла в их употреблении нет.


**em** - ширина буквы "М", берется относительно размера шрифта
em – относительные, они определяются по текущему контексту

**ex** - ширина буквы "Х"
**ch** - ширина символа "0"
Эти единицы используются чрезвычайно редко, так как «размер шрифта» em обычно вполне подходит.


**%** - относительно такого же свойства родителя (а может и не родителя, margin, width/height)

**rem** - reasonable em
Тоже относительная единица измерения, но зависищая от предыдущего установленного значения, а не от родителя напрямую.

```html
<li>Собака
  <ul>
  <li>бывает
    <ul>
    <li>кусачей
      <ul>
      <li>только
        <ul>
        <li>от жизни
          <ul>
          <li>собачей</li>
          </ul>
        </li>
        </ul>
      </li>
      </ul>
    </li>
    </ul>
  </li>
  </ul>
</li>
</ul>
```

```css
html {
    font-size: 14px;
  }
  li {
    font-size: 0.8em;
  }
```

Например имея такую каскадную разметку и используя "em" размер шрифта для каждого последующего элемента будет уменшен, так как шрифт считается относительно родителя.

```css
html {
    font-size: 14px;
  }
  li {
    font-size: 0.8rem;
  }
``` 

Но используя "rem" размер шрифта для каждой `<li>` будет взят относительно тега html.


*Относительно экрана*

- vw – 1% ширины окна
- vh – 1% высоты окна
- vmin – наименьшее из (vw, vh), в IE9 обозначается vm
- vmax – наибольшее из (vw, vh)


### Distance measurement

- deg - degrees, 360 degrees in a full circle.
- grad - gradians, also known as "gons" or "grades". There are 400 gradians in a full circle.
- rad - radians. There are 2π radians in a full circle.
- turn - turns. There is 1 turn in a full circle.


- dpi - dots per inch.
- dpcm - dots per centimeter.
- dppx - dots per px unit.

Последняя група измерений может быть использована при работе с `resolution` в `@media-query`.


## CSS Multi-column Layout

В этом блоке мы по факту говорим о групе css-свойств позволяющих игратся с колонками

- column-count
- column-gap
- column-rule-style
- column-rule-width
- column-rule-color
- column-rule
- column-span
- column-width


**column-count** - к-во колонок

**column-gap** - размер между колонками

**column-rule-style** - стиль для разделителя между колонок (none | hidden | dotted | dashed | solid | double | groove | ridge | inset | outset)

**column-rule-width** - размер для разделителя колонок

**column-rule-color** - цвет для разделителя

**column-rule** - shortcut для стилей разделителя (оч похож на border): `1px solid lightblue`

**column-span** - указывает, сколько столбцов должен охватывать элемент, имеет только два значения (all, none)
    
```css
.newspaper {
  column-count: 3;
  column-gap: 40px;
  column-rule: 1px solid lightblue;
}

h2 {
  column-span: all;
}
```

<img src="media/column-span-all.png" width="800"/>

```css
h2 {
  column-span: none;
}
```

<img src="media/column-span-none.png" width="800"/>

**column-width**
    Задает габариты для колонки (не работает вместе с column-count)

**column-fill**
    Задает правило как контент должен заполнять колонки при наличии свободного места
    
<img src="media/column-fill.png" width="700"/>


## CSS Variables

Переменные в CSS должны быть объявлены в селекторе CSS, который определяет его область действия.
Мы можем обьявлять собственные переменные стартуя идентификатор с "--" двойного дефиса.
Для того чтобы прочитать это значение используем конструкцию *var(var_name, optional_default_value)*

```css
:root {
  --main-bg-color: pink;
}

body {
  background-color: var(--main-bg-color);
}
```


## Preprocessors

### Idea

Лень двигатель прогресса. Хороший пример - принцип DRY - Don't repeat yourself. Я весьма подозреваю что вы стараетесь соблюдать этот принцип когда делаете макеты или чем вы там занимаетесь. Так же я весьма уверен что вы хотя бы пытались чуть автоматизировать рутину своей повседневной работы. Так же у вас могли быть ситуации когда вы переиспользовали какие-то элементы. Мало ли.

Так вот... CSS это тупая таблица стилей. Селектор и стили, ничего сверх умного тут придумать нельзя. Лет 5-10 назад было довольно модно держать один мегажирный CSS файл на 10К+ строк и радоваться жизни внося все больше изменений и т.д. Соответственно даже если вы соблюдаете всякие правила модульной верстки и все такое, у вас возникает несколько проблем:

- организация стилей, то есть держать все в одном файле не удобно особенно когда проект длится годами;

- Дублирование стилей и селекторов. По мере развития проекта появляются какие-то снипиты которые можно реюзать. Так же у вас может появиться масса однообразных селекторов отличающихся лишь немного. При модульных подходах вложенности редко имеет место быть но все же имеет. Но не будем забывать что большинство фигачит селекторы просто так. В итоге если мы переместили блок или переименовали класс какого-то блока нужно отредактировать еще массу селекторов.

- Привязка размеров и параметров к другим стилям, например у вас в стилях задана ширина блока, от нее зависят другие параметры, отступы для других блоков и т.д. Да, в css3 появился calc для этого но это было относительно недавно и только с недавних пор можно почти без опаски использовать эту штуку.

- Таблицы стилей, как и HTML ориентированы на удобный разбор этого добра машиной, но не человеком. Человек же существо ленивое и как-то вот лень лишний раз скобку поставить или точку с запятой. Просто лень.

Есть так же хорошее правило, или идея если хотите.... Если код можно сгенерить - его лучше сгенерить. То есть для решения всех выше перечисленных проблем придумали препроцессоры. Они как бы были и раньше всех этих scss/less/stylus но как-то не решали всех проблем и т.д. Что в итоге было предложено (перечисляю в том же порядке что и в списке выше).

- У CSS есть такая штука как @ import. Но не очень круто импортировать сотню стилей в продакшене. Стоит сделать так что бы все стили были склеены при сборке проекта. Эта идея в итоге развилась и если разработчик использует это дело правильно, можно зайти в любой файл со стилями и увидеть список всего от чего зависят эти стили. Какие стили подключаются и т.д. Причем один файл с зависимостями может быть подключен в нескольких файлах а препроцессор сам разберется как и куда все вставлять сгенерив максимально оптимизированный по структуре файл. А разработчик получил четкую структуру файлов и возможность быстро найти где что и от чего зависит.

- С селекторами проблему предложили решить наиболее логичным вариантом. Если у нас есть вложенные селекторы, то имеет смысл определять их внутри блока этого селектора. Это существенно упрощает поддержку стилей. Так же для управления снипитами и прочим добавили миксины - эдакие параметризованные или нет функции которые выплевывают шматок CSS. До появления штук вроде autoprefixer это был единственный способ писать поддерживаемые стили, использовать плюшки CSS3 и вообще новые плюшки и не сойти с ума от префиксов. Префиксы это только пример, там могут быть самые разные штуки позволяющие грамотно производить реюз стилей

- Проблему зависимостей значений стилей друг от друга решили... собственно самым очевидным способом - переменные. Это удобно, легко поддерживать и в умелых руках это мощный инструмент. Нужно поменять базовые цвета - не нужно лазить по 100500 блоков и править значения руками, можно поправить переменные и все будет хорошо.


### LESS (Features)

- Variables
    ```less
    @link-color:        #428bca; // sea blue
    .link {
      color: @link-color;
    }
    ```
  
- Nesting & Parent Selectors
    ```less
    a {
      color: blue;

      &:hover {
        color: green;
      }
    }
    ```
  
- Merge (обьеденить значение для одной css-property)
    ```less
    .mixin() {
      transform+_: scale(2);
    }
    .myclass {
      .mixin();
      transform+_: rotate(15deg);
    }
    ```
    ==>
    ```css
    .myclass {
      transform: scale(2) rotate(15deg);
    }
    ```
  
- Mixin (если вы приписываете "()" к селектору, то такой класс создан не буден, смотри output)
    ```less
    .my-mixin {
      color: black;
    }
    .my-other-mixin() { // will be skipped
      background: white;
    }
    .class {
      .my-mixin();
      .my-other-mixin();
    }
    ```
    ==> 
    ```css
    .my-mixin {
      color: black;
    }
    .class {
      color: black;
      background: white;
    }
    ```
    
- CSS Guards
    ```less
    @dr: if(@my-option = true, {
      button {
        color: white;
      }
      a {
        color: blue;
      }
    });
    ```
  
    ```less
    & when (@my-option = true) {
      button {
        color: white;
      }
      a {
        color: blue;
      }
    }
    ```
  
### SASS

#### Разница между SASS | SCSS

<img src="media/SASS%20%7C%20SCSS.png" width="300"/>

В основну это способ написания, у SCSS он больше похож на CSS, так как имеет фигурные скобки "{}" и ";" в конце стиля

#### Features

*(Все последующие примеры будут наводится с помощью SCSS)*

- Variables

    ```scss
    $font-stack:    Helvetica, sans-serif;
    $primary-color: #333;
    
    body {
      font: 100% $font-stack;
      color: $primary-color;
    }
    ```

- Nesting
    ```scss
    nav {
      a {
        display: block;
        padding: 6px 12px;
        text-decoration: none;
      }
    }
    ```
  
- Mixing
    ```scss
    @mixin transform($property) {
      -webkit-transform: $property;
      -ms-transform: $property;
      transform: $property;
    }
    .box { @include transform(rotate(30deg)); }
    ```
    ==>
    ```css
    .box {
      -webkit-transform: rotate(30deg);
      -ms-transform: rotate(30deg);
      transform: rotate(30deg);
    }
    ```
  
    Как видно из примера стили дописываются, а сам миксин не попадает в output
  
- Extends
    ```scss
    %message-shared {
      border: 1px solid #ccc;
      padding: 10px;
      color: #333;
    }
    .message {
      @extend %message-shared;
    }
    
    .success {
      @extend %message-shared;
      border-color: green;
    }
    ```
    ==> 
    ```css
    .message, .success {
      border: 1px solid #ccc;
      padding: 10px;
      color: #333;
    }
    
    .success {
      border-color: green;
    }
    ```
    Как мы видим механика отличается, extend создает селектор через запятую, а не дописывает стили
    
- Циклы
    ```sass
    $squareCount: 4
    @for $i from 1 through $squareCount 
      #square-#{$i} 
       background-color: red
       width: 50px * $i
       height: 120px / $i
    ```
    ==>
    ```css
    #square-1 {
      background-color: red;
      width: 50px;
      height: 120px;
    }
    
    #square-2 {
      background-color: red;
      width: 100px;
      height: 60px;
    }
    
    #square-3 {
      background-color: red;
      width: 150px;
      height: 40px;
    }
    ```
  
  
## CSS Methodologies

### BEM

BEM - Block Element Modifier. Идеей является разделение UI на отдельные независимые блоки.

**Block** - функционально независимый компонент на странице; Имя блока уже определяет его функции на странице(menu, article), а не его состояние.

    - При стилизации блоков не нужно избежать использование external geometry (margin) или позиционирование
    - CSS Tag(?) или ID селекторов чтобы сделать эти компоненты и их стили максимально reusable

**Element** - составная часть блока; (item, text, image, etc.)

    - пример имени такого элемента: "block-name__element-name"
    
**Modifier** - сущность описывающая внешний вид элемента

    - ("What size?" or "Which theme?" and so on — size_s or theme_islands)
    - its state ("How is it different from the others?" — disabled, focused, etc.) 
    - its behavior ("How does it behave?" or "How does it respond to the user?" — such as directions_left-top)

#### Concept

- Positioning elements inside a block
    ```html
    <body class="page">
        <div class="page__inner">
          <!-- header and navigation-->
          <header class="header">...</header>
    
          <!-- footer -->
          <footer class="footer">...</footer>
        </div>
    </body>
    ```
    ```css
    .page__inner {
        margin-right: auto;
        margin-left: auto;
        width: 960px;
    }
    ```
  
    Идея в том чтобы вынести стилизацию габаритов и отступов из этого компонента, сделав его максимально реюзабельным, а `page__inner` использовать на новом месте не обязательно
    
- Combining a tag and a class in a selector
    Не рекомендуется, так как это уточнит и сделает селектор более специфичным
    
- Nested selectors
    Позволяет использовать вложенные селекторы, но рекомендует минимизировать их использование. Вложенные селекторы увеличивают связывание кода и делают невозможным повторное использование.
  
  
#### Pro | Cons

**Pros**

- сокращает вложенность но повышает понятность для каждого отдельно взятого класса
- структурность кода
- максимальная его реюзабельность
- общие подходы к написанию и следованию его всей командой

**Cons**

- Излишество
- непривычные правила написания
- пересекающиеся правила и те которые накладываются на именование внешних компонентов, которые по сути не используют тот нейминг который у вас в приложении
- огромный список класов при взгляде на элемент
  
  