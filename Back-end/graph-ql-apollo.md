## Apollo

GraphQL client

## Schema Definition Language (SDL)

Human-readable schema syntax

```
type Book {
  title: String
  author: Author
}

type Author {
  name: String
  books: [Book]
}
```

Schema helps you understand what **Shape** of data and what **relationships** will be present.

### Schema types

**Scalar types:**

- `Int`: Signed 32‐bit integer
- `Float`: Signed double-precision floating-point value
- `String`: UTF‐8 character sequence
- `Boolean`: true or false
- `ID` (serialized as `String`): A unique identifier, often used to re-fetch an object or as the key for a cache.
While serialized as a String, ID signifies that it is not intended to be human‐readable.

**Object types**

```
type TypeName {
  fieldA: String
  fieldB: Boolean
  fieldC: Int
  fieldD: CustomType
}

type CustomType {
  circular: TypeName
}
```

### The Query type

A GraphQL query is for fetching data and compares to the GET verb in REST-based APIs.


### The Mutation type

Mutations are operations sent to the server to create, update or delete data. These are comparable to the  PUT, POST, PATCH and DELETE verbs on REST-based APIs.

### ! Exclamation mark

Exclamation mark tells us that this param is required
(should be non-nullable according to documentation [doc](https://graphql.org/learn/schema/))

### Examples of BE-FE Interactions

#### BE

```
const typeDefs = gql`
    type Company {
        id: ID!
        name: String
    }
    
    type Query {
        Companies: [Company]
        Company(id: ID!): Company
    }
    
    type Mutation {
        addCompany(name: String!): Company,
        updateCompany(id: ID!, name: String!): Company,
        deleteCompany(id: ID!): Company,
    }
`;
```

We declare custom type `Company` with `id` and `name` fields

```
type Company {
    id: ID!
    name: String
}
```

Also here an analogue GET operations

```
type Query {
    Companies: [Company]        // GET api/v1/companies - returns all companies
    Company(id: ID!): Company   // GET api/v1/company?id=1 - returns specific company by id (where id is required param)
}
```

Analogue of POST, PUT, DELETE

```
type Mutation {
    addCompany(name: String!): Company,
    updateCompany(id: ID!, name: String!): Company,
    deleteCompany(id: ID!): Company,
}
```

**resolver**

Object which contains implementation how exactly we should provide data

```js
const uuid = () => Math.round(Math.random() * 100);

const companies = [
    {
        id: '1',
        name: 'Apple'
    },
    {
        id: '2',
        name: 'Microsoft'
    }
];

const resolvers = {
    Query: {
        Companies: () => companies,
        Company: (root, { id }) => companies.find((company) => company.id === id)
    },
    Mutation: {
        addCompany: (root, { name }) => {
            const newlyCreatedCompany = {
                id: uuid(),
                name
            };
            companies.push(newlyCreatedCompany);
            return newlyCreatedCompany;
        },
        updateCompany: (root, { id, name }) => {
            const index = companies.findIndex((company) => company.id === id);
            if (index !== -1) {
                companies[index].name = name;
            }
            return companies[index];
        },
        deleteCompany: (root, { id }) => {
            const index = companies.findIndex((company) => company.id === id);
            const [ removedCompany ] = companies.splice(index, 1);
            return removedCompany;
        }
    },
};
```

#### FE

Examples of queries to fetch data:

- All companies (GET)
```js
const COMPANIES_QUERY = `
    {
      Companies {
        id
        name
      }
    }
`;
```

- Specific company by id (GET)
```js
const COMPANIES_QUERY = `
    {
      Company (id: 1) {
        id
        name
      }
    }
`;
```

- Create new company (POST)
```js
const CREATE_COMPANY = `
    mutation {
      addCompany (name: "Google") {
        id
        name
      }
    }
`;
```

- Update company by id (PUT)
```js
const UPDATE_COMPANY = `
    mutation {
      updateCompany (id: "2", name: "Google") {
        id
        name
      }
    }
`;
```

- Delete a company by id (DELETE)
```js
const DELETE_COMPANY = `
    mutation {
      deleteCompany (id: "2") {
        id
        name
      }
    }
`;
```

### Operation type and operation name

As we see previous we've used *query* and *mutation*.

By default all requests are query.

Operation types:
- query
- mutation
- subscription
- describes

Operation name doesn't have any influence on query itself, but it very useful on debugging on server-side.
And for better understanding what this request exactly do.

```js
const COMPANIES_QUERY = `
    {
      Companies {
        id
        name
      }
    }
`;
```

This code could be transformed into:

```js
const COMPANIES_QUERY = `
    query GetCompaniesList {
      Companies {
        id
        name
      }
    }
`;
```

### Variables

We may replace hardcoded values as input params using variables.

**Old case:**

```js
const CREATE_COMPANY = `
    mutation {
      addCompany ( name: "Test") {
        id
        name
      }
    }
`;
```

But if we want to pass company name as param we could use next construction:

```js
const CREATE_COMPANY = `
    mutation createNewCompany ($name: String!) {
      addCompany ( name: $name ) {
        id
        name
      }
    }
`;
```

*!!! Please, be attentive if name is required param don't forget use **"!"** in query schema*

```js
mutation createNewCompany ($name: String!) {
```

And in request we should specify variables:

```js
import axios from 'axios';

const localhostGraphQL = axios.create({
    baseURL: 'http://localhost:4000/api/v1',
});

const execQuery = () => {
    localhostGraphQL
        .post('', {
            query: CREATE_COMPANY,
            variables: { name: 'Test2' }
        })
        .then(getData)
        .then(console.log);
};
```

### Working with Input Types

Lets imagine that we wanna specify like a param some pre-defined structure (InputType).

Usual `type` doesn't suits this case. So it will be look like this:

```js
const typeDefs = gql`
    type Company {
        id: ID!
        name: String
    }
    
    input CompanyInput {
        id: ID!
        name: String
    }
    
    type Mutation {
        useCompany (company: CompanyInput!): Boolean
    }
`;
```

For this case we can't avoid duplication.
In GraphQL, an input cannot be used as a type and a type cannot be used as an input.
Unfortunately, even if the fields appear identical to an existing type, you always have to define a separate input to use as an argument.

And FE code will be smth like this:

```js
import axios from 'axios';

const localhostGraphQL = axios.create({
    baseURL: 'http://localhost:4000/api/v1',
});

const USE_COMPANY = `
    mutation createNewCompany ($company: CompanyInput!) {
      useCompany ( company: $company )
    }
`;

const execQuery = () => {
    localhostGraphQL
        .post('', {
            query: USE_COMPANY,
            variables: { company: { id: '69', name: 'Test2' } }
        })
        .then(getData)
        .then(console.log);
};
```

### Directives

To avoid doing manual string interpolation we may use:

```
query Hero($episode: Episode, $withFriends: Boolean!) {
  hero(episode: $episode) {
    name
    friends @include(if: $withFriends) {
      name
    }
  }
}
```

output

```json```
