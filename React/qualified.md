# React

*Какую проблему решает react?*

Самые «тяжелые» операции в web — работа с DOM. Реакт оптимизирует эту работу и берет на себя обязаности по работе с ним.
Как? **Virtual DOM** + обновление страницы за минимум «телодвижений».
Так же вы сами можете влиять на этот процесс посредством различных *hooks* которые предоставляет либа.

## JSX

**JSX** (JavaScript XML) - это синтаксическое расширение для JavaScript (функции CreateElement).  

```jsx harmony
<MyButton color="blue" shadowSize={2} action={this.action}>
  Click Me
</MyButton>
```

```js
React.createElement(
  MyButton,
  { color: 'blue', shadowSize: 2 },
  'Click Me'
)
```

- Babel компилирует JSX в вызовы React.createElement()
- Теги начинаются с заглавной буквы (чтобы отличить React-компоненты от html тегов)
т.к. неявно вызывается функция React.createElement, в файле должен быть import React from 'react'
- атрибуты: class === className, for === htmlFor
- встраивание пользовательского ввода в JSX является безопасным (не допускает XSS-атак) - потому что все преобразуется в строку, а не выполняется

## Virtual DOM

Это концепт, при котором мы создаем легковесную "виртуальную" копию DOM дерева и все модификации происходят именно с ней.
При этом мы просто указываем декларативно что именно мы хотим видить, при этом абстрагируясь от непосредственной 
модификации атрибутов, DOM-узлов, привязки events.

После внесения всех модификаций образовуется новая "виртуальная" модель, которая сравнивается с предидущей и именно эти изменения вносятся в реальный DOM.

> In simple words, you tell React what state you want the UI to be in, and it makes sure that the DOM matches that state.
The great benefit here is that as a developer, you would not need to know how the attribute manipulation, event handling 
or the manual DOM updates happen behind the scenes.

### Batch Update

React следует методологии *batch update* при котором изменения вносятся **"пачками"**, а не по одному.

**Rewind:**

- Frequent DOM manipulations are expensive and performance heavy.
- Virtual DOM is a virtual representation of the real DOM.
- When state changes occur, the virtual DOM is updated and the previous and current version of virtual DOM is compared. This is called “diffing”.
- The virtual DOM then sends a batch update to the real DOM to update the UI.
- React uses virtual DOM to enhance its performance.
- It uses the observable to detect state and prop changes.
- React uses an efficient diff algorithm to compare the versions of virtual DOM.
- It then makes sure that batched updates are sent to the real DOM for repainting or re-rendering of the UI.

Есть два варианта узнать, что данные изменились:

- Первый из них — «dirty checking» (грязная проверка) заключается в том,
чтобы опрашивать данные через регулярные промежутки времени и рекурсивно проверять все значения в структуре данных.
- Второй вариант — «observable» (наблюдаемый) заключается в наблюдении за изменением состояния.
Если ничего не изменилось, мы ничего не делаем. Если изменилось, мы точно знаем, что нужно обновить.

## Shadow DOM

https://learn.javascript.ru/shadow-dom

**Shadow DOM** – это средство для создания отдельного DOM-дерева внутри элемента, которое не видно снаружи без применения специальных методов.

Для того чтобы определить "точку вставки" (insertion point) обычного содержимого можно использовать тег `<content>`.

``` html
<p id="elem">Доброе утро, страна!</p>
```

```javascript
var root = elem.createShadowRoot();
root.innerHTML = `
    <p>Start caption</p>
    <content select=".header"></content>
    <content></content>
    <content select=".author"></content>
    <p>End caption</p>
`;
```

**Output:**
```text
Start caption

New article title
"Me!"
Lorem ipsum for some test text

End caption
```

В `<content>` атрибутом select можно указать конкретный селектор содержимого, которое нужно переносить.
Например, `<content select="h3"></content>` перенесёт только заголовки.

**Важные детали:**

- Тег `<content>` влияет только на отображение, он не перемещает узлы физически.
Как видно из картинки выше, текстовый узел «Доброе утро, страна!» остался внутри `p#elem`.
Его можно даже получить при помощи `elem.firstElementChild`.
- Внутри `<content>` показывается не элемент целиком `<p id="elem">`, а его содержимое,
то есть в данном случае текст «Доброе утро, страна!».

### ShadowRoot

`elem.shadowRoot` - корень внутреннего DOM-дерева. Если нужно работать с содержимым в Shadow DOM, то нужно перейти к иммено к нему

Также вы можете закрыть доступ к модификации shadowRoot средствами JS сменив режим.

```javascript
let shadow = elementRef.attachShadow({ mode: 'open' });
let shadow = elementRef.attachShadow({ mode: 'closed' });
```

```javascript
const elem = document.getElementById('myShadowElement');
const shadow = elem.attachShadow({ mode: 'closed' });

shadow.innerHTML = `
    <p>Start caption</p>
    <content select=".header"></content>
    <content select=".date">Date is not specified</content>
    <content></content>
    <content select=".author"></content>
    <p>End caption</p>
`;
```

![Result of closed Shadow DOM](media/shadow-dom-closed.png)


## Presentational and Container Components

Они отличаются имменно идеологически по приминению и у функциях которые они исполняют

**Presentational components:**

- По большей мере отвечают за представление, как все выглядит;
- Обычно имею HTML-разметку внутри;
- Не имеют зависимостей как REST, Flux, actions, store;
- Принимают данные по большей мере через `props`;
- Не определяют как бизнесс-данные подгружаются или изменяются;
- Редко имеют собственный `state` (но когда имеют, это скорей состаяние UI);
- В основном это `functional components`, если только им не нужен `state`, lifecycle hooks, performance optimization;
- Examples: Page, Sidebar, UserInfo, List.

**Container components:**

- По большей мере обьясняют, как компоненты работают;
- Обычно не имеют разметки, разве что оберточные `div` и подключают другие компненты;
- Обеспечивают данными и логикой другим компонентам;
- Имеют зависимости, как REST, Flux, store;
- Stateful, склонны служить источниками данных;
- Обычно сгенерированные как High-Order-Components: as connect() React Redux;
- Examples: UserPage, StoryContainer. FollowersSidebar;

## Stateless and Stateful

Отличаются наявностью или отсутсвием state (внутреннего состояния) компоненты.

**When would you use a stateless component?**

- When you just need to present the props
- When you don’t need a state, or any internal variables
- When creating element does not need to be interactive
- When you want reusable code

**When would you use a stateful component?**

- When building element that accepts user input
- ..or element that is interactive on page
- When dependent on state for rendering, such as, fetching data before rendering
- When dependent on any data that cannot be passed down as props

## Classes and Functions

Начиная с версии 0.14, компоненты могут быть обьявлены как классы и ф-ии. Компоненты как ф-и выглядят проще,
но они лишены нескольких фич (state, lifecycle hooks).

## Pure Components

PureComponent изменяет lifecycle-метод shouldComponentUpdate, автоматически проверяя, нужно ли заново отрисовывать компонент.
При этом PureComponent будет вызывать рендер только если обнаружит изменения в state или props компонента,
а значит во многих компонентах можно менять state без необходимости постоянно писать.

В качестве проверки он использует сравнение **shallowEqual**.

**shallowEqual** аналог операции `===`
- примитивы сравнивает по значению;
- ссылочные типы по ссылкам, а по значениям аттрибутов.

## Lifecycle

**Основные методы жизненного цикла:**

![Result of closed Shadow DOM](media/main_lifecycle_hooks.png)

**Полная диаграмма вызова hooks:**

![Result of closed Shadow DOM](media/full_lifecycle_hooks.png)

> Примечание:
  Устаревшие методы(для 16.8.6):
  - UNSAFE_componentWillMount()
  - UNSAFE_componentWillUpdate()
  - UNSAFE_componentWillReceiveProps()

### Обработка ошибок
Следующие методы вызываются, если произошла ошибка в процессе рендеринга, методе жизненного цикла или конструкторе любого дочернего компонента.

- static getDerivedStateFromError()
- componentDidCatch()

### API

- setState()
- forceUpdate()
- [defaultProps](https://ru.reactjs.org/docs/react-component.html#defaultprops)  
    Можно определить как свойство самого класса компонента, которое позволяет установить `props` класса по умолчанию.
    Это используется для неопределённых `undefined` пропсов, но не для пропсов со значением `null`(считается как значение).

- [propTypes](https://ru.reactjs.org/docs/typechecking-with-proptypes.html)  
    Можно опрежелить каким типам должны соответсвовать те или иные `props`
    
- [displayName](https://ru.reactjs.org/docs/react-component.html#displayname)  
    По умолчанию используется имя функции или класса, указанное при определении компонента,
    но например, можно использовать при отладке или создании High-Order Component.

### Список hooks:

- **constructor(props)**  
    Вы можете не использовать конструктор в React-компоненте, если вы не определяете состояние или не привязываете методы.
    
    В начале конструктора необходимо вызывать super(props). Если это не сделать, this.props не будет определён
    
    Цели:  
        - Инициализация внутреннего состояния через присвоение объекта this.state;  
        - Привязка обработчиков событий к экземпляру.
    
- **componentDidMount()**  
    Этот метод подходит для настройки подписок

- **shouldComponentUpdate(nextProps, nextState)**  
    Вызывается перед рендером, когда получает новые пропсы или состояние. Значение по умолчанию равно `true`.
    Этот метод не вызывается при первом рендере или когда используется `forceUpdate()`.
    
    При этом если вы опеределили компоненту как `PureComponent` то этот метод
    по дефолту будет производить `shallowCompare` для state & props.

- **componentDidUpdate(prevProps, prevState, snapshot)**  
    componentDidUpdate() вызывается сразу после обновления. Не вызывается при первом рендере.
    
    Метод позволяет работать с DOM при обновлении компонента. Также он подходит для выполнения таких сетевых запросов,
    которые выполняются на основании результата сравнения текущих пропсов с предыдущими.
    Если пропсы не изменились, новый запрос может и не требоваться.
    
    snapshot - передается, если был реализован `getSnapshotBeforeUpdate()`
    
- **static getDerivedStateFromProps()**  
    Он должен вернуть объект для обновления состояния или null, чтобы ничего не обновлять.
    
    ```javascript
    static getDerivedStateFromProps (nextProps, prevState) {
        if (nextProps.someValue !== prevState.someValue){
            return { someState: nextProps.someValue };
        }
        else return null;
    }
    ```
    
- **getSnapshotBeforeUpdate(prevProps, prevState)**  
    Это применяется редко, но может быть полезно в таких интерфейсах, как цепочка сообщений в чатах, в которых позиция прокрутки обрабатывается особым образом.
    
    Значение снимка (или null) должно быть возвращено.
    
> Примечание:
  Предохранители перехватывают ошибки в компонентах ниже по дереву. Предохранители не могут поймать ошибку внутри себя.
    
- **static getDerivedStateFromError()**  
    Этот метод жизненного цикла вызывается после возникновения ошибки у компонента-потомка.
    Он получает ошибку в качестве параметра и возвращает значение для обновления состояния.

- **componentDidCatch(error, info)**  
    Этот метод жизненного цикла вызывается после возникновения ошибки у компонента-потомка. Он получает два параметра:
        - error — перехваченная ошибка
        - info — объект с ключом componentStack, содержащий информацию о компоненте, в котором произошла ошибка.

```javascript
class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Обновите состояние так, чтобы следующий рендер показал запасной интерфейс.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    // Пример "componentStack":
    //   in ComponentThatThrows (created by App)
    //   in ErrorBoundary (created by App)
    //   in div (created by App)
    //   in App
    logComponentStackToMyService(info.componentStack);
  }

  render() {
    if (this.state.hasError) {
      // Здесь можно рендерить запасной интерфейс
      return <h1>Что-то пошло не так.</h1>;
    }

    return this.props.children;
  }
}
```

## State vs. Props

**Props**
- передаются в компонент из-вне
- props нельзя изменить (однонаправленный поток данных, сверху-вниз)

**State**
- хранит внутреннее состояние компонента между рендерами;
- setState() асинхронный, иммутабельный, можно внутрь передавать функцию или объект
    (если не зависим от прошлого состояния)
    
При изменении как одного так и другого происходит ре-рендер.

## Refs

Рефы дают возможность получить доступ к DOM-узлам или React-элементам, созданным в рендер-методе.

### Создание рефов

- React.createRef();
- callback-ref

```javascript
class MyComponent extends Component {
    constructor (props) {
        super(props);
        this.containerRef = React.createRef();
        this.inputRef = null;
    }

    componentDidMount () {
        console.log(this.containerRef.current); // React.createRef()
        console.log(this.inputRef); // callback-ref
    }

    render () {
        return (
            <div ref={this.containerRef}>
                <input
                    type="text"
                    ref={(element) => {
                        this.inputRef = element;
                    }}
                />
            </div>
        );
    }
}
```

Если ref колбэк определён как встроенная функция, колбэк будет вызван дважды во время обновлений: первый раз со 
значением `null`, а затем снова с DOM-элементом. Это связано с тем, что с каждым рендером создаётся новый экземпляр
функции, поэтому React должен очистить старый реф и задать новый. Такого поведения можно избежать, если колбэк в 
ref будет определён с привязанным к классу контекстом.


## Events

- React events используют нейминг в формате camelCase;
- JSX вы можете передать ф-ю для этого обработчика;
- React использует SyntheticEvent. SyntheticEvent сделан в соответствии с W3C спецификации (кроссбраузерно);

> SyntheticEvent, a cross-browser wrapper around the browser’s native event. It has the same interface as the browser’s 
native event, including stopPropagation() and preventDefault(), except the events work identically across all browsers.

## Data Fetching

> React itself doesn’t have any allegiance to any particular way of fetching data.

- axios (promises)
- fetch (browser api)

```javascript
componentDidMount() {
    axios.get(`http://www.reddit.com/r/${this.props.subreddit}.json`)
      .then(res => {
        const posts = res.data.data.children.map(obj => obj.data);
        this.setState({ posts });
      });
  }
```

## What is the significance of keys in React?

Keys are used for identifying unique Virtual DOM Elements with their corresponding data driving the UI.
They help React to optimize the rendering by recycling all the existing elements in the DOM.
These keys must be a unique number or string, using which React just reorders the elements instead of re-rendering them.
This leads to increase in application’s performance.

## [Context](https://ru.reactjs.org/docs/context.html)

**Контекст** позволяет передавать данные через дерево компонентов без необходимости передавать пропсы на промежуточных уровнях.

### Когда нужно использовать

Контекст разработан для передачи данных, которые можно назвать «глобальными» для всего дерева React-компонентов (например,
текущий аутентифицированный пользователь, UI-тема или выбранный язык). По возможности не используйте его,
так как это усложняет повторное использование компонентов.

**TodoList.js**
```jsx harmony
// --------------------------------------------- parent component -------------------------------------
import React, { PureComponent, createContext } from 'react';

const author = {
    name: 'Roman',
    age: 25
};

const TodoAuthorContext = createContext({ name: 'Bruce', age: 30 }); // default params for context

export const TodoAuthorConsumer = TodoAuthorContext.Consumer;

class TodoList extends PureComponent {
    render () {
        return (
            <div>
                <TodoAuthorContext.Provider value={author}>
                    <ul>
                        {this.props.todos.map((todo, index) =>
                            <TodoItem
                                key={`key-${index}`}
                                item={todo}
                                index={index}
                                toggleTodoStatus={this.toggleTodoStatus}
                            />
                        )}
                    </ul>
                </TodoAuthorContext.Provider>
            </div>
        );
    }
}
```

**TodoBody.js**
```jsx harmony
// ------------------------------------- child component -------------------------------------
import { TodoAuthorConsumer } from './TodoList';

class TodoBody extends PureComponent {
    render () {
        return (
            <TodoAuthorConsumer>
                {(author) => (
                    <Fragment>
                        <span>{this.props.text}</span>,
                        <span>Author: {author.name}: {author.age}</span>
                    </Fragment>
                )}
            </TodoAuthorConsumer>
        );
    }
}
```

```js
const TodoAuthorContext = createContext({ name: 'Bruce', age: 30 }); // default params for context
```

Если вы не определите `<TodoAuthorContext.Provider value={author}>` который бы обварачивал `<TodoAuthorConsumer>`,
то значение `author` будет взято из дефолтного. В случае если же `Consumer` был обернут в `Provider`, то это дефолтное
значение не учитывается.  

Особое внимание на синтаксическую конструкцию при использовании `Consumer`

```jsx harmony
<TodoAuthorConsumer>
    {(author) => (
        <Fragment>
            <span>{this.props.text}</span>,
            <span>Author: {author.name}: {author.age}</span>
        </Fragment>
    )}
</TodoAuthorConsumer>
```


