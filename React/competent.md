# React

## [High-Order Component](https://ru.reactjs.org/docs/higher-order-components.html)

**Компонент высшего порядка (Higher-Order Component, HOC)** — это один из продвинутых способов для повторного использования логики.
HOC не являются частью API React, но часто применяются из-за композиционной природы компонентов.

> Компонент высшего порядка — это функция, которая принимает компонент и возвращает новый компонент

```javascript
const EnhancedComponent = higherOrderComponent(WrappedComponent);
```

**Usage example:**

```javascript
const CommentListWithSubscription = withSubscription(
  CommentList,
  (DataSource) => DataSource.getComments()
);

const BlogPostWithSubscription = withSubscription(
  BlogPost,
  (DataSource, props) => DataSource.getBlogPost(props.id)
);
```

**Example of HOC:**

```javascript
// Это функция принимает компонент...
function withSubscription(WrappedComponent, selectData) {
  // ...и возвращает другой компонент...
  return class extends React.Component {
    constructor(props) {
      super(props);
      this.handleChange = this.handleChange.bind(this);
      this.state = {
        data: selectData(DataSource, props)
      };
    }

    componentDidMount() {
      // ...который подписывается на оповещения...
      DataSource.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
      DataSource.removeChangeListener(this.handleChange);
    }

    handleChange() {
      this.setState({
        data: selectData(DataSource, this.props)
      });
    }

    render() {
      // ... и рендерит оборачиваемый компонент со свежими данными!
      // Обратите внимание, что мы передаём остальные пропсы
      return <WrappedComponent data={this.state.data} {...this.props} />;
    }
  };
}
```

## Композиция, Агрегация, Ассоциация

**Ассоциация** - когда один класс включает в себя другой в качестве полей.

**Композиция** - составление целого(обектов, ф-ий, классов) на основе меньших их частей,
при этом сама сущность которая включает в себя эти части сама управляет их поведением.

> Композиция – это когда двигатель не существует отдельно от автомобиля. Он создается при создании автомобиля и полностью управляется автомобилем.
В типичном примере, экземпляр двигателя будет создаваться в конструкторе автомобиля. 

**Агрегация** - сопоставление независящих друг от друга частей, но в рамках определенной сущности.

> Агрегация – это когда экземпляр двигателя создается где-то в другом месте кода, и передается в конструктор автомобиля в качестве параметра. 

## Composition vs Inheritance

При наследовании мы передаем потомку полный функционал его родителя, плюс привязывает к определенному интерфейсу.
То есть потомок будет иметь функционал по тем же методам и полям что и у родителя.

При этом используя композицию мы можем выборочно добавлять ту или иную логику компоненту, можно и с другими ключами

```javascript
import React, { Component } from 'react';
import DataSource from '...';

export default function dataProvider (WrappedComponent, selectDataHandler) {
    return class extends Component {
        constructor (props) {
            super(props);
            this.state = {
                data: []
            };
        }

        async componentDidMount () {
            const data = await DataSource.getList();
            this.setState({ data });
        }

        render () {
            return (
                <WrappedComponent
                    {...this.props}
                    data={this.state.data}
                    getItem={DataSource.getEntityById}
                />
            );
        }
    }
}
```

В этом примере мы передали `WrappedComponent` как prop `getItem` логику методу на получение одной айтемы.
А не полностью всю логику обьекта DataSource.
А также добавили поле `data` тем самым расширив `WrappedComponent`

## Styling in React

- Add css-classes using `className` property;  
    Also u may use lib [classnames](https://www.npmjs.com/package/classnames#usage-with-reactjs)
    which simplify class list generation by simple rules

- Use [inline styles](https://reactjs.org/docs/dom-elements.html#style)
    
    ```javascript
    render () {
        const wrapperStyle = {
            color: 'red',
            width: '100%',
            backgroundColor: 'purple',
            WebkitTransition: 'all', // note the capital 'W' here
            msTransition: 'all' // 'ms' is the only lowercase vendor prefix
        };
        return (
            <div style={wrapperStyle} className='comments-list'/>
        );
    }
    ```
    
- Styled components

- Animations:  
    - [React Transition Group](https://reactcommunity.org/react-transition-group/)
    - [React Motion](https://github.com/chenglou/react-motion)
    
    
## React Router

Для корректной работы данного компонента нужно использовать компонент обертку (BrowserRouter, Router)

Для привязки роут-компонент используется следующая структура

```js
import { BrowserRouter as Router, Link, Route } from 'react-router-dom';

// pre-defined components
import Index from './components/Index';
import Contact from './components/Contact';
import Feedback from './components/Feedback';
```

```html
function App() {
  return (
      <Router>
          <div className="app-wrapper">
              <header className="app-header flex justify-start align-center">
                  <img src={logo} className="app-logo" alt="logo"/>

                  <nav>
                      <ul className='flex row'>
                          <li><Link to="/">Home</Link></li>
                          <li><Link to="/contact/">Contacts</Link></li>
                          <li><Link to="/feedback/">Feedback</Link></li>
                      </ul>
                  </nav>
              </header>

              <div className="content-wrapper">
                  <p>My custom layout</p>

                  <Route path="/" exact component={Index} /> /* Renders: This is index page (home page) */
                  <Route path="/contact/" component={Contact} /> /* Renders: Here info contact information about this site */
                  <Route path="/feedback/" component={Feedback} /> /* Renders: You can leave feedback on this page */
              </div>
          </div>
      </Router>
  );
}
```

### Route

Как видно из примера компонент `<Route/>` имеет несколько параметров

- **path** - сообственно какому путю соответсвует даный компонент;
    При этом можно указать и несколько роутов:
    
    ```jsx harmony
    <Route path="/users/:id" component={User} />
    <Route path={["/users/:id", "/profile/:id"]} component={User} />
    ```
    Routes without a path always match.


- **component** - компонент который будет отображатся по роуту;  
    Но как быть в случаях когда вам нужно помимо указания самого компонента нужно ему также передать props.
    Сделать это лучше используя атрибут `render`
    
    ```jsx harmony
    <Route
      path='/dashboard'
      render={(props) => <Dashboard {...props} isAuthenticated={true}
    />
    ```
    
    Использововать инлайн-функцию для генерации компонента под роут не лучшее решение. Почему?  
    
    > When you use component (instead of render or children, below) the router uses `React.createElement` to create a
    new React element from the given component.
    That means if you provide an inline function to the component prop, you would create a new component every render

- **exact** - роут будет отображен при *полном соответсвии* с указаным роутом;  

    ```html
    <Route path="/users" component={Users} />
    <Route path="/users/create" component={CreateUser} />
    ```
    
    Например имею такую структуру роутов при вводе в адрессную строку `/users/create` роутер найдет в свою очередь два соответсвия.
    
    React Router ищет не полную точность, а partial matching(частичное соответсвие) этим и обьясняется такое поведение.
    
    ```html
    <Route path="/" component={Index} /> /* Renders: This is index page (home page) */
    <Route path="/contact/" component={Contact} /> /* Renders: Here info contact information about this site */
    <Route path="/feedback/" component={Feedback} /> /* Renders: You can leave feedback on this page */
    ```
    
    В такой структуре например, где `<Route path="/" component={Index} />` первый роут указан без атрибута `exact`,
    при вооде в адрессную строку `http://localhost:3000/contact` мы увидим два текста:
    
    ```
    This is index page (home page)
    Here info contact information about this site
    ```
 
- **strict** - будет ли учитан trailing slash *(/)* при проверке соответсвий в роутинге с location.pathname
    
    ```html
    <Route strict path="/one/" component={About} />
    ```
    
    | path  | location.pathname | matches? |
    |-------|-------------------|----------|
    | /one/ | /one              | no       |
    | /one/ | /one/             | yes      |
    | /one/ | /one/two          | yes      |
    
    > Warning: strict can be used to enforce that a location.pathname has no trailing slash,
    but in order to do this both strict and exact must be true.
    
    | path | location.pathname | matches? |
    |------|-------------------|----------|
    | /one | /one              | yes      |
    | /one | /one/             | no       |
    | /one | /one/two          | no       |


### Switch

Отображает только первый результат который соответсвует адрессу.

Представте что мы захотели добавить обработку 404, в случае если ни один роут не подходит.
После добавления `<Route component={NotFound} />` при вводе некорректного роута мы увидим layout компонента `NotFound`

```jsx harmony
<Router>
    <div className="app-wrapper">
        <Route path="/" component={Index} exact />
        <Route path="/contact" component={Contact} />
        <Route path="/feedback" component={Feedback} />
        <Route component={NotFound} />
    </div>
</Router>
```

Но вот незадача, имея данную разметку и перейдя например по роуту `/contact` мы увидим на экране разметку двух компонентов: `Contact` & `NotFound`.
Почему? Потому что для последнего роута мы не указали `path` и как результат он соответсвует всем роутам.
Помочь в этом может использование `<Switch/>`. Который отрисует первое соответсвие(match).

```jsx harmony
<Router>
    <div className="app-wrapper">
        <Switch>
            <Route path="/" component={Index} exact />
            <Route path="/contact" component={Contact} />
            <Route path="/feedback" component={Feedback} />
            <Route component={NotFound} />
        </Switch>
    </div>
</Router>
```

### Prompt

Используется для того чтобы отловить момент ухода со страницы.  
- **message** - obvious
- **when** - [bool] определяет следует ли превентить уход

### Link vs NavLink

Оба этих компонента предоставляют возможность навигации по апликейшену.

#### Link

```jsx harmony
<Link to="/about">About</Link>

<Link
  to={{
    pathname: "/courses",
    search: "?sort=name",
    hash: "#the-hash",
    state: { fromDashboard: true }
  }}
/>
```

Также есть возможность получить внутреннюю ссылку на сам элемент `<a>` используя `innerRef`

```jsx harmony
const anchorRef = React.createRef();

<Link to="/" innerRef={anchorRef} />
```

#### NavLink

Специальная версия `<Link/>` которая позволяет дополнительно стилизировать линки через атрибуты

```jsx harmony
<nav>
  <ul className='flex row'>
      <li><NavLink to="/" exact activeClassName="selected-home">Home</NavLink></li>
      <li><NavLink to="/contact" activeClassName="selected-contact">Contacts</NavLink></li>
      <li><NavLink to="/feedback" activeClassName="selected-feedback">Feedback</NavLink></li>
      <li><NavLink to="/user/69" activeClassName="selected-user">User69</NavLink></li>
  </ul>
</nav>
```

**activeClassName** - определяет класс который будет дан элементу когда ссылка станет активной (по дефолту `active`)
**activeStyle** - определяет стили которые будет применены к элементу когда ссылка станет активной

Также вы можете использовать уже знакомые свойства для уточнения поиска совпадений:
- **strict**
- **exact**
- **isActive** - ф-ия в которой можно описать дополнительную логику для определения стоит ли маркровать сссылку как активную

### Query params

Вы можете иметь доступ к query params посредством свойства props.match.params.  
Как и показано ниже в примере.  

```html
<Route route="/user/:userId" component={User} />
```

```javascript
class User extends PureComponent {
    constructor (props) {
        super(props);
        this.user = this.getUserById(props.match.params.userId);
    }
    
    render () { /*...*/ }
}
```

## setState()

setState() actions are asynchronous and are batched for performance gains.  

setState() does not immediately mutate this.state but creates a pending state transition.
Accessing this.state after calling this method can potentially return the existing value.
There is no guarantee of synchronous operation of calls to setState and calls may be batched for performance gains.

This is because setState alters the state and causes re-rendering.
This can be an expensive operation and making it synchronous might leave the browser unresponsive.
Thus the setState calls are asynchronous as well as batched for better UI experience and performance.

## React.cloneElement

`React.cloneElement` clone and return a new React element using using the passed element as the starting point.
The resulting element will have the original element's props with the new props merged in shallowly.
New children will replace existing children. `key` and `ref` from the original element will be preserved.  

`React.cloneElement` only works if our child is a single React element.
For almost everything `{this.props.children}` is the better solution.
Cloning is useful in some more advanced scenarios, where a parent send in an element and the child component needs
to change some props on that element or add things like `ref` for accessing the actual DOM element.