# React

## React Hooks

Хуки — это функции, с помощью которых вы можете «подцепиться» к состоянию и методам жизненного цикла React из функциональных компонентов.

Хуки не работают внутри классов — они дают вам возможность использовать React без классов. 

С помощью хуков вы можете извлечь логику состояния из компонента, чтобы её протестировать или повторно использовать.
Хуки позволяют вам повторно использовать логику состояния, не затрагивая дерево компонентов.

Хуки позволяют разбить один компонент на маленькие функции по их назначению (например, подписке или загрузке данных), а не на основе методов жизненного цикла

> Хуки позволяют использовать больше возможностей React без написания классов

### useState

useState возвращает две вещи:
- текущее значение состояния 
- функцию для его обновления.

Она схожа с this.setState в классах, но не сливает новое и старое состояние вместе.

```js
const [ currentState, fnToChangeState ] = useState(initialStateValue);
```

**Example of usage with input**

```jsx harmony
import React, { useState } from 'react';
import { string } from 'prop-types';

const StyledInput = (props) => {
    const [ value, setValue ] = useState(props.initialValue);
    return (
        <div>
            <input type="text" value={value} onChange={(event) => setValue(event.target.value)}/>
        </div>
    );
};

StyledInput.propTypes = {
    initialValue: string
};

StyledInput.defaultProps = {
    initialValue: 'default value 13'
};
```

Заметь что если передать как дефолтное значение `undefined` для `useState`  

`const [ value, setValue ] = useState(props.initialValue);`  

то мы получим ошибку в виде `A component is changing an uncontrolled input...`
будь внимательным и проверяй установлено ли дефолтное значение для **controlled elements(input, textarea, select)**

### useEffect

> Если вам знакомы классовые методы жизненного цикла React, хук useEffect представляет собой совокупность методов componentDidMount, componentDidUpdate, и componentWillUnmount.

В этом примере мы рассмотрим как можно работать с рефами и со стейтом у функциональных компонентах.
В нем есть повторение одного и того же функционала, тем лучше для нас, так мы уверены что вывод корректен.

```jsx harmony
import React, { useEffect, createRef } from 'react';

function Example (props) {
    const outputClicksCounter = createRef();
    const [ clicks, changeClicksCount ] = useState(0);

    useEffect(() => {
        outputClicksCounter.current.innerText = clicks;
    });

    return (
        <div>
            <p>Now clicks count is {clicks}</p>

            <button onClick={() => changeClicksCount(clicks + 1)}>Increase clicks count</button>

            <p>
                Alternate clicks counter
                <span className='clicks-count' ref={outputClicksCounter}/>
            </p>
        </div>
    );
}
```

Если необходимо оформить подписку и отписку, то можно воспользоватся АПИ и вернуть метод
```js
useEffect(() => {
    function handleStatusChange(status) {
      setIsOnline(status.isOnline);
    }

    ChatAPI.subscribeToFriendStatus(props.friend.id, handleStatusChange);
    return () => {
      ChatAPI.unsubscribeFromFriendStatus(props.friend.id, handleStatusChange);
    };
  });
```

Если нужно исполнить код только раз(componentDidMount) вы можете указать вторым аргументом пустой массив

```js
useEffect(() => {
    loadDataOnlyOnce();
}, []);
```

### Custom hooks

**Пользовательский хук** — это js-функция, имя которой начинается с «use», и которая может вызывать другие хуки

Когда мы хотим, чтобы две JavaScript-функции разделяли какую-то логику, мы извлекаем её в третью функцию.
И компоненты и хуки являются функциями, поэтому с ними это тоже работает!

Пользовательский хук — это JavaScript-функция, имя которой начинается с «use», и которая может вызывать другие хуки.

```js
import React, { useState, useEffect } from 'react';
import { getList } from '../../api/todos';

//---------------------------------- Custom hooks ---------------------------------

function useTodoData () {
    const [ todos, setTodos ] = useState([]);

    useEffect(() => {
        async function fetchData () {
            try {
                const list = await getList();
                setTodos(list);
            } catch (e) {
                console.error('Can\'t fetch todos data');
            }
        }

        fetchData();
    }, []);

    return todos;
}

function useUserInfoData () {
    const [ user, setUser ] = useState({});

    useEffect(() => {
        const dummyUser = {
            name: 'Alpha',
            age: 25
        };
        setUser(dummyUser);
    }, []);

    return user;
}

//--------------------------------------- Components ---------------------------------------

export function TodosList () {
    const todos = useTodoData();
    const user = useUserInfoData();

    return (
        <div className='custom-hooks-wrapper'>
            <p>User's list: {user.name}</p>
            <ul>
                {todos.map(({ id, title }) => {
                    return (
                        <li key={id}>{title}</li>
                    );
                })}
            </ul>
        </div>
    );
}



export function FirstTodoItem () {
    const todos = useTodoData();
    const user = useUserInfoData();

    const getFirstItem = list => list[0] || {};
    const { title = '' } = getFirstItem(todos);

    return (
        <div>
            {title} And user name is: {user.name}
        </div>
    );
}
```

### useReducer

Используя этот хук вы можете описывать изменения стейта по аналогии с Redux

const [ state, dispatch ] = useReducer(counterReducer, initialCount, initialStateGenerator);

```jsx harmony
import React, { Fragment, useReducer } from 'react';

function initialStateGenerator (initialCount) {
    return {
        count: initialCount
    }
}

function counterReducer (state, action) {
    switch (action) {
        case 'increment':
            return {
                count: state.count + 1
            };

        case 'decrement':
            return {
                count: state.count - 1
            };

        default:
            return state;
    }
}

const Counter = ({ initialCount }) => {
    const [ state, dispatch ] = useReducer(counterReducer, initialCount, initialStateGenerator);

    return (
        <Fragment>
            <p>
                Current number is:
                <span>{state.count}</span>
            </p>

            <button onClick={() => dispatch('increment')}> ADD </button>
            <button onClick={() => dispatch('decrement')}> MINUS </button>
        </Fragment>
    );
};

Counter.defaultProps = {
    initialCount: 0
};

export default Counter;
```

### useRef

Возвращает изменяемый ref-объект, свойство .current которого инициализируется переданным аргументом (initialValue).
Возвращённый объект будет сохраняться в течение всего времени жизни компонента.

При этом разница между `.createRef()` и `.useRef()` что при использовании `createRef` каждый раз при ре-рендере будет созадана новая ссылка на переменную.

`const wrapper = useRef(null);`

А при использовании `.useRef()` wrapper после всех ре-рендеров будет ссылатся на одну и ту же область памяти.

```jsx harmony
function Caption () {
    const wrapper = useRef(null);
    return (
        <div ref={wrapper}/>
    );
}
```

### React.memo

React.memo - это компонент высшего порядка. Он похож на React.PureComponent, но предназначен для функциональных компонентов.

Если ваш функциональный компонент всегда рендерит одинаковый результат для одних и тех же пропсов,
вы можете обернуть его в вызов React.memo для повышения производительности в некоторых случаях, мемоизируя результат.
Это значит, что React будет использовать результат последнего рендера, избегая повторного рендеринга.

По умолчанию он поверхностно сравнивает вложенные объекты в объекте props. Если вы хотите контролировать сравнение,
вы можете передать свою функцию сравнения в качестве второго аргумента.

```js
function MyComponent(props) {
  /* рендер с использованием пропсов */
}

function areEqual(prevProps, nextProps) {
  /*
  возвращает true, если nextProps рендерит
  тот же результат что и prevProps,
  иначе возвращает false
  */
}

export default React.memo(MyComponent, areEqual);
```

### useMemo

Возвращает мемоизированное значение.

Передайте «создающую» функцию и массив зависимостей.
useMemo будет повторно вычислять мемоизированное значение только тогда, когда значение какой-либо из зависимостей изменилось.
Эта оптимизация помогает избежать дорогостоящих вычислений при каждом рендере.

```jsx harmony
import React, { useState, useMemo } from 'react';
import { number } from 'prop-types';

function ColumnsHeader ({ colsCount, colsWidth }) {
    // const columnsWidth = calculateColsWith(colsCount, colsWidth);
    const columnsWidth = useMemo(
        () => calculateColsWith(colsCount, colsWidth),
        [colsCount, colsWidth]
    );
    
    console.log(colsWidth); // 100
    const [clicks, changeClicksCount] = useState(0);

    return (
        <div>
            <p>Column Headers and clicks {clicks}</p>

            <button onClick={() => changeClicksCount(clicks + 1)}>
                Re-render
            </button>
        </div>
    );

    function calculateColsWith (colsCount, colsWidth) {
        console.log('calculate cols dimensions');
        return [colsCount, colsWidth];
    }
}

ColumnsHeader.propTypes = {
    colsCount: number,
    colsWidth: number
};

ColumnsHeader.defaultProps = {
    colsCount: 5,
    colsWidth: 100
};

export default ColumnsHeader;
```

В этом примере значение переменной `columnsWidth` посчитается только раз и не будет заново перещитано пока не
поменяется один из параметров указаных в засимимостях для ф-ии useMemo вторым аргументом. 


### useCallback

Возвращает мемоизированный колбэк.

Передайте встроенный колбэк и массив зависимостей. Хук useCallback вернёт мемоизированную версию колбэка, который изменяется только,
если изменяются значения одной из зависимостей. Это полезно при передаче колбэков оптимизированным дочерним компонентам,
которые полагаются на равенство ссылок для предотвращения ненужных рендеров (например, shouldComponentUpdate).


### useDebugValue

useDebugValue может использоваться для отображения метки для пользовательских хуков в React DevTools.

Может использоватся внутри пользовательських хуков.

![Deno](media/useDebugValue-demo.png)


### ??? Недостатки хуков, что они еще не покрывают

## Controlled components
https://ru.reactjs.org/docs/forms.html#controlled-components

## Performance

- Production сборка для деплоя на енв;
- [Анализ нагрузки используя табу "performance" в хроме](https://ru.reactjs.org/docs/optimizing-performance.html#profiling-components-with-the-chrome-performance-tab);
- Виртуализация больших обьемов информации (react-virtualized);
- smart метод жизненного цикла **shouldComponentUpdate()** (prefer usage of PureComponent);
- Иммутабельные данные

Использование иммутабельных данных позволяет вам спокойно использовать PureComponent, что позволит избежать ситуации
когда ре-рендер не произошел из-за скрытой мутации данных.

Иммутабельные структуры данных предоставляют вам дешёвый способ отслеживания изменений в объектах и всё, что вам нужно
для реализации shouldComponentUpdate. В большинстве случаев это даст вам хороший прирост в производительности.

## React Testing (Jest)

### Jest globals

- **describe(name, fn)** - creates a block that groups several related tests;

- **test(name, fn, timeout)** (it) - wrapper for one test case;
    - test.skip - *it.skip(name, fn) or xit(name, fn) or xtest(name, fn)* skips some test case
    - test.todo - highlights this test in the summary output
    - test.only - *it.only(name, fn, timeout) or fit(name, fn, timeout)* helps when you debug and want to run only some specific test

- **beforeAll(fn, timeout)** - runs a callback before any tests in file will execute. If result is promise Jest will wait until it finishes;
    ```js
    const globalDatabase = makeGlobalDatabase();
    
    beforeAll(() => {
      // Clears the database and adds some testing data.
      // Jest will wait for this promise to resolve before running tests.
      return globalDatabase.clear().then(() => {
        return globalDatabase.insert({testData: 'foo'});
      });
    });
    
    // Since we only set up the database once in this example, it's important
    // that our tests don't modify it.
    test('can find things', () => {
      return globalDatabase.find('thing', {}, results => {
        expect(results.length).toBeGreaterThan(0);
      });
    });
    ```
 
- **beforeEach(fn, timeout)** - runs a callback before each test in file;

- **afterAll(fn, timeout)** - runs a function after all the tests in this file have completed
    ```js
    const globalDatabase = makeGlobalDatabase();
    
    function cleanUpDatabase(db) {
      db.cleanUp();
    }
    
    afterAll(() => {
      cleanUpDatabase(globalDatabase);
    });
    
    test('can find things', () => {
      return globalDatabase.find('thing', {}, results => {
        expect(results.length).toBeGreaterThan(0);
      });
    });
    ```

- **afterEach(fn, timeout)** - runs a callaback after each test case completed;

**Live example:**

```js
import { SET_TODOS, TOGGLE_COMPLETE_STATUS } from '../constants/todos.constants';
import { setTodos, toggleTodo } from './todos.actions';

describe('TODOs action', () => {
    test('Should return setTodo with appropriate structure', () => {
        const mockTodos = [{ id: 1, title: 'Test' }];
        const mockAction = {
            type: SET_TODOS,
            payload: mockTodos
        };
        expect(setTodos(mockTodos)).toEqual(mockAction);
    });

    test.skip('Should return toggleTodo with appropriate structure', () => {
        const mockId = 1111;
        const mockAction = {
            type: TOGGLE_COMPLETE_STATUS,
            payload: mockId
        };
        expect(toggleTodo(mockId)).toEqual(mockAction);
    });

    test.todo('Should call addTodo with some structure');
});
```

**and output:**

![Test Output](media/test-output-1.png)

### Matchers [https://jestjs.io/docs/en/expect]

- **.toBe()** - the most universal operator

- **.extend(matchers)** You may use it to add your own matchers

    ```js
    expect.extend({
        myRangeCheck (received, from, to) {
            const isInRange = from <= received && received <= to;
            const message = isInRange ? 'Your num is in range' : 'You\'re missed';
            return {
                message: () => message,
                pass: isInRange
            };
        }
    });

    test('Custom case', () => {
        expect(5).myRangeCheck(0, 10);
    });
    ```
    
- **.anything()**
- **.any()**


- **.toBeFalsy()**
- **.toBeTruthy()**
- **.toBeNull()**
- **.toBeDefined()**


- **.toEqual()**
    ```js
    test('Should return setTodo with appropriate structure', () => {
        const mockTodos = [{ id: 1, title: 'Test' }];
        const mockAction = {
            type: SET_TODOS,
            payload: mockTodos
        };
        expect(setTodos(mockTodos)).toEqual(mockAction);
    });
    ```
- **.toStrictEqual()**


- **.toHaveBeenCalled()**
- **.toHaveBeenCalledWith()**
- **.toHaveBeenCalledTimes()**
- **.toHaveReturned()**


- **.toThrow(error)()**

```js
function drinkFlavor(flavor) {
  if (flavor == 'octopus') {
    throw new DisgustingFlavorError('yuck, octopus flavor');
  }
  // Do some other stuff
}

test('throws on octopus', () => {
  function drinkFlavor(flavor) {
      if (flavor == 'octopus') {
          throw new Error('yuck, octopus flavor');
      }
      // Do some other stuff
  }
  
  test('throws on octopus', () => {
      expect(() => drinkFlavor('octopus')).toThrow();
  });
});
```

### Mock of handling request in background

Представим что у нашем примере под капотом при вызове метода происходит выполнение запроса

**../../api/todos.js**
```js
import axios from 'axios';

const getData = ({ data }) => data;

export function getList () {
    return axios
        .get('https://jsonplaceholder.typicode.com/todos')
        .then(getData);
}
```


**todo.action.js**
```js
import { getList } from '../../api/todos';

export function loadTodos () {
    return async function (dispatch) {
        try {
            const todos = await getList();
            dispatch(setTodos(todos));
        } catch (err) {
            console.error(err);
        }
    }
}

export function setTodos (todos) {
    return {
        type: SET_TODOS,
        payload: todos
    }
}
```

И нам нужно протестировать разные кейсы с реализацией `getList()`

```js
import { SET_TODOS, TOGGLE_COMPLETE_STATUS } from '../constants/todos.constants';
import { setTodos, toggleTodo, loadTodos } from './todos.actions';

// указать что мы можем изменить имплементацию
// но не как именно 
jest.mock('../../api/todos');
import * as todoApi from '../../api/todos';

describe('TODOs action', () => {
    test('Should return appropriate structure on loadTodos with mock data', async () => {
        const mockTodos = [{ id: 1, title: 'Title2' }];
        const dispatchMock = jest.fn((data) => data);
        const mockAction = {
            type: SET_TODOS,
            payload: mockTodos
        };

        todoApi.getList.mockImplementation(() => Promise.resolve(mockTodos));

        const dataLoader = loadTodos();
        await dataLoader(dispatchMock);
        expect(dispatchMock).toHaveBeenCalledWith(mockAction);
    });

    test('Should doing noting when can\'t loadData for loadTodos with mock data', async () => {
        todoApi.getList.mockImplementation(() => Promise.reject('Error while loading data...'));

        const dispatchMock = jest.fn((data) => data);
        const dataLoader = loadTodos();
        await dataLoader(dispatchMock);

        expect(dispatchMock).not.toHaveBeenCalled();
    });
});
```

### Enzyme

Enzyme is a JavaScript Testing utility for React that makes it easier to test your React Components' output.

#### Diff between *shallow* and *mount*

Shallow is rendering without children components rendering
- constructor
- props
- render

Mount is full children rendering
- componentDidMount
- componentDidUpdate

### What is *Mock* and *Stub*

**Stub**

Stub is object which contains predetermined behaviour.

**Mock**

Mock is smth that is part of your test and correspond your expectation according to test.
Mock can be determined at runtime. 