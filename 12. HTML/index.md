# HTML

## Page Layouts with tables and div’s

**Таблицы плюсы/минусы**

Прежде всего нужно сказать что использование таблиц требует много писанины и смотрется не очень когда у несколько уровней вложености.
В отличии от таблиц с div'ами в этом плане это не смотрется так ужасно.

- плюсом является табулярная структура в которой определенному контенту назначается ячейка в которой он располагается
- можно задать опеределенную стилизацию контента используя аттрибуты (выравнивания)


- к минусам можно определить сложность в понимании написаного, например, использвание аттрибутов colspan, rowspan усложняют верстку. Также нужно вчитатся в структуру чтобы понять какая строка/ячейка за что отвечает;
- сложность в имплементации каких-либо изменений, таких например как добавить/скрыть еще одну колонку
 

**DIV плюсы.минусы** 

- меньше верстки (more readable, easier to maintain, faster to develop, less buggy, smaller in size)
- легко показывать/скрывать блоки (display/visibility)

- не очень семантично понятный код
- нужно понимать CSS и его применение на странице, так как последовательность блоков на странице не значит что они располагаются друг за другом (position: absolute). 
- злоупотребление div'ами
 
 
## iFrames
Inline Frame

IFrame — это кадр внутри кадра. Это компонент HTML-элемента, который позволяет встраивать документы, видео и интерактивные медиафайлы на страницу. Делая это, вы можете отобразить дополнительную веб-страницу на главной странице.

Элемент iFrame позволяет включать часть содержимого из других источников. Он может интегрировать контент в любом месте на вашей странице, без необходимости включать их в структуру веб-макета, как традиционный элемент.

По своей природе элемент іFrame не представляет никакой угрозы безопасности для вашей веб-страницы или ваших читателей. Частично, он был разработан, чтобы помочь создателям контента добавлять визуально привлекательный материал для читателей. Тем не менее, вам нужно обратить внимание на надёжность сайта при добавлении іFrame. Поскольку мы перестаем контролировать содержимое, то через iframe возможна подгрузка нежелательного контента (18+, вирусы и прочее).

**Плюсы/Минусы**

- Быстрая подгрузка видеоматериалов, карт, презентаций и прочего контента.
- Фреймы в HTML позволяют показать посетителю одновременно несколько страниц, которые абсолютно самостоятельны.


- Существует вероятность, что индексирование веб-сайта поисковыми роботами будет прерываться при переходе на фрейм.
- Угроза фильтров. Санкции от поисковых систем возможны, когда во фреймах находится контент запрещенного характера.
- Угроза использования кода который вы никак не конролируете

**Cross-origin policy**

Ограничение «Same Origin» («тот же источник») ограничивает доступ окон и фреймов друг к другу, а также влияет на AJAX-запросы к серверу.

Причина, по которой оно существует – безопасность. Если есть два окна, в одном из которых vasya-pupkin.com, а в другом gmail.com, то мы бы не хотели, чтобы скрипт из первого мог читать нашу почту.

Если одно окно попытается обратиться к другому, то браузер проверит, из одного ли они источника. Если нет – доступ будет запрещён.

```javascript
<iframe width="545" height="438" src="https://www.youtube.com/embed/fHaAsFZL1nQ"></iframe>
```

## Forms & form elements

Формы позволяют пользователям вводить данные, которые обычно отправляются на веб-сервер для обработки и хранения, или используются на стороне клиента для немедленного обновления интерфейса каким-либо образом.

HTML-код веб-формы состоит из одного или нескольких **form control** (иногда называемых виджетами), а также некоторых дополнительных элементов, помогающих структурировать общую форму.

```html
<form name="first-form" action="/my-handling-form-page" method="post" enctype="..."></form>
```

- *action* - адрес по которому обрабатываются данные формы
- *enctype* - способ кодирования данных формы
    - application/x-www-form-urlencoded - Вместо пробелов ставится +, символы вроде русских букв кодируются их шестнадцатеричными значениями
    - multipart/form-data - данные не кодируются. Это значение применяется при отправке файлов.
    - text/plain - пробелы заменяются знаком +, буквы и другие символы не кодируются.


**Основные теги**
- form
- label
- input
- select
- textarea - элемент формы, в которую можно вводить несколько строк текста, допустимо делать переносы строк, они сохраняются при отправке данных на сервер
- fieldset - рамка с подписью для групировки элементов формы

```html
<label for="nameInput">Name:</label>
<input id="nameInput" type="text" name="username">
```
Для того чтобы не писать аттрибуты `for` & `id` для связки лейбы с инпутом можно положить инпут внутрь лейбы (но некоторые либи матюжатся на это)

```html
<label>
    <span>Name:</span>
    <input type="text" name="username">
</label>
```

```html
<select form="..." multiple="" size="">
    <option value="a">Alpha</option>
    <option value="b" selected>Beta</option>
    <option value="c">Omega</option>
</select>
```

- form - форма с которой нужно связать этот селект
- multiple - возможность выбрать несколько элементов
- size - сколько айтемов отображать у выпающем списке

```html
<textarea maxlength="50" placeholder="..." readonly cols="10" rows="5"></textarea>
```

### Input & its types

- checked (checkbox, radiobutton)
- disabled
- form
- maxlength
- name
- pattern
- placeholder
- readonly
- type - и тут веселуха
- value

<img src="media/input-types-default.png"/>

<img src="media/input-types-html5.png"/>

<img src="media/input-types-browser-compatibility.png"/>

```html
<input
  type="file"
  multiple
  accept="file_extension|audio/*|video/*|image/*|media_type"
/>
```

- multiple - можно ли загружать несколько файлов
- accept - тип файлов которые можно загружать


```html
<input type="number" min="10" max="100" step="10">
```

```html
<input type="email" pattern="...">
```


## Tags

### Base tags

- div, span, p, a, img
- ul (ol, dl) -> li
- form (label, input, select, textarea, fieldset, legend(подпись для fieldset))

- abbr, acronym(устоявшееся сокращение: СПИД, ликбез, замполит, США, DOS)
- address - информации об авторе, ссылки  и т.д. Планируется, что поисковые системы будут анализировать содержимое этого тега для сбора информации об авторах сайтов.
- base - инструктирует браузер относительно полного базового адреса текущего документа
    - если адрес документа указан как `<base href="http://www.megasite.ru/hzchd/">`, то при добавлении рисунков достаточно использовать относительный адрес `<img src="images/labuda.gif">`. При этом полный путь к изображению будет `http://www.megasite.ru/hzchd/images/labuda.gif`, что позволяет браузеру всегда находить графический файл, независимо от того, где находится текущая веб-страница
    - target - имя окна или фрейма, куда будет загружаться документ, открываемый по ссылке. (фактически можно задать для всех ссылок `target='_blank'`)
    
- blockquote(целая цитата отдельным блоком), q(кусочек в предложении)
- cite - помечает текст как цитату или сноску на другой материал
- h1-h6, hgroup
- b/strong, i/em, ins(подчеркнутый)
- map, area
- small, sub, sup, del
- table, tbody, thead, tfoot, caption, td, tr, th
- pre, var, code, kbd(keyboard)

### HTML5 tags

- header, footer, section, article, aside, menu(выпадающее меню), nav(блок навигации)
- video, audio, src
- canvas, svg
- datalist -> option (автодополнение)
- details(toggle content) -> summary
- figure, figcaption
- main -основное содержимое на странице, Функционально режим чтения браузера будет искать наличие элемента `<main>`, а также элементов заголовка и секционных элементов, которые преобразовывают контент в специальный вид для чтения
- meter - ывода значения в некотором известном диапазоне
- progress

- mark, output, time
- bdi(направление текса), wbr - where break row(где сделать перенос если одно большое слово не помещается)

### Audio/Video

```html
<video width="320" height="240" autoplay poster="...">
  <source src="movie.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
    Your browser does not support the video tag.
</video>
```

Задать source для медиафайла можно и используя аттрибут `src`, но используя теги можно задать несколько source которые будут использованые если файл по ссылкам выше не будет найден.

**Примеры events:**
- abort
- canplay
- ended
- error
- pause
- play
- playing
- progress - when resource is downloading
- seeking - when the user is finished moving/skipping to a new position in the audio/video
- timeupdate
- volumechange


## Design Patterns

**HTML Structure**
Идея это распределить контент иерархически, то есть сделать вложеными. И поделить на три категории:
- структруные: html, head, body
- блочные
- инлайновые

**XHTML**
Идея сделать более строгие правила при написании HTML кода(скобки, закрывающие теги). Разница в том как страница будет парсится для XHTML это XML, HTML -> HTML.

**DOCTYPE**
По факту решение проблемы как сказать парсеру про тип документа HTML | XHTML

**Header elements**
Определить место для метаданных, подключаемых файлов, стилей

**Conditional Stylesheet**
В зависимости от браузера(IE or not) подключить тот или иной ресурс
```html
<!--[if lte IE 6]>
<!--[if gt IE 6]>
<!--[if IE 7]>
```
lte (less than or equals)
lt (less than)
gt (greater than)
gte (greater than or equals)

**Structural Block Elements**
Решаемая проблема это определение каркаса страницы, а точнее элементов которыми он является. Есть структурные блоки, внутри которых блочные с которыми может взаимодействовать JS и посковые системы.
(article, nav, table, section, ol, ul)

**Terminal Block Elements**
Если по книжке то: терминальные блоки - это контейнеры которые содержат инлайновые элементы. 
(h1-h6, p, blockquote, address, caption)
 
**Multi-purpose Block Elements**
Идея предоставить гибкость расширения структуры документа путем вложения структур
(div, li, td, form)

**Inline Elements**
Добавить явное значение к тексту, стилизовать текст, сделать его более семантически понятным
(span, strong, em, mark)

**Class and ID Attributes**
Добавить возможность групировки элементов для общей стилизации и дополнительной семантике, а также навпротив отделить некоторые из них.

## HTML Browser compatibility
HTML5 поддерживается у всех современных браузерах, в дополнение для всех, старых и новых, если элемент не распознан он считается как inline.

```css
header, section, footer, aside, nav, main, article, figure {
  display: block;
}
```


## Accessibility in HTML5

Рекоммендации призванные помочь пользователям с ограниченными возможностями (например с дефектами или отсутствием зрения).
А также подход писать верстку "по правильному" используя нужные аттрибуты и подходы

**role аттрибут**

При этом если писать семантически понятно, то они и не нужны
`<header>` -> role=”banner”
`<nav>` -> role=”navigation”
`<footer>` -> role=”contentinfo”
`<aside>` -> role=”complementary”
    

**Изображения**

Активное использование аттрибута *alt* для пояснения сути изображения, если изображение - линка, то куда она ведет, а если ничего нет - то просто пустой аттрибут
    
**Таблицы**

- название таблицы в тег - caption
- инфо о таблице summary аттрибут для тега table
- использование всех троих составных тегов thead, tfoot, tbody (tfoot должен располагатся до tbody чтобды он успел отрендерится)
- th - заглавные ячейки, td - с данными
- строчная ячейка-заголовок с аттрибутом scope="row"
- использование аттрибута `abbr` для указания коротких названий заголовков
    `<td abbr="vehicle">8970 Cross Country Vehicle</td>` 
- Не допускать вложенности таблиц. Если вложенная таблица простая — заменить её списком. В противном же случае screen-reader`ам будет сложно ассоциировать вложенные ячейки.

**Обработчики событий**
- не использовать чисто заточеных под мышку например, так как screen-reader'ы могут использовать события клавиатуры
    
**Навигация**
- теги `<A/>` всегда должны содержать аттрибут *href* хотябы "#" как дефолтное значение
- если для навигации используются блочные элементы, а не `<A/>` то они должны ометь возможность получить фокус через аттрибут tabIndex
    
**Формы**
- каждый элемент формы должен иметь свою `<LABEL/>`, а если это невозможно то использовать аттрибут `title`
- для сложных форм следует использовать `<FIELDSET/>` с соответсвующим ему тегом `<LEGEND/>` который пояснит группу.


## Web components

[Самая актуальная ссылка с новым APIv1](https://developers.google.com/web/fundamentals/web-components/customelements)

**Что это такое? Зачем?**

**Плюсы**
- возможность встраивания клиентской логики в большой сервер-рендерный проект
- возможность разбиения UI на кусочки со своей разметкой, стилизацией и логикой (в принципе как и в любых фреймворках типа React, Vue)
- изоляция написаного кода(ShadowDOM - CSS) что сводит на нет конфликты при разработке разными командами

**Минусы**
- Распространено мнение, что внедрение веб-компонентов сделает фреймворки ненужными, потому что встроенной функциональности будет достаточно для создания интерфейсов. Однако, это не так. Кастомные html-тэги действительно напоминают Vue или React компоненты, но этого недостаточно, чтобы заменить их целиком. В браузерах не хватает возможности частичного обновления DOM – подхода к описанию интерфейсов, когда разработчик просто описывает желаемый html, а фреймворк сам позаботится об обновлении DOM–элементов, которые действительно изменились по сравнению с прошлым состоянием
- использование ShadowDOM влияет на скорость рендеринга страницы
- глобальные имена компонентов. При создании веб-компонента его имя заносится в глобальный реестр, в приципе это не проблема, но если вы используете сторонюю библиотеку это может быть проблемой
- невозможность использовать tree-shaking(механизм удаления неиспользуемых импортов). В реакт мы явно импортируем компонент для того чтобы использовать, при подходе с веб-компонентами и их глобальным регистром эта связь не просматривается потому легко понять что компонент обьявлен и не используется невозможно
- типизация как в JS - динамическая. Как пример можно навести propTypes в реакте
- проблема с обновлением свойств в компонентах. У веб-компонентах этот механизм происходит с помощью getter & setter. И все ок если мы обновляем отдельно только по одному свойству, а если мы обновим три свойства одновременно - то буде проиведенно два лишних вызова. К сожалению у веб-компонентах не предусмотрено механизма который бы отслежевал обновления и делал их "умно"
- Путаница между свойствами и атрибутами
    
```javascript
div = document.createElement('div');

div.setAttribute('class', 'one');
div.className; // 'one'

div.className = 'two';
div.getAttribute('class'); // 'two'
```

```javascript
input = document.createElement('input');

input.getAttribute('value'); // null
input.value = 'one';
input.getAttribute('value'); // null

input.setAttribute('value', 'two');
input.value; // 'one'
```

Также стоит упомянуть о lifecycle method для веб компонентов:

- **connectedCallback**
    вызывается каждый раз когда элемент внедряется в DOM. Тут уместно запрашивать ресурсы и производить рендеринг. Большинство работы лучше откладывать на этот метод;


- **disconnectedCallback**
    вызывается каждый раз при удалении элемента из DOM и используется для освобождения памяти (отмена запросов, отмена интервалов, таймеров и обработчиков и пр.);


- **adoptedCallback**
    вызывается когда элемент был перемещен в новый документ, например вызовом document.adoptNode();


- **attributeChangedCallback**
    вызывается каждый раз при добавлении, изменении или замене атрибутов, входящих в список observedAttributes — этот метод будет вызван с тремя аргументами: именем атрибута претерпевшего изменения, его старым значением и его новым значением.


**ВАЖНО! The name of a custom element must contain a dash (-).**

Также важно отметить что веб-компоненты есть двух видов:
- **автономные пользовательские элементы** (autonomous custom element)
- **кастомизированые встроенные элементы** (customized built-in element)

Автономный пользовательский элемент не имеет особенностей, он может получать любые атрибуты, кроме атрибута **is**, такой элемент наследуется от обычного **HTMLElement**

```javascript
class HelloWorld extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `<p>Hello world</p>`;
  }
}

customElements.define('hello-world', HelloWorld);
```

```html
<hello-world />
```

В свою очередь кастомизированный встроенный элемент наследуется от специфических классов HTML элементов. И при регистрации третий аргумент для ф-ии `define()` обязателен(он скажет до какого тега кастомный элемент подвязан)

```javascript
class JumpingButton extends HTMLButtonElement {/*...*/}

customElements.define('jumping-button', JumpingButton, { extends: 'button' });
```

```html
<button is="jumping-button">Click Me!</button>
```


### Как писать
За создание пользовательских элементов веб-страницы отвечает интерфейс CustomElementRegistry, который позволяет регистрировать элементы, возвращает сведения о зарегистрированных элементах и т.д. Данный интерфейс доступен нам как объект window.customElement, у которого есть три интересующих нас метода:

В данном случае наведен пример обычного веб-компонента без ShadowDOM и html-import

```javascript
class Greeting extends HTMLElement {
    connectedCallback() {
        this.render();
    }

    // произошла установка списка атрибутов для срабатывания attributeChangedCallback
    static get observedAttributes() {
        return ['username'];
    }

    // (componentWillReceiveProps) но для каждого отдельного property
    // не делать лишней работы когда значения атрибута не изменились
    attributeChangedCallback(attr, prev, next) {
        if(prev !== next) {
            this[attr] = next;
        }
    }


    render () {
        if(!this.ownerDocument.defaultView) return;
        this.innerHTML = `
            <p>Hello user ${this.username}</p>
        `;
    }
}

customElements.define('greeting-message', Greeting);
```

### Shadow DOM

Для того чтобы заюзать `shadow dom` нужно проделать две маленкие модификации.
- В конструкторе заюзать метод `attachShadow({mode: 'open|close'})` который добавит теневое дерево к компоненту
    - mode: Строка указывающая на режим инкапсуляции для shadowDOM (будет ли разметка доступна для внешнего мира)

```javascript
class Greeting extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
    }
    // ...
}
```

- Для установки layout на отображение теперь нужно обратится к полю `shadowRoot`

```javascript
{
    // ...
    render () {
        if(!this.ownerDocument.defaultView) return;
        this.shadowRoot.innerHTML = `
            <p>Hello user ${this.username}</p>
        `;
    }
}
```

### Pass custom layout

Этот функционал нам предоставляет работа с аттрибутом `slot`.

```html
<greeting-message username="Roman">
    <p slot="comment">My custom passed layout</p>
</greeting-message>
```

```javascript
render() {
    if (!this.ownerDocument.defaultView) return;
    this.shadowRoot.innerHTML = `
        <style>
          :host{
             display: block;
             padding: 10px;
             background-color: lightgray;
             border: 1px solid green;
          }

          :host-context {
            background-color: lightgreen;
          }
        </style>

        <div>
            <p>Hello user ${this.username}</p>
            <slot name="comment"></slot>
        </div>
    `;
}
```

### Styles

CSS селекторы из внешнего окружения не будут применяться к содержимому теневого DOM, а его стили, соответственно, не вытекут наружу, что позволяет нам использовать простейшие селекторы, которые, кроме удобства написания также лучше по производительности.

- **:host** - в примере наведенном выше этот селектор будет ссылатся на `<greeting-message/>` тег.
- **:host(*selector*)** - стили будут применены если корень компонента матчится с селектором описаным в скобках.
    - root имеет класс: `:host(.some-class)`
    - root имеет аттрибут: `:host([customAttribute])`

- **:host-context(*selector*)** - применить стили если root находится *под*(в DOM структуре) селектором описаным в скобках
- **::slotted(*selector*)** - стилизировать контент переданый из вне который матчится селекторую (ВАЖНО! В отличии от :host не работает без скобок, селектор обязателен)


Го рассмотрим все эти примеры на практике
```html
<greeting-message username="Roman">
    <p slot="comment">My custom passed layout</p>
</greeting-message>

<!-- usage host pseudo-class with select  :host(.blue-bg) -->
<greeting-message username="Roman" class="blue-bg">
    <p slot="comment">My custom passed layout</p>
</greeting-message>

<!-- usage :host-context(.dark-theme) when host is located inside some selector(.dark-theme in our case) -->
<div class="dark-theme">
    <greeting-message username="Roman">
        <p slot="comment">My custom passed layout</p>
    </greeting-message>
</div>
```

```javascript
class GreetingsMessage extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.template = document.getElementById('greetings-message-template');
    }

    connectedCallback() {
        this.render();
    }

    // произошла установка списка атрибутов для срабатывания attributeChangedCallback
    static get observedAttributes() {
        return ['username'];
    }

    // (componentWillReceiveProps) но для каждого отдельного property
    // не делать лишней работы когда значения атрибута не изменились
    attributeChangedCallback(attr, prev, next) {
        if (prev !== next) {
            this[attr] = next;
        }
    }

    render() {
        if (!this.ownerDocument.defaultView) return;
        this.shadowRoot.innerHTML = `
            <style>
              :host{
                  display: block;
                  padding: 10px;
                  margin: 10px;
                  background-color: lightgray;
                  border: 1px solid green;
                  color: green;
              }
      
              :host(.blue-bg){
                  background: lightblue;
              }
      
              ::slotted(p) {
                  color: blue;
              }
      
              :host-context(.dark-theme) {
                  background-color: #202224;
                  color: #842F72;
              }
      
              :host-context(.dark-theme) ::slotted {
                  color: grey;
              }
            </style>

            <div>
                <p>Hello user ${this.username}</p>
                <slot name="comment"></slot>
            </div>
        `;
    }
}
```

![Web components](media/web-components.png)

*Стилизировать слоты(переданую разметку внутрь компонента)*

Заметьте что все стили что вы описываете внутри кастомного компонента не будут применены к разметке которую вы передаете внутрь компонента(через slot).
Есть два варика застилизировать:

- по тегу `slot`
- по псевдовдоклассу `::slotted(selector)`

```css
slot,
::slotted(p) {
    color: blue;
}
```

### Templates & Imports

Вот как например выглядит html шаблон:

```html
<template>
    <div>
        <img src="{{src}}" alt="{{alt}}">
    </div>
</template>
```

Браузеры при создании элемента `<template>` создают DocumentFragment, доступ к которому мы можем получить посредством свойства `.content`.

**ВАЖНО! Html import фича была удалена из браузеров и нужно юзать полифилы**

В тех примерах которые пишут сами google они создают темплейт в JS файле. И потом юзают в конструкторе компоненты как описано ниже.

```javascript
const template = document.createElement('template');
  template.innerHTML = `
    <style>
      :host {
        display: flex;
        flex-wrap: wrap;
      }
      ::slotted(howto-panel) {
        flex-basis: 100%;
      }
    </style>
    <slot name="tab"></slot>
    <slot name="panel"></slot>
  `;
```

```javascript
this.shadowRoot.appendChild(template.content.cloneNode(true));
this._tabSlot = this.shadowRoot.querySelector('slot[name=tab]');
this._panelSlot = this.shadowRoot.querySelector('slot[name=panel]');
```



## Spellcheck & Contenteditable

- spellcheck - указывает нужно ли проверять орфографию
- contenteditable - позволяет редактировать содержимое html tag

```html
<p contenteditable="true" spellcheck="true">This is a paragraph.</p>
```


## Custom scheme handlers
[Google link](https://developers.google.com/web/updates/2011/06/Registering-a-custom-protocol-handler)

navigator.registerProtocolHandler(protocol, uri, title)
Позволяет веб-сайтам зарегистрировать себя как возможный обработчик для конкретных протоколов.

*Возможность писать кастомные редиректы по протоколам*
По сути *протокол* - это shortcut для какого-либо обработчика, который можно указать при переходе.

Список зарезервированых протоколов: "mailto", "mms", "nntp", "rtsp", "webcal"

```html
<a href="web+alpha://google-search">Custom handler</a>

<script>
    navigator.registerProtocolHandler("web+alpha",
        "http://localhost:63342/test/second.html/?uri=%s",
        "Custom handler"
    );
</script>
```

Например при клике на эту линку, я буду редирекнут на страницу с таким URL: `http://localhost:63342/test/second.html/?uri=web%2Balpha%3A%2F%2Fgoogle-search`
```javascript
decodeURIComponent("web%2Balpha%3A%2F%2Fgoogle-search")
-> "web+alpha://google-search"
```


## HTML performance

- следовать семантике и синтаксису кода, а также принципам Accessibility in HTML5 (соответствующая разметка для людей с ограничеными возможностями)
- использовать html только для разметки(а не для стилизации)
- протестировать аппликейшен на разных девайсах и разрешениях


- следовать separation of concerns: Use HTML for structure, CSS for presentation and JavaScript for behaviour
- используйте HTML, затем переходите к CSS, если и когда это необходимо, и, если это действительно необходимо, переходите к JavaScript. Например во многих случаях можно использовать HTML для проверки формы и CSS или SVG для анимации - не прибегая к JavaScript
- разпределять CSS & JS code по отдельным файлам, это позволит заюзать кэширование и будет проще дебажить, на прод сборках весь код будет все равно минифицирован
- CSS в `<head>`, JS в конец `body`
- никаких inline handlers для JS: onclick etc
- булевые аттрибуты не имеют значений, если аттрибут есть - то он true
    Данный пример может законфюзить, так как он работает не так как ожидается
    ```html
    <video src="foo.webm" autoplay="false" controls="false">
    <!-- => -->
    <video src="foo.webm" autoplay controls> 
    ```
- следовать форматированию кода


### Repaint vs. Reflow

A **repaint** occurs when changes are made to elements that affect visibility but not the layout. For example, *opacity*, *background-color*, *visibility*, and *outline*

**Reflows** have a bigger impact. This refers to the re-calculation of positions and dimensions of all elements, which leads to re-rendering part or all of the document. Changing a single element can affect all children, ancestors, and siblings.

When reflows are triggered:

- Adding, removing or changing visible DOM elements
  The first is obvious; using JavaScript to change the DOM will cause a reflow.
  
- Adding, removing or changing CSS styles
Similarly, directly applying CSS styles or changing the class may alter the layout. Changing the width of an element can affect all elements on the same DOM branch and those surrounding it.

- CSS3 animations and transitions
Every frame of the animation will cause a reflow.

- Using offsetWidth and offsetHeight
Bizarrely, reading an element’s offsetWidth and offsetHeight property can trigger an initial reflow so the figures can be calculated.

**Recommendations:**

- *Use Best-Practice Layout Techniques*
    Не использовать таблицы так как парсер тратит время на расчет габаритов ячейки. Не использовать inline styles.
    
- *CSS Rules: number of lines and complex selectors*
    Использовать tools которые позволяют найти неиспользуемые стили
    
- *Minimize DOM depth*
    The smaller and shallower your document, the quicker it can be reflowed. It may be possible to remove unnecessary wrapper elements if you’re not supporting older browsers.
    
- Remove Complex Animations From the Flow*
    Ensure animations apply to a single element by removing them from the document flow with `position: absolute;` or `position: fixed;`. This permits the dimensions and position to be modified without affecting other elements in the document.

- *Modify Hidden Elements*
    Elements hidden with display: none; will not cause a repaint or reflow when they are changed. If practical, make changes to the element before making it visible.

- *Update Elements in Batch*
    - апплаить изменения стилей пачками (классом, а не списком изменений свойств)
    - использовать document fragment
    
- *Limit the Affected Elements*
    В пункте говорится о переключателе таб и предлагается сделать tab-content фиксированого размера, чтобы при переключении не триггерить reflow для соседних элементов
    
- *Анализировать производительность*
    DevTools в помощь  


## Canvas
**What is HTML Canvas**
Canvas - это элемент HTML 5, который предназначен для создания растрового изображения при помощи JavaScript.

```html
<canvas id="drawingSurface" width="800" height="600" style="border: 1px solid black;">
    Your browser does not support HTML5.
</canvas>
```

### Прямоугольники
- *strokeRect(x, y, ширина, высота)* // Рисует прямоугольник
- *fillRect(x, y, ширина, высота)*   // Рисует закрашенный прямоугольник
- *clearRect(x, y, ширина, высота)*  // Очищает область на холсте размер с прямоугольник заданного размера

```javascript
context.strokeRect(100, 100, 200, 200);
```

```javascript
context.fillStyle = 'pink';
context.fillRect(100, 100, 200, 200);
```


### Линии и дуги

- *beginPath()* // используется что бы «начать» серию действий описывающих отрисовку фигуры, каждый новый вызов этого метода сбрасывает все действия предыдущего и начинает «рисовать» занова.
- *closePath()* // является не обязательным действием и по сути оно пытается завершить рисование проведя линию от текущей позиции к позиции с которой начали рисовать
- *stroke()* // обводит фигуру линиями
- *fill()* // заливает фигуру сплошным цветом 

```javascript
const drawingSurface = document.getElementById('drawingSurface');
const context = drawingSurface.getContext('2d');
context.beginPath();
context.moveTo(100, 100);
context.lineTo(200, 200);
context.lineTo(300, 100);
context.stroke();
```

Своими словами: работу с canvas можно представить в виде реального рисования в жизни.
- где для начала вам нужно взять лист бумаги: `drawingSurface.getContext('2d')`.
- "сказать" что ты будешь рисовать не отрывая руки `context.beginPath()`
- навестись на точку с которой начинаешь рисовать: `context.moveTo(100, 100)`
- нарисовать линию `context.lineTo(200, 200);`
- еще одну `context.lineTo(300, 100)`
- убрать руку `context.stroke()`


### Круг

360 градусов = 2π радиан

![Radians in arc](media/canvas-arc.gif)

*arc(x, y, radius, startAngle, endAngle, anticlockwise)*
    - startAngle - угол начала дуги (в радианах)
    - endAngle - угол завершения дуги (в радианах)
    - anticlockwise - задает направления для рисования дуги
    
```javascript
context.beginPath();
context.arc(200, 200, 50, 0 * Math.PI, 1 * Math.PI);
context.stroke();
```

![Arc example](media/arc-canvas-example.png)
    

### Текст

```javascript
context.font = "30px Arial";
context.fillText("Hello World", 10, 50);
context.strokeText("Hello World", 10, 100);
```

![Arc example](media/text-canvas-example.png)


### Градиенты

**Linear Gradient**

*context.createLinearGradient(x0, y0, x1, y1)*

![Linear Gradient](media/linear-gradient.png)


```javascript
const gradient = context.createLinearGradient(10,0, 210,0);

gradient.addColorStop(0, 'green');
gradient.addColorStop(.5, 'cyan');
gradient.addColorStop(1, 'blue');

context.fillStyle = gradient;
context.fillRect(10, 10, 200, 100);
```

![Linear gradient example](media/linear-gradient-example.png)


**Draw Circular Gradient**

*context.createRadialGradient(x0, y0, r0, x1, y1, r1)*

![Radial Gradient](media/radial-gradient.jpg)

```javascript
context.arc(200, 200, 200, 0, 2 * Math.PI);
const gradient = context.createRadialGradient(200, 100, 50, 200, 200, 200);

gradient.addColorStop(0, 'yellow');
gradient.addColorStop(1, 'red');
context.fillStyle = gradient;
context.fill();
```

![Radial gradient example](media/radial-gradient-example.png)


### Изображения

*context.drawImage(img, x, y, width, height);*

```javascript
const image = new Image();
image.src = './cat1.jpg'; // any other path to image
context.drawImage(image, 0, 0);
```

![Image canvas](media/image-canvas-exmaple.png)


## SVG

Scalar Vector Graphic

XML-подобный язык разметки для векторной графики.

### Плюсы/Минусы
- при масштабировании качество изображения не ухудшится
- вы можете напрямую взаимодействовать с элементами на странице, так как каждый отдельный элемент - это соответствующий тэг.
- при большом к-ве обьектов начинаеть страдать перформанс


### Tags
- circle
	- r - радіус круга
	- cx - зміщення по осі х (але відносно центра фігури, а не її країв, як у margin)
	- cy - зміщення по осі у
	
```html
<circle r="50" cx="100" cy="100"></circle>
```

- ellipse(rx, ry, cx, cy)
	- rx - радіус еліпса по осі х
	- ry - радіус еліпса по осі у
	- cx - зміщення по осі х
	- cy - зміщення по осі у
	
```html
<ellipse rx="100" ry="50" cx="100" cy="100" fill="lightgreen"></ellipse>
```

- rect(width, height)
	- width
	- height
	- x - зміщення по відповіній осі
	- y - зміщення по відповіній осі
	- rx - заокруглення по осі х
	- ry - заокруглення по осі у
	
```html
<rect width="100" height="100" x="15" y="15" rx="10" ry="20"></rect>
```

- polygon(points="x,y x,y ...")
	- points - набір значень х,у для представлення вершин
	
```html
<polygon points="50,50 100,100 150,50"></polygon>
```

- line(x1,y1,x2,y2)
	- x1, y1 - координати початку;
	- x2, y2 - координати кінця
	- stroke - border-color - якщо не визначено - ви нічого не побачите
	
```html
<line x1="10" y1="10" x2="50" y2="10" stroke="green"></line>
```

- polyline(points)
	- points - набір значень х,у для представлення вершин
```html
<polyline points="0,100 50,25 50,75"></polyline>
```


- linear-gradient(id)
```html
<svg>
    <linearGradient id="linear-gradient">
        <stop offset="0%" stop-color="gold"/>
        <stop offset="100%" stop-color="teal"/>
    </linearGradient>
    <circle r="60" cx="150" cy="50%" fill="url(#linear-gradient)"></circle>
</svg>
```

**Разница между *polyline* and *polygon***
- polygon зєднує останню і першу координату, таким чином замикаючи фігуру
- polyline використовується для обозначення відкритих фігур


### Attributes
- fill _(default color - black)_
- fill-opacity
- cx & cy - відступ по осям від центру
- x & y - відступ по осям від краю
- rx & ry - округлення по осям (border-radius) або обозначення радіуса для певної осі
- points - координати точок
- stroke - колір границі
- stroke-width - ширина границі
- stroke-opacity - прозорість ліній
- stroke-linecap - кінець обводки (butt, round, square)
- stroke-linejoin - як обробляги вигини для ліній (miter(дефолт), round, bevel)
- stroke-dasharray - як обробяти пунктири (15 10, 15(15 15) - ширина ліній і пробілів, массив)
- stroke-dashoffset - сдвиг для рендерінга пунктирів
- xlink:href - запозичити стилі із іншого градієнту


## Разница между canvas/svg

**SVG** - масштабируемая векторная графика, технология рисования с хранением объектов в памяти (Retained mode graphics). Как и HTML, SVG имеет объектную модель документа (DOM). Это значит, что при использовании этой технологии для реализации интерактивных действий (таких как управление мышью и т.п.) со стороны программиста требуется меньше усилий, поскольку события привязываются непосредственно к элементам DOM.

**Сanvas** — это HTML элемент, который используется для создания растровой графики при помощи JavaScript. В отличии от svg, canvas работает с растровой графикой. Это технология мгновенного рисования, она не хранит свои элементы в дереве DOM, следовательно нет никакого способа изменить существующий рисунок или реагировать на события. Это означает, что, когда потребуется новый кадр, необходимо будет отрисовать всю сцену заново.


## Optimization pages for searching systems
В этом топике идет обсуждение троих составных частей работы поисковой системы:

### How search engines indexes pages (google or yandex for example)
**Сканирование**
    Чтобы представить в выдаче определенную страницу, ее нужно сперва добавить в базу *(индекс)*.
    Во время этого процесса Google обнаруживает новые или обновленные страницы и добавляет их в свою базу. Для облегчения работы он использует специальную программу - «Googlebots».
    Они посещают список URL-адресов, полученных в процессе прошлого сканирования и дополненных данными карты сайта, которую предоставляют веб-мастера и анализируют их содержание.
    Процесс сканирования происходит на регулярной основе в целях выявления изменений, изъятия «мертвых» ссылок и установления новых взаимосвязей.
    Тем ни менее, боты не посещают абсолютно каждый сайт. Чтобы попасть в список проверяемых, веб-ресурс должен быть рассмотрен, как достаточно важный.
    
**Индексация**
    процесс сохранения полученной информации в базе данных в соответствии с *различными факторами для последующего извлечения информации**. Ключевые слова на странице, их расположение, мета-теги и ссылки представляют особый интерес для индексации Google.

**Позиционирование**
    Когда пользователь вводит запрос, Google производит в базе данных поиск, подходящий под условия и алгоритмически определяет актуальность содержания, что выводит к определенному рейтингу среди найденных сайтов. Логично, что результаты, которые считаются более релевантными для пользователя поисковой системы, намеренно получают более высокий ранг, чем результаты, которые имеют меньше шансов обеспечить адекватный ответ.


### Rules for following to be indexed by bot
Поисковая система отправляет своего робота (он же паук, он же краулер) на поиски новых страниц, которые появляются в сети ежесекундно. Паучья стая собирает данные, передвигаясь по ссылкам с одной страницы на другую, и передает их в базу. Обработку информации производят уже другие механизмы.

Поисковые системы воспринимают сайты совсем не так, как мы с вами. В отличие от рядового пользователя, поисковый робот видит всю подноготную сайта. Если его вовремя не остановить, он будет сканировать все страницы, без разбора, включая и те, которые не следует выставлять на всеобщее обозрение.

При этом нужно учитывать, что ресурсы робота ограничены: существует определенная квота – количество страниц, которое может обойти паук за определенное время. Если на вашем сайте огромное количество страниц, есть большая вероятность, что робот потратит большую часть ресурсов на «мусорные» страницы, а важные оставит на будущее.

Поэтому индексированием можно и нужно управлять. Для этого существуют определенные инструменты-помощники, которые мы далее и рассмотрим.

**Robots.txt** - простой текстовый файл (как можно догадаться по расширению), в котором с помощью специальных слов и символов прописываются правила, которые понимают поисковые системы.

*User-agent* - Обращение к роботу(показывает, к какому поисковику относятся указанные ниже правила. Если адресатом является любой поисковик, пишем звездочку)
*Allow* - Разрешить индексирование.
*Disallow* - Запретить индексирование.
*Host* - Адрес главного зеркала.
*Sitemap* - Адрес карты сайта.
*Crawl-delay* - Время задержки между скачиванием страниц сайта.
*Clean-param* - Страницы с какими параметрами нужно исключить из индекса.


**meta tag**
```html
<meta name="robots" content="index,follow" />
```

**rel="canonical""**
```html
<link rel="canonical" target="_blank" href="http://site.ru/page/2/">
```

Для каждой страницы можно задать канонический (предпочитаемый) адрес, который и будет отображаться в поисковой выдаче. Прописывая атрибут в коде дубля, вы «прикрепляете» его к основной странице, и путаницы c ee версиями не возникнет.


### Single page application and potential problems with search engines
- Возможность сканирования сайта: системы Google должны быть способны просканировать сайт, учитывая его структуру. А у SPA по факту только одна страница.
- Бюджет сканирования: времени, выделенного Google на обработку сайта, должно хватить для его полноценной индексации.
- невозможности анализа сайта: системы Google не должны испытывать проблем в ходе анализа сайта с использованием технологий, используемых для формирования его страниц.


### Ways to address SPA issues with search engines
Дополнительные средства улучшения SEO для React веб-сайтов:

- *React Router v4*: компонент для создания дружественных к SEO «rout»-ов в React приложениях;
- *React Helmet*: едва ли не самый важный компонент для обеспечения SEO React-приложений, дающий возможность управлять meta-тегами приложения и отличающийся простотой применения.

**Изоморфный реакт** (приложение, у которого серверная и клиентская части имеют общую кодовую базу.) 

У тебя есть одностраничное приложение. Ты ходишь в нём по ссылкам, в адресной строке меняется путь, а с сервера при переходе подгружаются нужные данные. При этом при изначальной загрузке страницы клиентская часть толком никаких данных из базы ещё не содержит, и вынуждена показывать тебе заставку "Загрузка".

Так вот изоморфное приложение отличается тем, что при запросе с сервера страницы по какому-нибудь пути он самостоятельно подгрузит все данные, сложит их, например, в redux'овый стор, отрендерит страничку ровно в том виде, в котором она и должна быть, а затем отдаст её браузеру. Браузер уже сможет сразу показать всю вёрстку со всеми данными, а затем, после загрузки скриптов, всё будет дальше продолжать работать как одностраничное приложение.

**Пререндеринг** — это процесс предварительного рендеринга страниц веб-сайта с целью подготовки их к просмотру поисковым веб-ботом. Пререндер-сервис перехватывает запросы к веб-сайту, по «user-agent» запроса определяет тип клиента, и если запрос был сделан веб-ботом, сервис отсылает обратно предварительно отрендеренную кешированную статическую версию сайта. Если же запрос был сделан не поисковым веб-ботом, загружается обычная страница.


### Web components and search engines

- пробрасывание элементов через `slot`
- пробрасывание данных через аттрибуты
- использование полифилов



## WebGL

**WebGL** - это просто средство растеризации. Он отображает точки, линии и треугольники на основе написанного кода. Чтобы получить что-то от WebGL, вам нужно написать код, где, используя точки, линии и треугольники, и вы достигнете своей цели.

Код представлен в виде пар функций. Эти две функции - *вершинный* и *фрагментный* шейдер, и обе они написаны на очень строго типизированном языке, подобному C/C++, который называется GLSL. (GL Shader Language).

**Шейдер** — компьютерная программа, предназначенная для исполнения процессорами видеокарты (GPU).

Задача **вершинного шейдера** - вычислять положения вершин. Основываясь на положениях вершин, которые возвращает функция, WebGL затем может растеризовать различные примитивы, включая точки, линии или треугольники.

Задача **фрагментного шейдера** - вычислять цвет для каждого пикселя примитива, который в данный момент отрисовывается.

Вы устанавливаете настройки для каждого объекта, который хотите отрисовать, а затем выполняете эти две функции через вызов *gl.drawArrays* или *gl.drawElements*, которые выполнят шейдеры на графическом процессоре.
