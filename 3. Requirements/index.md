# Requirements

[//]: <------------------------------------------------ QUALIFIED ---------------------------------------------------->

## Definition

Требования... это спецификация того, что должно быть реализовано. В них описано поведение системы, свойства системы или ее атрибуты.

IEEE Standard Glossary of Software Engineering Terminology (1990) определяет требования как:

1. условия или возможности, необходимые пользователю для решения проблем или достижения целей;
2. условия или возможности, которыми должна обладать система или системные компоненты, чтобы выполнить контракт или удовлетворять стандартам, спецификациям или другим формальным документам;
3. документированное представление условий или возможностей для пунктов 1 и 2.

## Уровни требований

Требования к ПО состоят из трех уровней:

- business (бизнесс-требования)
- user (требования пользователеей)
- functional (функциональные требования)

Вдобавок каждая система имеет еще свои **non-functional(нефункциональные)** требования.

### Business requirements

Содержат высокоуровневые цели организации или заказчиков системы.
Как правило, их высказывают те, кто финансируют проект, покупатели системы, менеджер реальных пользователей, отдел маркетинга.
В этом документе объясняется, почему организации нужна такая система, то есть описаны цели, которые организация намерена достичь с ее помощью.

### User requirements

Описывают цели и задачи, которые пользователям позволит решить система.
К отличным способам представления этого вида требований относятся варианты использования, сценарии и таблицы «событие — отклик».
Таким образом, в этом документе указано, что клиенты смогут делать с помощью системы.

Пример варианта использования — «Сделать заказ» для заказа билетов на самолет, аренды автомобиля, заказа гостиницы через Интернет.

### Functional requirements

Определяют функциональность ПО, которую разработчики должны построить, чтобы пользователи смогли выполнить свои задачи в рамках бизнес-требований.
«Система должна по электронной почте отправлять пользователю подтверждение о заказе».
Функциональные требования описывают, что разработчику необходимо реализовать.

### Non-functional requirements

В дополнение к функциональным требованиям спецификация содержит нефункциональные, где описаны цели и атрибуты качества.
**Атрибуты качества (quality attributes)** представляют собой дополнительное описание функций продукта, выраженное через описание его характеристик, важных для пользователей или разработчиков.
К таким характеристикам относятся:
- легкость и простота использования;
- легкость перемещения;
- целостность;
- эффективность и устойчивость к сбоям.

Другие нефункциональные требования описывают внешние взаимодействия между системой и внешним миром, а также ограничения дизайна и реализации.

### System requirements

Обозначают высокоуровневые требования к продукту, которые содержат многие подсистемы. Говоря о системе, мы подразумеваем программное обеспечение или подсистемы ПО и оборудования.

### Benefits from a High-Quality Requirements Process

- уменьшение объема переделок
- сбор требований позволяет команде разработчиков лучше понять пользователей или реалии рынка
- вовлечение клиентов в процесс снижает вероятность появления ложных ожиданий, возникающих из-за различия того, в чем пользова тели нуждаются, и того, что разработчики выпускают
- недвусмысленно составленные документы облегчают тес тирование продукта

Выводы подитоженые в книге:

- меньше дефектов в требованиях;
- меньше переделок;
- меньше ненужных функций;
- ниже стоимость модификации;
- быстрее разработка;
- меньше разобщенности;
- меньше расползания границ;
- меньше беспорядка в проекте;
- точнее оценки тестирования;
- выше удовлетворение заказчиков и разработчиков.

### Characteristics of Excellent Requirements

Лучший способ определить, действительно ли ваши требования имеют желаемые атрибуты — попросить нескольких заинтересованных в проекте лиц внимательно просмотреть спецификацию.
Так, например, аналитики и разработчики не могут точно определить полноту или корректность документа, тогда как пользователям не удается оценить технические характеристики.

- **Полнота**
    Каждое требование должно полно описывать функциональность, которую следует реализовать в продукте. То есть оно должно содержать всю информацию, необходимую для разработчиков, чтобы тем удалось создать этот фрагмент функциональности.

    Если вы понимаете, что данных определенного рода не хватает, используйте пометку **«TBD»** (to be determined — необходимо определить) на полях как стандартный флаг для выделения такого места.

- **Корректность**
    Каждое требование должно точно описывать желаемую функциональность. Для соблюдения корректности необходима связь с источниками требований, например с пожеланиями пользователей или высокоуровневыми системными. Требования к ПО, которые конфликтуют с родительскими требованиями, нельзя считать корректными.
    
- **Осуществимость**
    Необходима возможность реализовать каждое требование при известных условиях и ограничениях системы и операционной среды.
    
- **Необходимость**
    Каждое требование должно отражать возможность, которая действительно необходима пользователям или которая нужна для соответствия внешним системным требованиям или стандартам. Кроме того, оно должно исходить от лица, которое имеет полномочия на запись положения.

- **Назначение приоритетов**
    Назначьте приоритеты каждому функциональному требованию, характеристике или варианту использования, чтобы определить, что необходимо для каждой версии продукта. Если все положения одинаково важны, менеджеру проекта будет трудно справиться с уменьшением бюджета, нарушением сроков, потерей персонала или добавлением новых требований в процессе разработки,

- **Недвусмысленность**
    Все читатели требований должны интерпретировать их одинаково, но естественный язык зачастую грешит многозначностью. Пишите документацию просто, кратко и точно, применяя лексику, понятную пользователям.
    
- **Проверяемость**
    Попробуйте разработать несколько тестов или примените другие приемы для проверки, например экспертизу или демонстрации, чтобы установить, действительно ли в продукте реализовано каждое требование.
    

## The Root Causes of Project Success and Failure

В статье нам приводится опрос сделаный Standish Group в 1994(ну такое). Согласно этому опросу они оценили кучу проектов и вынесли свой вердикт. 

Failure factors (percentage in survey, % of all projects):

- Lack of user input: 13%
- Incomplete requirements and specifications: 12%
- Changing requirements and specifications: 12%

Success factors:

- User involvement: 16%
- Executive management support: 14%
- Clear statement of requirements: 12%

Largest software development problems by category. (Data derived from ESPITI [1995])

![Largest software development problems by category](media/dev-problems-categories.png)

Норм [статья](https://project-management.com/top-10-main-causes-of-project-failure/) которая более актуальная(June 4, 2019)

[//]: <------------------------------------------------ COMPETENT ---------------------------------------------------->

## Product Champion

The product champion may have a wide variety of titles: **product manager**, **project manager**, marketing manager, engineering manager, IT manager, and project lead. But no matter the title, the job is the same. It's a big job. The champion must

Человек представляющий интересы конечных пользователей в рамках проекта.

- Manage the elicitation process(процессом выявления) and determine when enough requirements are discovered;
- Manage the conflicting inputs from all stakeholders;
- Be the representative of the official channel between the customer and the development team;
- Own the product vision;
- Advocate for the product;
- Manage the expectations of customers, executive management, and the marketing and engineering teams;
- Communicate the features of the release to all stakeholders;


## User classes and their characteristics

Помимо всего прочего, пользователей продукта можно подразделять по таким признакам:
- по частоте использования продукта;
- по опыту в предметной области и опыту работы с компьютерными системами;
- по требуемой им функциональности;
- по задачам, которые им приходится выполнять;
- по правам доступа к системе (например, обычный пользователь, гость или администратор).

![User Classes](media/user-classes.png)


## Quality attributes

Представляют собой дополнительное описание функций продукта, выраженное через описание его характеристик, важных для пользователей или разработчиков.

**Важны преимущественно для пользователей:**

- Доступность;
- Эффективность;
- Гибкость;
- Целосность;
- Способность к взаимодействию;
- Надежность;
- Устойчивость к сбоям;
- Удобство и простота использования.

**Важны преимущественно для разработчиков:**

- Легкость в эксплуатации;
- Легкая переносимость;
- Возможность повторно использовать;
- Тестируемость.


## Assumptions and constraints

### Assumptions

**Assumptions (ожидания)** - это ожидаемые события или обстоятельства, которые предполагаются в течение жизненного цикла вашего проекта.

Например:

- You will get all the resources you need;
- During the rainy season, cheap labor will be available;
- All relevant stakeholders will come to the next meeting;
- Your team members have all the required skills;
- All of the equipment is in good condition;
- The supplier will deliver consumables on time.

### Constraints

Constraints - это ограничения накладываемые на проект, например на бюджет, ресурсы или график.

Например:
- scope
- quality
- schedule
- budget
- resources
- risk tolerances (предельно допустимая степень рисков)

- You must finish 25% of the project work within 30 days;
- You must work within the available resources;

### Пример сочетания Assumptions и Constraints

Project: Build Cabin for Mr./Mrs Smith

**Requirements:**
• 3 bedrooms
• 2 baths
• Plan specs ….

**Constraints:**
• Budget: $120,000
• Schedule: 3 months
• Quality: Pass all city inspections

**Assumptions:**
• Budge: Funding provided prior to start
• Resource: Unlimited access to property
• Resource: Materials available for immediate delivery
• Scope: Use ABC Company Plans for Cabin, v 99.3.
• Scope: Plans are complete; changes will go through


## Basic Modeling techniques
**Data dictionary**(словарь данных) - общего хранилища, где определены значение, тип данных, длину, формат, необходимая степень точности л допустимые диапазоны значений или список значений всех элементов данных и атрибутов, используемых в приложении.

**Глоссарий** - это словарь узкоспециализированых терминов в какой-либо области с толкованием.


### Data-flow diagram (ERD)
диаграмма потоков данных

![DDF](media/ddf.png)
![DDF Explanation](media/ddf-explanation.png)

Way of representing a flow of a data of a process or a system (usually an information system). The DFD also provides information about the outputs and inputs of each entity and the process itself


### Entity-relation diagram (ERD)
![ERD](media/erd.jpg)
Показывает взяимосвязи между разнымы компонентами чего-либо. По классике этот пример наводился для баз данных.


### Process modeling
**Flowcharts** (блок-схемы) - это представление того или иного решения с помощью геометрических фигур(блоков), обозначающих операции, данные, поток и тд.

![Flow Chart](media/flowchart.png)

- паралелограм - блок входищих/исходящих данных;
- прямоугольник - блок вычислений;
- ромб - блок принятия решений, раздвоения;
- эллипс - начало/конец алгоритма;
- стрелки - связи;


**Activity diagram**
![Activity Diagram](media/activity-diagram.png)

Когда-то на уроках информатики в школе мы рисовали блок-схемы, чтобы наглядно изобразить алгоритм решения некоторой задачи. Действительно, моделируя поведение проектируемой системы, часто недостаточно изобразить процесс смены ее состояний, а нужно также раскрыть детали алгоритмической реализации операций, выполняемых системой. Как мы уже говорили, для этой цели традиционно использовались блок-схемы или структурные схемы алгоритмов. В UML для этого существуют диаграммы деятельности, являющиеся частным случаем диаграмм состояний. Диаграммы деятельности удобно применять для визуализации алгоритмов, по которым работают операции классов.


## Requirements artifacts
**Product vision** - описывает, что продукт представляет собой сейчас и каким он станет впоследствии.

**Project scope** - границы которые показывают, к какой области конечного долгосрочного образа продукта будет направлен текущий проект. В положении о границах определена черта между тем, что входит в проект и тем, что остается вовне. То есть указанные рамки также определяют ограничения проекта.