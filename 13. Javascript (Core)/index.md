# Javascript (Core)

## Types

В JS все типы делятся на две группы:
- примитивные
- ссылочные

In JavaScript there are 5 different data types that can contain values:
- string
- number(Infinity, NaN)
- boolean
- object
- function
- symbol <-

There are 6 types of objects:
- Object
- Date
- Array
- String
- Number
- Boolean

And 2 data types that cannot contain values:
- null
- undefined


## Property Attributes of an object

В этом разделе нужно сказать об модификаторах для property.
Когда ты определешь property через метод `Object.defineProperty()`

```javascript
const obj = {
    myField: 1
};

Object.defineProperty(obj, 'customField', {
    value: 42,
    writable: false, // можно ли изменять значение property
    configurable: true, // можно ли удалять property
    enumerable: true // будет ли свойство доступно при переборе
});
```

**writeable**

Если установлено у `false` то при попытке установить какое-либо значение результат будет зависить от `strict mode`:
- *non-strict*: ничего не произойдет
- *strict*: `"Uncaught TypeError: Cannot assign to read only property 'customField' of object '#<Object>' at script.js:14"`


**configurable**

аналогично как и с `writeable`:
- *non-strict*: ничего не произойдет
- *strict:* `Uncaught TypeError: Cannot delete property 'customField' of #<Object> at script.js:25`

**enumerable**

для того чтобы получить список свойств на перебор есть несколько методов:
- `Object.keys`
- оператор `for...in`

```javascript
console.log(Object.keys(obj)); // обьект с примера выше

for(let prop in obj) {
    console.log(prop);
}
```

если `enumerable: true` 
```
(2) ["myField", "customField"]
myField
customField
```

если `enumerable: false`
```
["myField"]
myField
```

**Геттеры и Сеттеры**

Заметь что `get() / set()` и `value / writeable` взамоисключающие свойства.

```javascript
function MyObject() {
    let customField = null;

    Object.defineProperty(this, 'customField', {
        get: function() {
            return customField + ' from the getter';
        },
        set: function(value) {
            customField = value + 100;
        }
    });
}

const mo = new MyObject();
mo.customField = 58;
console.log(mo.customField);
```

**Object.getOwnPropertyDescriptor**

Также очень интересная ф-ия которая позволяет вам посмотреть все эти настройки attributes property.
Примеры для наших обьектов:

```javascript
console.log(Object.getOwnPropertyDescriptor(obj, 'customField'));
console.log(Object.getOwnPropertyDescriptor(mo, 'customField'));
```

```
{value: 56, writable: true, enumerable: true, configurable: true}
{enumerable: false, configurable: false, get: ƒ, set: ƒ}
```


## Type Conversion

```javascript
typeof "John"                 // Returns "string"
typeof 3.14                   // Returns "number"
typeof NaN                    // Returns "number"
typeof false                  // Returns "boolean"
typeof [1,2,3,4]              // Returns "object"
typeof {name:'John', age:34}  // Returns "object"
typeof new Date()             // Returns "object"
typeof function () {}         // Returns "function"
typeof myCar                  // Returns "undefined" *
typeof null                   // Returns "object"
```

**Операторы для определения типа данных**

- typeof
- instanceOf


JavaScript variables can be converted to a new variable and another data type:
- By the use of a JavaScript function
- **Automatically** by JavaScript itself (например в if при сравнении значений или при исполнении математических операций)

```javascript
5 + null    // returns 5         because null is converted to 0
"5" + null  // returns "5null"   because null is converted to "null"
"5" + 2     // returns "52"      because 2 is converted to "2"
"5" - 2     // returns 3         because "5" is converted to 5
"5" * "2"   // returns 10        because "5" and "2" are converted to 5 and 2
```

```
if ('' === false)
```

**Методы для преобразования типов**

- .toString()
- .parseInt()
- .parseFloat()
- Number() or +'13'


## Increment / decrement

Чтобы хотелось отметить так это разницу между преикрементом и постинкрементом ++i / i++

```javascript
let a1 = 10;
let a2 = 10;
const b = a1++; // присвоит 10, а потом наростит значение для a1(11)
const c = ++a2; // сначала наростит значение для а2(11), а потом присвоит результат

console.log({a1, a2, b, c});
{
  a1: 11,
  a2: 11,
  b: 10,
  c: 11
}
```


## Variable scoping

**Локальные области видимости**
Области видимости для переменной - это по факту тело ф-ии, но с поправочкой только в стандарте ES5.
В стандарте ES6 это блок(любой блок), кусок кода обрамленный фигурными скобками `{}`

**Глобальные области видимости**
И тут поять таки играет роль используем ли мы 'strict mode'. 

При обьявлении переменной в non-strict она будет записана у window
```javascript
a = 2; // window.a = 2;
```

При использовании strict будет ошибка
```javascript
'use strict';
a = 2; // Uncaught ReferenceError: a is not defined
```

Но с использованием `var` все норм
```javascript
'use strict';
var a = 2; // window.a = 2;
```

**Время жизни переменной**
В локальном scope когда функция отрабоет.
В глобальном, когда будет закрыта таба браузера.


## Context of calling (*.call(), *.apply(), *.bind())

Все эти методы предоставляют возможности установить значения `this` для функции, но с определенным отличием:

При этом `.call()` и `.apply()` довольно похожи так как они вызовут функции с передаными контекстами, разница только в том как вы передаете параметры в эту ф-ию.

- .call(this, arg1, arg2)
- .apply(this, [arg1, arg2])

```javascript
function sum (b, c) {
    console.log(this.a + b + c);
}

const context = {a: 10};
sum.call(context, 1, 2); // 13
sum.apply(context, [20, 30]); // 60
```

- .bind(context, param1, param2)
bind же в свою очередь подменит контекст но не вызовет эту ф-ию на прямую - но вернет новую с переданым контектом.

```javascript
function sum (b, c) {
    console.log(this.a + b + c);
}

const context = {a: 10};
const _sum = sum.bind(context);
_sum(1, 2); // 13
```

При этом в `.bind()` есть возможность предопределить эти аргументы заранее, то есть сказать что параметр `b` всегда будет таким для всех вызовов ф-ии `sum`

```javascript
function sum (b, c) {
    console.log(this.a + b + c);
}

const context = {a: 10};
const _sum = sum.bind(context, 20);
_sum(30); // 60
```


## Error handling
Здесь мы говорим об использовании `try`, `catch`, `throw`

```javascript
try {
    throw new Error('Custom error'); // вылетит в консоль
} catch (e) {
    console.log(e);
}
```

**Структура обьекта Error**
- *name* - тип ошибки. Например, при обращении к несуществующей переменной: "ReferenceError", "SyntaxError" etc.
- *message* - текстовое сообщение о деталях ошибки.
- *stack* - везде, кроме IE8-, есть также свойство stack, которое содержит строку с информацией о последовательности вызовов, которая привела к ошибке.


## Closures
[Доходчивая статья](https://medium.com/@stasonmars/%D0%BF%D0%BE%D0%BD%D0%B8%D0%BC%D0%B0%D0%B5%D0%BC-%D0%B7%D0%B0%D0%BC%D1%8B%D0%BA%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B2-javascript-%D1%80%D0%B0%D0%B7-%D0%B8-%D0%BD%D0%B0%D0%B2%D1%81%D0%B5%D0%B3%D0%B4%D0%B0-c211805b6898)


*Замыкания* - это функции, которые ссылаются на внешние переменные (переменные, которые используются локально, но определенные в ограниченной области видимости).
*Замыкание* — это комбинация функции и лексического окружения, в котором эта функция была объявлена.
*Лексическое окружение* - список переменных в рамках определенной ф-ии.


Иными словами, эти функции "помнят" среду, в которой они были созданы.

```javascript
function alpha () {
    const name = 'Alpha';

    return function beta (surname) {
        return name + ' ' + surname;
    }
}
```

В этом примере ф-ия `beta()` является *замыканием*, так как она имеет доступ к переменной `name`.

**Примеры использования:**
- паттерн "Модуль"


## Hoisting

Всплытие - это процесс по перемещению всех объявлений в начало текущей области видимости.
Функция или переменная может быть использована до фактического оглашения.

```javascript
var x = 5; // Initialize x

elem = document.getElementById("demo"); // Find an element
elem.innerHTML = x + " " + y;           // Display x and y

var y = 7; // Initialize y
```

=>

```javascript
var x = 5; // Initialize x
var y;     // Declare y

elem = document.getElementById("demo"); // Find an element
elem.innerHTML = x + " " + y;           // Display x and y

y = 7;    // Assign 7 to y
```

Также в этих примерах видно что 'всплывает' только оглашение, а не инициализация определенным значением.


## Strict mode in JS

- заменяет исключениями некоторые ошибки, которые интерпретатор JavaScript ранее молча пропускал. 
- исправляет ошибки, которые мешали движкам JavaScript выполнять оптимизацию -- в некоторых случаях код в строгом режиме может быть оптимизирован для более быстрого выполнения, чем код в обычном режиме. 
- строгий режим запрещает использовать некоторые элементы синтаксиса, которые, вероятно, в следующих версиях ECMAScript получат особый смысл.

преобразование ошибок в исключения; изменения, упрощающие вычисление переменной в определённых случаях использования её имени; изменения, упрощающие eval и arguments

```javascript
"use strict";
                      // Предполагая, что не существует глобальной переменной
mistypeVaraible = 17; // mistypedVaraible, эта строка выбросит ReferenceError
                      // из-за опечатки в имени переменной

// Присваивание значения глобальной переменной, защищенной от записи
var undefined = 5; // выдаст TypeError
var Infinity = 5; // выдаст TypeError

// Присваивание значения свойству, защищенному от записи
var obj1 = {};
Object.defineProperty(obj1, "x", { value: 42, writable: false });
obj1.x = 9; // выдаст TypeError

// Присваивание значения свойству, доступному только для чтения
var obj2 = { get x() { return 17; } };
obj2.x = 5; // выдаст TypeError

// Задание нового свойства нерасширяемому объекту
var fixed = {};
Object.preventExtensions(fixed);
fixed.newProp = "ohai"; // выдаст TypeError
```

в строгом режиме попытки удалить неудаляемые свойства будут вызывать исключения (в то время как прежде такая попытка просто не имела бы эффекта)

```javascript
"use strict";
delete Object.prototype; // выдаст TypeError
```

запрещает дупликацию параметров у ф-ии

```javascript
function sum(a, a, c) { // !!! синтаксическая ошибка
  "use strict";
  return a + a + c; // ошибка, если код был запущен
}
```

запрещает установку свойств primitive значениям

```javascript
false.true = '';         // TypeError
(14).sailing = 'home';   // TypeError
'with'.you = 'far away'; // TypeError
```

**eval and arguments**
В строгом режиме снижается количество странностей в поведении arguments и eval, оба из которых примешивают определённое количество магии в обычный код. Так eval добавляет или удаляет переменные и меняет их значения, а переменная arguments может удивить своими проиндексированными свойствами, которые являются ссылками (синонимами) для проименованных аргументов функции.

- ключевые слова eval и arguments не могут быть переопределены или изменены
- поля объекта arguments не связаны с проименованными аргументами функции, а являются их продублированными копиями значений
- arguments.callee больше не поддерживается


## Enhanced Object Literals
Возможность проще создавать обьекты в JS без вызовов каких-либо ф-ий

```javascript
var o = {};
var o = {a: 'foo', b: 42, c: {}};
```

## Класс Object

- Object.assign() - Создаёт новый объект путём копирования значений всех собственных перечислимых свойств из одного или более исходных объектов в целевой объект.
- Object.create() - Создаёт новый пустой объект с прототипом переданым в качестве аргумента.
    ```javascript
    const myObj = {
        alpha: 1,
        beta: 2
    };
    
    const newProto = Object.create(myObj);
    // newProto
    {
      __proto__: {
          alpha: 1,
          beta: 2
      }
    }
    ```
  
- Object.defineProperty() 
- Object.defineProperties() 
- Object.getOwnPropertyDescriptor()


- Object.freeze() - Замораживает объект: предотвращает добавление новых свойств к объекту, удаление старых свойств из объекта и изменение существующих свойств или значения их атрибутов перечисляемости, настраиваемости и записываемости.
- Object.seal() - предотвращая добавление новых свойств к объекту и делая все существующие свойства не настраиваемыми. Значения представленных свойств всё ещё могут изменяться, поскольку они остаются записываемыми
- Object.preventExtensions() - Предотвращает любое расширение объекта.


- Object.observe() - используется для асинхронного обзора изменений в объекте. Он предоставляет поток изменений в порядке их возникновения.
- Object.is() - Определяет, являются ли два значения различимыми (то есть, одинаковыми)
    - в отличии от оператора `===` не делает приобразование к единому типу
    - обьекты сравнивает по ссылкам
    - Object.is(NaN, NaN) - true 


## Dependency injection (common, AMD, namespase)

### AMD

[AMD and RequireJS](https://code.tutsplus.com/tutorials/next-generation-javascript-with-amd-and-requirejs--net-21596)

AMD (Asynchronous Module Definition)

Это только спецификация как писать JS modules, а чтобы загружать файлы нам нужен лоадер - requirejs например.

- стандартный путь как писать куски функционала
- легко адаптировать код других людей под шаблон
- так как структура определена мы можем использовать единый подход как загружать и обрабатывать JS-модули
- возможность писать несколько модулей в одном файле при этом каждый из них инкапсулирован
    
**Пример**

file structure
```
test
|-- _requirejs
|   |-- require.js // lib from the site
|   |-- main.js
|   |-- utils.js
|-- index.html
```

```html
<html>
<head>
    <script data-main="requirejs/main" src="requirejs/require.js"></script>
</head>
<body/>
</html>
```

*data-main* - definition of the entry point

```javascript
// main.js
// ['utils'] cause utils.js on the level (path from main.js not from index.html(requirejs/utils) ) 
require(['utils'], function(utils) { 
    console.log('main module', utils);
});
```

```javascript
// utils.js
define(function() {
    return {
        alpha: () => 'alpha',
        beta: () => 'beta'
    };
});
```

**Немного лирики:**

*define* is used to define named or unnamed modules based on the proposal using the following signature:

```javascript
define(
    module_id /*optional*/, 
    [dependencies] /*optional*/, 
    definition function /*function for instantiating the module or object*/
);
```

*require* on the other hand is typically used to load code in a top-level JavaScript file or within a module should you wish to dynamically fetch dependencies

```javascript
require(['foo', 'bar'], function ( foo, bar ) {
        // rest of your code here
        foo.doSomething();
});
```

*Dynamically-loaded Dependencies*

```javascript
define(function ( require ) {
    var isReady = false, foobar;
 
    // note the inline require within our module definition
    require(['foo', 'bar'], function (foo, bar) {
        isReady = true;
        foobar = foo() + bar();
    });
 
    // we can still return a module
    return {
        isReady: isReady,
        foobar: foobar
    };
});
```


### CommonJS
Это также подход к написанию JS модулей, который для работы требует лоадеров на стороне клиента.

```javascript
var lib = require('package/lib');
 
function foo() {
    lib.log('hello world!');
}
 
exports.foo = foo;
```

### AMD vs. CommonJS
AMD применяет подход browser-first в разработке, выбирая асинхронное поведение и упрощенную обратную совместимость, но не имеет никакой концепции файлового ввода-вывода. Он поддерживает объекты, функции, конструкторы, строки, JSON и многие другие типы модулей, работающие изначально в браузере.

CommonJS, с другой стороны, использует серверный подход, предполагающий синхронное поведение. Под этим мы подразумеваем, что, поскольку CJS поддерживает unwrapped модули(при AMD мы оборачуем модули), он может чувствовать себя немного ближе к спецификациям ES.next/Harmony, освобождая вас от оболочки define(), которую использует AMD.



## Inheritance

### Прототипное наследование

```javascript
function Animal(name) {
    this.name = name;
}

Animal.prototype.run = (speed) => {
    console.log(`run with speed ${speed}`);
};


function Rabbit(name) {
    Animal.call(this, name); // отнаследовать
}

Rabbit.prototype = Object.create(Animal.prototype);
Rabbit.prototype.constructor = Rabbit;
```


### Наследование классов

```javascript
class Car {
    constructor (engine, body) {
        this.engine = engine;
        this.body = body;
    }

    ride (direction) {
        console.log(`I go to that ${direction}`);
        return true;
    }
}

class BmwX5 extends Car { // extends
    constructor (engine, body, spoiler) {
        super(engine, body); // calling of parent constructor
        this.spoiler = spoiler;
        this.defaultDirection = 'Las Vegas';
    }

    ride (direction, autopilot = false) {
        const chosenDirection = autopilot ? this.defaultDirection : direction;
        return super.ride(chosenDirection); // call parent method using super.methodName() 
    }
}
```


## Carrying & Chaining

**Каррирование** – это преобразование функции с множеством аргументов в набор вложенных функций с одним аргументом (f(a, b, c) -> f(a)(b)(c)).

Универсальная ф-ия карирования
```javascript
function partial(fn, ...args) {
    return (..._arg) => {
        return fn(...args, ..._arg);
    }
}
```

**Когда может быть полезна?**
- Когда вам нужно зафиксировать параметр в какой-либо функции. Или другими словами использовать параметр по умолчанию.
- Возможность передать ф-ию как callback, но с предустановленными параметрами.

```javascript
const price = discount(1500,0.10); // $150
const price = discount(2000,0.10); // $200
const price = discount(50,0.10); // $5
const price = discount(5000,0.10); // $500
const price = discount(300,0.10); // $30
```

Здесь мы видим что скидка остается та же, но меняется сумма.

```javascript
const tenPercentDiscount = discount(0.1);
tenPercentDiscount(500);
tenPercentDiscount(2000);
tenPercentDiscount(5000);
tenPercentDiscount(300);
```

**Chaining**
```javascript
arr
    .filter()
    .map()
    .reduce()
```


## Shallow and retained size
**Shallow size** - размер самого объекта, включая ссылки на другие обьекты, но не сами обьекты;
![Shallow size](media/shallow-size.png)

**Retained size** - размер самого обьекта, включая ссылки на обьекты плюс сами обьекты, но только те которые ссылаются на кого-то еще.
![Retained size](media/retained-size.png)

**Deep size** - размер обьекта плюс размер всех его ссылок(не важно куда они ссылаются)
![Deep size](media/deep-size.png)

![Demo](media/demo-shallow-retained.png)



## Young and old generation
![Generations](media/v8-generations.png)

Young generation - раздел памяти у V8, куда первоначально записываются обьекты(где хранятся в памяти).

- Изначально все обьекты помещаются в 'nursery' блок
- После "выживания", обьекты копируются в 'intermediate' generation
- Если еще живут - то переходят в 'old'

V8 implements two garbage collectors: 
- one that frequently collects the young generation;
- one that collects the full heap including both the young and old generation;

Young generation: большинство объектов размещено здесь. Он невелик и предназначен для очень быстрого garbage collecting, независимо от других мест.
Оно в свою очередь делится на два поддраздела: from & to. Когда память в одном заполняется - выполняется swap, где одни идут в другой подраздел, другие в old generation, третьи которые уже не на что не ссылаются - удаляются.

Old generation: содержит большинство объектов, которые могут иметь указатели на другие объекты. Большинство объектов перемещаются сюда после некоторого выживания в young generation.


## Garbage collector
Основной концепцией управления памятью в JavaScript является принцип достижимости.

Если упростить, то «достижимые» значения – это те, которые доступны или используются. Они гарантированно находятся в памяти.

Основной алгоритм сборки мусора – «алгоритм пометок» (англ. «mark-and-sweep»).

Согласно этому алгоритму, сборщик мусора регулярно выполняет следующие шаги:

- Сборщик мусора «помечает» (запоминает) все корневые объекты.
- Затем он идёт по их ссылкам и помечает все найденные объекты.
- Затем он идёт по ссылкам помеченных объектов и помечает объекты, на которые есть ссылка от них. Все объекты запоминаются, чтобы в будущем не посещать один и тот же объект дважды.
- …И так далее, пока не будут посещены все ссылки (достижимые от корней).
- Все непомеченные объекты удаляются.


## Anti-patterns related to memory leaks
- Цикличные ссылки
- Замыкания
- глобальные переменные
- Ссылки на удаленные DOM элементы
- Таймеры, Интервалы


## Memory cleaning
**Hidden classes**
```javascript
function Point(x, y) {
  this.x = x;
  this.y = y;
}

var p1 = new Point(11, 22);
var p2 = new Point(33, 44);
// At this point, p1 and p2 have a shared hidden class
p2.z = 55;
// warning! p1 and p2 now have different hidden classes!
```

Это можно назвать моделью для каждого обьекта, так как их поля и методы отличаются, хотя кажется что они одного типа.

**Числа**
```javascript
var i = 42;  // this is a 31-bit signed integer
var j = 4.2;  // this is a double-precision floating point number
```

не использовать без нужды float тип


**Массивы**
Кардинально не менять типы элементов массива на лету, если можно - то инициализировать размер и тип данных для массива.
Плюс не оставлять пустоты.

```javascript
var a = new Array();
a[0] = 77;   // Allocates
a[1] = 88;
a[2] = 0.5;   // Allocates, converts
a[3] = true; // Allocates, converts
```

```javascript
const arr = new Array(10);
arr[0] = 0;
```

**Мономорфное использование методов**
```javascript
function add(x, y) {
  return x + y;
}

add(1, 2);      // + in add is monomorphic
add("a", "b");  // + in add becomes polymorphic
```


**Использование documentFragment**