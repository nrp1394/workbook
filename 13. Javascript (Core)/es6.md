# ES6

[Understanding ECMAScript 6 by Nicholas Zakas (2015)](https://leanpub.com/understandinges6/read#leanpub-auto-increased-capabilities-of-the-function-constructor)

## let and scope
В ES-2015 предусмотрены новые способы объявления переменных: через `let` и `const` вместо `var`.
- область видимости переменной let – **блок {...}**
- `let` не всплывает в отличии от `var`
- `let` нельзя повторно объявлять
    ```javascript
    let x;
    let x; // ошибка: переменная x уже объявлена
    ```
- `const` задаёт константу, то есть переменную, которую нельзя менять


## Arrow functions
- стрелочные ф-ии предоставляют более просто синтаксис для написания ф-ий.
- стрелочная ф-ия не имеет своего `this`. Она использует тот который актуален для ф-ии в скоупе которой она вызывается


## Default params, Rest, Spread
*Default params* позволяют инициализировать именованные параметры значениями по умолчанию, если не передано значение или `undefined`.
```javascript
function alpha(a = 1, b = 2) {/*...*/}
```

*Rest* позволяет нам представлять неопределенное количество аргументов в виде массива.
```javascript
alpha('one', 'two', 5, 6, 7)
function alpha(first, second, ...args) {
    // args = [5, 6, 7]
}
```

*Spread* позволяет деструктуризировать(распаковать) массивы или обьекты в список переменных и передать их например как аргументы у ф-ию

Пример деструктуризации:
```javascript
const arr = [1, 2, 3, 4];
const [first, second, third, fourth] = arr;
```

```javascript
const arr = ['one', 'two'];
alpha(...arr);
```


## Map
**Map**
Коллекция содержащая пары ключ-значение и при этом сохраняет порядок вставки.
Любое значение (как объекты, так и примитивы) могут быть использованы в качестве ключей.

```javascript
new Map([iterable])
```

*iterable* - массив или любой другой итерируемый объект чьими элементами являются пары ключ-значение (массивы из двух элементов, например [[ 1, 'one' ],[ 2, 'two' ]]) 

```javascript
const map = new Map(
    [
        ['first', 1],
        ['second', 2],
    ]
);
```

<img src="media/map-output.png" alt="Map output" width="350"/> 

**Сравнение Object vs Map**
- Ключами Объекта выступают Строки и Символы, в то время как любое значение может быть ключом Map, включая функции, объекты и примитивы.
- В отличие от Объектов, ключи в Map упорядочены. Таким образом, во время итерации Map, ключи возвращаются в порядке вставки.
- Вы легко можете получить количество элементов в Map с помощью свойства size, в то время как количество элементов Объекта может быть определено только вручную.
- Map - итерируемый объект и может быть итерирован напрямую, в то время как Объект требует ручного получения списка ключей и их итерации.
- Map может иметь более высокую производительность в случаях частого добавления или удаления ключей.

**Методы/Свойства**
- *size* - к-во элеменов в коллекции
- *.clear()* - очистить коллекцию
- .has(key)
- .delete(key)
- .get(key)
- .set(key, value)
- .keys()
- .values()
- .forEach(callbackFn[, thisArg])
- .entries()
- @@iterator
    
  
Два остальных можно использовать для перебора коллекции руками:
```javascript
const mapCollection = new Map(
    [
        ['first', 1],
        ['second', 2],
    ]
);

console.log(mapCollection);

// полноценный итератор для перебора через .next() { value: [key, value], done: true/false }
const iterator1 = mapCollection[Symbol.iterator](); 
for (let item of iterator1) {
    console.log(item);
}

for (let item of mapCollection.entries()) {
    console.log(item);
}

// Не забывайте что можно также Map -> Array
console.log(Array.from(mapCollection));
```


## Set
Коллекция для хранения множества значений(любого типа, как примитивы, так и другие типы объектов), причём каждое значение может встречаться лишь один раз.

```javascript
const setCollection = new Set(
    ['alpha', 'beta', 'omega']
);
```

`Set` имеет те же методы что и `Map` но с небольшим отличием:
- .add()

и не имеет:
- .set()
- .get()

Вот как к примеру виглядят `.keys()`, `.values()`, `.entries()`

<img src="media/set-methods-demo.png" alt="Map output" width="550"/> 


## Promises 
Promise - обьект позволяющий работать с отложенными и асинхронными операциями.

**Методы**

- *Promise.all(iterable)*
  Ожидает исполнения всех промисов или отклонения любого из них.
  Возвращает промис, который исполнится после исполнения всех промисов в iterable. В случае, если любой из промисов будет отклонен, Promise.all будет также отклонен.
  
- *Promise.allSettled(iterable)*
  Ожидает завершения всех полученных промисов (как исполнения так и отклонения).
  Возвращает промис, который исполняется когда все полученные промисы завершены (исполнены или отклонены), содержащий массив результатов исполнения полученных промисов.
  
- *Promise.race(iterable)*
  Ожидает исполнения или отклонения любого из полученных промисов.
  Возвращает промис, который будет исполнен или отклонен с результатом исполнения первого исполненного или отклонённого промиса из .iterable.
  
```javascript
const promiseOk = new Promise((resolve, reject) => {
    resolve('ok!');
});

const promiseFail = new Promise((resolve, reject) => {
    reject('ah');
});

Promise
    .allSettled([promiseOk, promiseFail])
    .then(console.log) // <- then will work
    .catch(console.error);
```

```javascript
// output of Promise.allSettled()
[
    {status: "fulfilled", value: "ok!"},
    {status: "rejected", reason: "ah"}
]
```


## ES6: Classes, super
```javascript
class Alpha {
    constructor () {
        // ...
    }
    
    someMethod () {
    
    }   
}
```

**Множественное наследование**: Класс может быть унаследован от нескольких. ES6 не поддерживает множественное насделование.
**Multi-level Inheritance**: A class can inherit from another class (via) which inherit from the parent class.

```javascript
class Child extends Root
class Leaf extends Child 
// So the leaf extends root indirectly
```

**super** - оператор для доступа к методам родительского класса.



## ES6: Destructuring assignment

**Деструктуризировать** - значит распаковать(разбить) массивы или обьекты в список переменныых.

```javascript
const alpha = {
    fieldA: '1',
    fieldB: '2',
    otherField1: '3',
    otherField2: '4'
};

const {fieldA: myA, fieldB, ...otherFields} = alpha;
// myA: равна fieldA - '1'
// fieldB: '2'
// otherFields: {otherField1: "3", otherField2: "4"}
```

```javascript
const {alpha: myAlpha = '1', beta = '2'} = {};
console.log(myAlpha, beta);
// myAlpha = '1'
// beta = '2'
```

```javascript
const arr = ['alpha', 'beta', 'gamma', 'omega'];

const [, , gamma] = arr; // скипнуть первых два элемента массива
const [name = 'Ivan', surname = 'Petrov'] = []; // установить дефолтные значения
```


## ES6: modules
Еще один подход к организации модульности JS наряду с AMD(Asynchronous Module Definition), CommonJS.



```javascript
// utils.js
export const MY_CONST = 13;

export function alpha () {
    return 'alpha';
}

export function beta () {
    return 'beta';
}

export default {
    BETA: 14,
    omega: () => 'omega'
}
```

```html
<script src="script.js" type="module"></script>
```

```javascript
// script.js
import Utils, {alpha, beta, MY_CONST} from "./utils.js";

console.log(Utils); // {BETA: 14, omega: ƒ}
console.log(alpha, beta, MY_CONST); // {MY_CONST: 13, alpha: ƒ, beta: ƒ}
```

## ES6: Proxy/Reflect
Прокси – это обёртка вокруг объекта, которая «по умолчанию» перенаправляет операции над ней на объект, но имеет возможность перехватывать их.

Проксировать можно любой объект, включая классы и функции.

```javascript
let proxy = new Proxy(target, {
  /* ловушки */
});
```

- *target* – это объект, для которого нужно сделать прокси, может быть чем угодно, включая функции.

Затем обычно используют прокси везде вместо оригинального объекта target. Прокси не имеет собственных свойств или методов. Он просто перехватывает операцию, если имеется соответствующая ловушка, а иначе перенаправляет её сразу на объект target.

Мы можем перехватывать:
- Чтение (get), запись (set), удаление (deleteProperty) свойства (даже несуществующего).
- Вызов функции (apply).
- Оператор new (ловушка construct).
- И многие другие операции (полный список приведён в начале статьи, а также в документации).


Для большинства действий с объектами в спецификации JavaScript есть так называемый «внутренний метод», который на самом низком уровне описывает, как его выполнять. Например, [[Get]] – внутренний метод для чтения свойства, [[Set]] – для записи свойства, и так далее. Эти методы используются только в спецификации, мы не можем обратиться напрямую к ним по имени.
Ловушки как раз перехватывают вызовы этих внутренних методов.

<img src="media/proxy-traps.png" width="700"/>


**Инварианты**
JavaScript налагает некоторые условия – инварианты на реализацию внутренних методов и ловушек.
Большинство из них касаются возвращаемых значений:

- Метод [[Set]] должен возвращать true, если значение было успешно записано, иначе false.
- Метод [[Delete]] должен возвращать true, если значение было успешно удалено, иначе false.

Примеры для get & set

```javascript
const user = {
    name: 'Denchik',
    age: 17
};

const proxyUser = new Proxy(user, {
    get(target, property) {
        if (property === 'name') {
            return target[property] + ' and Telochki';
        }
        return target[property];
    },
    set(target, property, value) {
        if (property === 'age' && value <= 27) {
            target[property] = value;
            return true;
        }
        return true;
        // return false; // will return TypeError
    }
});

console.log(proxyUser.name);
proxyUser.age = 29;
console.log(proxyUser.age);
```

Более широкий пример с реализацтей get, set, deleteProperty, ownKeys

```javascript
let user = {
    name: "Alpha",
    age: 17,
    _password: "secret"
};

const protectedUser = new Proxy(user, {
    get(target, property) { // перехватываем получение свойства
        if (property.startsWith('_')) {
            throw new Error('Access denied');
        }
        return target[property];
    },
    set(target, property, value) { // перехватываем запись свойства
        if (property.startsWith('_')) {
            throw new Error('Access denied');
        }
        target[property] = value;
        return true;
    },
    deleteProperty(target, prop) { // перехватываем удаление свойства
        if (prop.startsWith('_')) {
            throw new Error('Access denied');
        } else {
            delete target[prop];
            return true;
        }
    },
    ownKeys(target) { // перехватываем попытку итерации
        return Object.keys(target).filter(key => !key.startsWith('_'));
    }
});

// check get
try {
    console.log(protectedUser._password);
} catch (e) {
    console.log(e.message);
}

// check set
try {
    protectedUser._password = 'newPassword';
} catch (e) {
    console.log(e.message);
}

// check delete property
try {
    delete protectedUser._password;
} catch (e) {
    console.log(e.message);
}

console.log(Object.keys(protectedUser)); // ['name', 'age']; // "_password" отсутсвует
```

### Reflect
это встроенный объект, который предоставляет методы для перехватывания JavaScript операций.
Все свойства и методы объекта Reflect являются статическими (так же, как и у объекта Math).
Для любой ловушки из Proxy существует метод в Reflect с теми же аргументами. Нам следует использовать его, если нужно перенаправить вызов на оригинальный объект.

По факту он делает то же что и прямое обращение к полям обьекта.

```javascript
get(target, property, context) { // перехватываем получение свойства
    if (property.startsWith('_')) {
        throw new Error('Access denied');
    }
    return target[property];
    // return Reflect.get(target, property, context); // можно и так
}
```

Но он играет роль если для доступа к полю в обьекте используется getter/setter для которого также важен и this.
Этот третий аргумент в `get` ловушке это контекст(`this`) поточного обьекта с которым мы работаем в данное время.

### Минусы и недостатки Proxy

- не имеет доступ до внутренних методов обьектов(приватных)
- нюансы работы с Map, Set, Date, Promise через внутрение поля этих обьектов которые не матчатся с теми внутренними методами ([[MapData]]) которые есть у этих обьектов.
- Прокси не равен первоначальному обьекту
- Прокси не перехватывают проверку на равенства


### Отключение прокси
Обычный прокси
```javascript
const proxy = new Proxy(target, handler);
```

Прокси с возможностью отключения
```javascript
let {proxy, revoke} = Proxy.revocable(target, handler)
```

!ВАЖНО! По факту обьект `target` все еще используется и когда прокси уже не нужен, нам нужно как-то очистить память которую он занимает, когда прокси уже не используется.

Вызов `revoke()` удаляет все внутренние ссылки на оригинальный объект из прокси, так что между ними больше нет связи, и оригинальный объект теперь может быть очищен сборщиком мусора.


## ES6: WeakMap/WeakSet

**WeakMaps** имеют “weak” («слабые») обращения к ключам объекта, а следовательно непрепятствие сборщику мусора, когда мы больше не имеем объекта-ключа.
Ключами **WeakMap** могут быть только **объекты**. Примитивы в качестве ключей не допускаются (т.е. Symbol не может быть ключом WeakMap).

Из-за того, что ссылки являются слабыми, ключи WeakMap не перечисляемы (то есть нет метода, который возвращает список ключей).

- .delete()
- .get()
- .has()
- .set()



**WeakSet** - коллекция, элементами которой могут быть только объекты, особый вид Set, не препятствующий сборщику мусора удалять свои элементы.

- WeakSet содержит только объекты, тогда как Set - значения любого типа.
- Ссылки на объекты в WeakSet являются слабыми: если на объект, хранимый в WeakSet нет ни одной внешней ссылки, то сборщик мусора удалит этот объект. Также это означает, что WeakSet не итерируем, так как нет возможности получить список текущих хранимых в WeakSet объектов.  

.add()
.delete()
.has()


## ES6: Async/await
синтаксис для работы с промисами

```javascript
async function f() {
    return 1;
    // return Promise.resolve(1); // результат будет одинаков
}
```

Ключевое слово **async** перед объявлением функции:
- Обязывает её всегда возвращать промис.
- Позволяет использовать await в теле этой функции.

Ключевое слово **await** заставит интерпретатор JavaScript ждать до тех пор, пока промис справа от **await** не выполнится. После чего оно вернёт его результат, и выполнение кода продолжится.

*await нельзя использовать в обычных функциях*
Если мы попробуем использовать *await* внутри функции, объявленной без *async*, получим синтаксическую ошибку:

```javascript
function f() {
  let promise = Promise.resolve(1);
  let result = await promise; // SyntaxError
}
```

**Пример с Promise.all()**

```javascript
// await будет ждать массив с результатами выполнения всех промисов
let results = await Promise.all([
  fetch(url1),
  fetch(url2),
  ...
]);
```

**Пример обработки ошибок**

```javascript
async function f() {
  try {
    let response = await fetch('/no-user-here');
    let user = await response.json();
  } catch(err) {
    // перехватит любую ошибку в блоке try: и в fetch, и в response.json
    alert(err);
  }
}
```


## ES6: generators and iterators

**Итератор**

- Итератор – объект, предназначенный для перебора другого объекта.
- У итератора должен быть метод `next()`, возвращающий объект `{done: Boolean, value: any}`, где `value` – очередное значение, а `done: true` в конце.
- Метод `Symbol.iterator` предназначен для получения итератора из объекта. Цикл `for..of` делает это автоматически, но можно и вызвать его напрямую.
- В современном стандарте есть много мест, где вместо массива используются более абстрактные «итерируемые» (со свойством Symbol.iterator) объекты, например оператор spread ....
- Встроенные объекты, такие как массивы и строки, являются итерируемыми, в соответствии с описанным выше.


**Генератор**

- Генераторы создаются при помощи функций-генераторов function* f(…) {…}.
- Внутри генераторов и только внутри них существует оператор yield.
- Внешний код и генератор обмениваются промежуточными результатами посредством вызовов next/yield.

*Генератор для написания итераторов*
```javascript
let range = {
    from: 1,
    to: 5,

    [Symbol.iterator]: function() {
        return {
            current: this.from,
            last: this.to,

            next() {
                if (this.current <= this.last) {
                    return { done: false, value: this.current++ };
                } else {
                    return { done: true };
                }
            }
        };
    },

    // с использованием генератора
    *[Symbol.iterator]() { // краткая запись для [Symbol.iterator]: function*()
        for(let value = this.from; value <= this.to; value++) {
            yield value;
        }
    }
};
```


*yield дорога в два конца*
```javascript
function* myGenerator () {
    const name = yield 'What is your name';
    const surname = yield 'Surname?';
    console.log(`You are ${name} ${surname}`);
}

const personGenerator = myGenerator();

const res1 = personGenerator.next();
const res2 = personGenerator.next('Roman');
const res3 = personGenerator.next('Nesterchuk');

// You are Roman Nesterchuk
console.log(personGenerator); // myGenerator {<closed>}
```


## ES6: Symbol
Символ (symbol) – примитивный тип данных, использующийся для создания уникальных идентификаторов.

Символы создаются вызовом функции Symbol(), в которую можно передать описание (имя) символа.
Даже если символы имеют одно и то же имя, это – разные символы.
```javascript
// Создаём символ id с описанием (именем) "id"
let id1 = Symbol("id");
let id2 = Symbol("id");

alert(id1 == id2); // false
```

Если мы хотим, чтобы одноимённые символы были равны, то следует использовать глобальный реестр: вызов `Symbol.for(key)` возвращает (или создаёт) глобальный символ с key в качестве имени. Многократные вызовы команды `Symbol.for` с одним и тем же аргументом возвращают один и тот же символ.

```javascript
// читаем символ из глобального реестра и записываем его в переменную
let id = Symbol.for("id"); // если символа не существует, он будет создан

// читаем его снова в другую переменную (возможно, из другого места кода)
let idAgain = Symbol.for("id");

// проверяем -- это один и тот же символ
alert( id === idAgain ); // true
```


Символы имеют два основных варианта использования:
- «Скрытые» свойства объектов. Если мы хотим добавить свойство в объект, который «принадлежит» другому скрипту или библиотеке, мы можем создать символ и использовать его в качестве ключа. Символьное свойство не появится в for..in, так что оно не будет нечаянно обработано вместе с другими. Также оно не будет модифицировано прямым обращением, так как другой скрипт не знает о нашем символе. Таким образом, свойство будет защищено от случайной перезаписи или использования.

  Так что, используя символьные свойства, мы можем спрятать что-то нужное нам, но что другие видеть не должны.

- Существует множество системных символов, используемых внутри JavaScript, доступных как Symbol.*. Мы можем использовать их, чтобы изменять встроенное поведение ряда объектов. Например, в дальнейших главах мы будем использовать Symbol.iterator для итераторов, Symbol.toPrimitive для настройки преобразования объектов в примитивы и так далее.


Технически символы скрыты не на 100%. Существует встроенный метод Object.getOwnPropertySymbols(obj) – с его помощью можно получить все свойства объекта с ключами-символами. Также существует метод Reflect.ownKeys(obj), который возвращает все ключи объекта, включая символьные. Так что они не совсем спрятаны. Но большинство библиотек, встроенных методов и синтаксических конструкций не используют эти методы.


## ES6: Typed arrays

**ArrayBuffer**
набор бинарных данных с фиксированной длинной. Вы не можете манипулировать содержимым ArrayBuffer напрямую. Вместо этого, необходимо создать типизированное представление DataView, которое будет отображать буфер в определенном формате, и даст доступ на запись и чтение его содержимого.

**Типизированные представления**
Название типизированного представления массива говорит само за себя. Оно представляет массив в распространенных числовых форматах, таких как `Int8`, `Uint32`, `Float64` и так далее. Среди прочих, существует специальное представление `Uint8ClampedArray`. Оно ограничивает значения интервалом от 0 до 255. Это полезно, например, при Обработке данных изображения в Canvas.

```javascript
var buffer = new ArrayBuffer(16);

var int32View = new Int32Array(buffer);
for (var i = 0; i < int32View.length; i++) {
    int32View[i] = i * 2;
}

console.log(buffer);
```

<img src="media/array-buffer.png" width="600"/>



## ES6: Experimental new features

**ArrayBuffer.transfer()**
Метод возвращает новый ArrayBuffer, содержимое которого было взято из данных oldBuffer, а затем либо усечено, либо расширено нулями с помощью newByteLength.

**SharedArrayBuffer & Atomic**
[Хабр память](https://habr.com/ru/company/ruvds/blog/332194/)

SharedArrayBuffer - как средство использовать общую область памяти для нескольких потоков. Как следствие мы говорив о буферах для WebWorker.

В статье говорится о примере где два потока паралельно исполняют операцию инкремента.
И в идеале это не должно приводить ни к чему плохому, но инкремент для человека выглядит как единая операция, на компьютера на самом деле их три.
И гонка как раз может происходить во время этих компьютерных операций не видимых для человека.

Избежать подобных ситуаций можно с помощью реализации так называемых «атомарных операций». Суть тут заключается в том, чтобы то, что для программиста выглядит как одна операция, а для компьютера — как несколько, выглядело бы как одна операция и для компьютера.

Атомарные операции позволяют превратить некие действия, которые могут включать в себя множество инструкций процессора, в нечто вроде единой неделимой инструкции. Эта атомарная «инструкция» сохраняет целостность даже если выполнение команд, из которых она состоит, приостанавливается, и, через некоторое время, возобновляется. 

Вместо обычного sharedVar++ это будет нечто вроде Atomics.add(sabView, index, 1). Первый аргумент метода add представляет собой структуру данных для доступа к SharedArrayBuffer (Int8Array, например). Второй — это индекс, по которому sharedVar находится в массиве. Третий аргумент — число, которое нужно прибавить к sharedVar.
Также помимо этого Atomic имеет методы для работы с потоками.



## Decorator
*Функция-декоратор* - это просто функция, которая принимает другую функцию, изменяет ее поведение, при этом не внося изменений в ее исходный код.

**Декорация классов**
```javascript
function superhero(target) {
    target.isSuperhero = true;
    target.power = 'flight';
}

@superhero
class MySuperHero {}

console.log(MySuperHero.isSuperhero); // true
```

**Декорация методов**
```javascript
function readonly(target, key, descriptor) {
    descriptor.writable = false;
    return descriptor;
}

class Cat {
    constructor(name) {
        this.name = name;
    }

    @readonly
    meow() { return `${this.name} says Meow!`}
}
```

*target* — класс, в котором находится декорируемый член класса.
*name* — имя члена класса.
*descriptor* — дескриптор члена класса. Это, по существу, объект, который был бы передан методу Object.defineProperty.



